/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import simulacia.Konstanty;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesCakaniaNaVolneMiesto extends Process {

    public ProcesCakaniaNaVolneMiesto(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            case Mc.start:
                sprava.setCode(Mc.koniecCakania10MinNaVolneMiesto);
                hold(Konstanty.cakanieNaVolneMiesto,sprava);
                //System.out.println("cakam na flek "+sprava.zakaznik().id()+", "+mySim().currentTime()+" "+sprava);
                //hold(1000000,sprava);
                
            break;
            
            case Mc.koniecCakania10MinNaVolneMiesto:
                //System.out.println("odchadzam po 10 min "+sprava.zakaznik().id()+", "+mySim().currentTime()+" "+sprava);
/*
                System.out.println("odchadzam "+sprava.zakaznik().id());
                assistantFinished(sprava);
*/
                sprava.setAddressee(myAgent());
                sprava.setCode(Mc.odchodZakaznikaPo10Minutach);
                notice(sprava);
                //System.out.println("NOTICE");
            break;
            
            case Mc.breakCA:
                //System.out.println("vybavuje sa "+sprava.zakaznik().id()+", "+mySim().currentTime()+" "+sprava);
                
/*              
                System.out.println("rusim odchod "+sprava.zakaznik().id());
                //System.out.println("kontrola00000000000000000000");
                sprava.setCode(Mc.zadavanieObjednavky);
                sprava.setAddressee(myAgent());
                notice(sprava);
*/
                //System.out.println("BREAK");
                assistantFinished(sprava);
            break;
        }
        
    }
    
}
