/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPRNG.UniformContinuousRNG;
import simulacia.Konstanty;
import OSPABA.Process;
import OSPABA.Simulation;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPrevzatiaOpravenehoAuta extends Process {
    //trvanie 
    private static UniformContinuousRNG uniform = new UniformContinuousRNG(Konstanty.prevzatieOpravenehoAutaMin, Konstanty.prevzatieOpravenehoAutaMax);

    public ProcesPrevzatiaOpravenehoAuta(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                double dlzkaPrevzatiaOpravenehoAuta = uniform.sample();
                sprava.priradenyPracovnik().setDlzkaPrevzatiaOpravenehoAuta(dlzkaPrevzatiaOpravenehoAuta);
                sprava.setCode(Mc.prevzatiaOpravenehoAuta);
                hold(dlzkaPrevzatiaOpravenehoAuta,sprava);
            break;
            
            case Mc.prevzatiaOpravenehoAuta:
                assistantFinished(sprava);
            break;
        }
    }
    
}
