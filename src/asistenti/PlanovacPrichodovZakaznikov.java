/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Scheduler;
import OSPABA.Simulation;
import OSPRNG.ExponentialRNG;
import agenti.AgentOkolia;
import entity.Auto;
import entity.Zakaznik;
import simulacia.Konstanty;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class PlanovacPrichodovZakaznikov extends Scheduler{

    //private static ExponentialRNG exp2 = new ExponentialRNG(Konstanty.strednaHodnotaPrichodovZakaznikovSem2);//sem2
    private static ExponentialRNG exp3 = new ExponentialRNG(Konstanty.strednaHodnotaPrichodovZakaznikovSem3);//sem3
    
    
    public PlanovacPrichodovZakaznikov(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        switch(message.code()) {
            case Mc.start:
                message.setCode(Mc.novyZakaznik);
                //hold(exp2.sample(),message);//sem 2 
                if(myAgent().reklama()<4875)
                    hold((exp3.sample()+2d)/(1d+((myAgent().reklama()/4875d))*0.78d),message);//sem 3 /*+2d*/
                else
                    hold((exp3.sample()+2d)/(1.78d),message);//sem 3 /*+2d*/
            break;
            
            case Mc.novyZakaznik:
                MessageForm copy = message.createCopy();
                //hold(exp2.sample(),copy);//sem 2 
                //System.out.println("rerere "+myAgent().reklama());
                if(myAgent().reklama()<4875)
                    hold((exp3.sample()+2d)/(1d+((myAgent().reklama()/4875d))*0.78),copy);//sem 3 /*+2d*/
                else
                    hold((exp3.sample()+2d)/(1.78d),copy);//sem 3 /*+2d*/
                
                Sprava sprava = (Sprava) message;
                Zakaznik z = new Zakaznik(mySim());
                Auto a = new Auto(mySim());
                a.setMajitel(z);
                z.setAuto(a);
                sprava.setZakaznik(z);
                sprava.setAuto(a);
                assistantFinished(sprava);
            break;
        }
    }
    
    @Override
    public AgentOkolia myAgent() {
        return (AgentOkolia) super.myAgent();
    }
    
}
