/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Query;
import OSPABA.Simulation;
import agenti.AgentParkovisk;
import entity.Parkovisko1;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class DotazVolneParkovacieMiesto1 extends Query {

    public DotazVolneParkovacieMiesto1(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void execute(MessageForm message) {
        Sprava sprava = (Sprava) message;
        Parkovisko1 parkovisko = myAgent().parkovisko1();
        int volneMiesto = parkovisko.dajVolneMiesto(sprava.mySim());
        sprava.setParkovacieMiesto1(volneMiesto);
    }
    
    @Override
    public AgentParkovisk myAgent() {
        return (AgentParkovisk) super.myAgent();
    }
    
}
