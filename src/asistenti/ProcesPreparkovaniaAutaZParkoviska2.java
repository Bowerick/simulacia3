/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import OSPRNG.DeterministicRNG;
import OSPRNG.TriangularRNG;
import agenti.AgentParkovisk;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPreparkovaniaAutaZParkoviska2 extends Process {

    private TriangularRNG triangular = new TriangularRNG(120d, 240d, 540d);//sem2
    
    ////153m   5km/h
    private DeterministicRNG preparkovanie = new DeterministicRNG((0.153d/5d)*3600d);//sem3
    private DeterministicRNG preparkovanie2 = new DeterministicRNG((0.117d/5d)*3600d);//sem3 vymenene parkoviska
    
    public ProcesPreparkovaniaAutaZParkoviska2(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                //double dlzkaPreparkovania = preparkovanie.sample();//sem3
                double dlzkaPreparkovania;// = preparkovanie2.sample();//sem3 vymenene parkoviska
                if(myAgent().vymeneneParkoviska()) {
                    dlzkaPreparkovania= preparkovanie2.sample();//sem3 vymenene parkoviska
                } else {
                    dlzkaPreparkovania= preparkovanie.sample();//sem3 
                }
                sprava.priradenyPracovnik().setDlzkaParkovaniaZ2(dlzkaPreparkovania);
                sprava.setCode(Mc.koniecPreparkovaniaAutaZParkoviska2);
                //hold(triangular.sample(),sprava);//sem 2
                hold(dlzkaPreparkovania,sprava);//sem 3
            break;
            
            case Mc.koniecPreparkovaniaAutaZParkoviska2:
                assistantFinished(sprava);
            break;
            
        }
    }
    
    @Override
    public AgentParkovisk myAgent() {
        return (AgentParkovisk) super.myAgent();
    }
    
}
