/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import simulacia.Konstanty;
import simulacia.Mc;

/**
 *
 * @author Robo
 */
public class ProcesPrejazduRampou extends Process {

    public ProcesPrejazduRampou(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        switch(message.code()) {
            case Mc.start:
                message.setCode(Mc.koniecPrechoduVstupnouRampou);
                hold(Konstanty.prejazdRampou, message);
            break;
            
            case Mc.koniecPrechoduVstupnouRampou:
                assistantFinished(message);
        }
    }
    
}
