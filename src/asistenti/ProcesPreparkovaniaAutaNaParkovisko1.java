/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import OSPRNG.DeterministicRNG;
import OSPRNG.TriangularRNG;
import agenti.AgentParkovisk;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPreparkovaniaAutaNaParkovisko1 extends Process {

    //toDo
    //vypocitat podla planu budovy... zatial si dam take ako na sem2 - pre kontrolu
    
    private TriangularRNG triangular = new TriangularRNG(120d, 240d, 540d);//sem2
    
    //117m   5km/h
    private DeterministicRNG preparkovanie = new DeterministicRNG((0.117d/5d)*3600d);//sem3
    private DeterministicRNG preparkovanie2 = new DeterministicRNG((0.153d/5d)*3600d);//sem3 vymenene parkoviska
    
    public ProcesPreparkovaniaAutaNaParkovisko1(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                //double dlzkaParkovaniaNa1 = triangular.sample();//sem2
                //double dlzkaParkovaniaNa1 = preparkovanie.sample();//sem3
                double dlzkaParkovaniaNa1;
                if(myAgent().vymeneneParkoviska()) {
                    dlzkaParkovaniaNa1= preparkovanie2.sample();//sem3 vymenene parkoviska
                } else {
                    dlzkaParkovaniaNa1= preparkovanie.sample();//sem3 
                }
                sprava.priradenyPracovnik().setDlzkaParkovaniaNa1(dlzkaParkovaniaNa1);
                sprava.setCode(Mc.koniecPreparkovaniaAutaNaParkovisko1);
                //hold(triangular.sample(), sprava);//sem2
                hold(dlzkaParkovaniaNa1,message);//sem 3
            break;
            
            case Mc.koniecPreparkovaniaAutaNaParkovisko1:
                assistantFinished(sprava);
        }
    }
    
    @Override
    public AgentParkovisk myAgent() {
        return (AgentParkovisk) super.myAgent();
    }
    
}
