/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import OSPRNG.UniformContinuousRNG;
import simulacia.Konstanty;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPrevzatiaAutaOdZakaznika extends Process {

    private static UniformContinuousRNG uniform = new UniformContinuousRNG(Konstanty.prevzatieAutaOdZakaznikaMin, Konstanty.prevzatieAutaOdZakaznikaMax);
    
    public ProcesPrevzatiaAutaOdZakaznika(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                double dlzkaPrevzatia =  uniform.sample();
                sprava.zakaznik().setDlzkaPrevzatia(dlzkaPrevzatia);
                sprava.setCode(Mc.koniecPrevzatiaAutaOdZakaznika);
                hold(dlzkaPrevzatia,sprava);
            break;
            
            case Mc.koniecPrevzatiaAutaOdZakaznika:
                assistantFinished(sprava);
            break;
        }
    }
    
}
