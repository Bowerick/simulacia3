/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import OSPRNG.TriangularRNG;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPreparkovaniaAutaNaParkovisko2 extends Process {
    
    public ProcesPreparkovaniaAutaNaParkovisko2(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                sprava.setCode(Mc.koniecPreparkovaniaAutaNaParkovisko2);
                hold(0.0, sprava);
            break;
            
            case Mc.koniecPreparkovaniaAutaNaParkovisko2:
                assistantFinished(sprava);
        }
    }
    
}
