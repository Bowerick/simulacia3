/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Query;
import OSPABA.Simulation;
import agenti.AgentKancelarie;
import entity.Pracovnik;
import java.util.List;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class DotazVolnyPracovnik1 extends Query {

    public DotazVolnyPracovnik1(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void execute(MessageForm message) {
        Sprava sprava = (Sprava) message;
        Pracovnik volnyPracovnik1 = null;
        List<Pracovnik> pracovnici = myAgent().pracovnici1();
        for(Pracovnik p: pracovnici) {
            if(!p.jePracujuci()) {
                volnyPracovnik1 = p;
                volnyPracovnik1.setObsluhovanyZakaznik(sprava.zakaznik());
                volnyPracovnik1.obsadPracovnika(mySim().currentTime());
                break;
            }
        }
        sprava.setPriradenyPracovnik(volnyPracovnik1);
    }
    
    @Override
    public AgentKancelarie myAgent() {
        return (AgentKancelarie) super.myAgent();
    }
}
