/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import OSPRNG.DeterministicRNG;
import OSPRNG.EmpiricPair;
import OSPRNG.EmpiricRNG;
import OSPRNG.UniformContinuousRNG;
import OSPRNG.UniformDiscreteRNG;
import java.util.Random;
import simulacia.Konstanty;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesZadaniaObjednavky extends Process {

    private static UniformContinuousRNG uniform = new UniformContinuousRNG(Konstanty.prevzatieObjednavkyMin, Konstanty.prevzatieObjednavkyMax);

    private Random generator = new Random();//generator poctu oprav sem2
    private EmpiricRNG generatorPoctuOprav = new EmpiricRNG(
                                                      new EmpiricPair(new DeterministicRNG(1d),0.05442176870748299),
                                                      new EmpiricPair(new DeterministicRNG(2d),0.320578231292517),
                                                      new EmpiricPair(new DeterministicRNG(3d),0.30612244897959184),
                                                      new EmpiricPair(new DeterministicRNG(4d),0.2108843537414966),
                                                      new EmpiricPair(new DeterministicRNG(5d),0.09863945578231292),
                                                      new EmpiricPair(new DeterministicRNG(6d),0.00935374149659864));
    private Random generator2 = new Random(); // generator typu opravy
    private Random generator3 = new Random(); //generator typu stredne tazkej opravy
    
    private UniformDiscreteRNG generatorDlzkyJednoduchejOpravy = new UniformDiscreteRNG(2*60, 20*60);//generator dlzky jednoduch opravy
    
    private UniformDiscreteRNG generatorDlzkyStredneTazkejOpravy1 = new UniformDiscreteRNG(10*60, 40*60);//generator dlzky jednoduch opravy
    private UniformDiscreteRNG generatorDlzkyStredneTazkejOpravy2 = new UniformDiscreteRNG(41*60, 61*60);//generator dlzky jednoduch opravy
    private UniformDiscreteRNG generatorDlzkyStredneTazkejOpravy3 = new UniformDiscreteRNG(62*60, 100*60);//generator dlzky jednoduch opravy
    
    private UniformDiscreteRNG generatorDlzkyTazkejOpravy = new UniformDiscreteRNG(120*60, 260*60);//generator dlzky jednoduch opravy
    
    
    
    
    
    
    public ProcesZadaniaObjednavky(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(message.code()) {
            
            case Mc.start:
                //System.out.println("zac zad objedn "+sprava.zakaznik().id());
                sprava.setCode(Mc.koniecZadaniaObjednavky);
                double dlzkaZadavania = uniform.sample();
                sprava.zakaznik().setDlzkaZO(dlzkaZadavania);
                generujOpravy(sprava);
                hold(dlzkaZadavania,sprava);
            break;
            
            case Mc.koniecZadaniaObjednavky:
                //System.out.println("koniec zad objedn "+sprava.zakaznik().id());
                
                assistantFinished(sprava);
            break;
        }
    }

    private void generujOpravy(Sprava sprava) {
        double dlzkaOpravyCelkovo  = 1;
        /*todo generovanie oprav
        ..
        ...dlzkaOpravy = ....
        */
        //generovanie oprav
        double d = generator.nextDouble();
        int pocet = -1;
        if(d<0.4) {
            pocet = 1;
        } else {
            if(d < 0.55) {
                pocet = 2;
            } else {
                if(d < 0.69) {
                    pocet = 3;
                } else {
                    if(d < 0.81) {
                        pocet = 4;
                    } else {
                        if(d < 0.91) {
                            pocet = 5;
                        } else {
                            if(d < 1.0) {
                                pocet = 6;
                            }
                        }
                    }
                }
            }
        }
        //int pocetOprav = pocet;//sem2
        int pocetOprav = generatorPoctuOprav.sample().intValue();//sem3
        for(int i=0;i<pocetOprav;i++) {
            int typOpravy =-1;
            d = generator2.nextDouble();
            if(d<0.7) {
                typOpravy = 1;
            } else {
                if(d < 0.9) {
                    typOpravy = 2;
                } else if(d < 1.0) {
                            typOpravy = 3;
                        }
            }
            if(typOpravy == -1)
                System.out.println("pruser 2");
            int dlzkaOpravy = -1;
            
            if(typOpravy == 1) {
                dlzkaOpravy = generatorDlzkyJednoduchejOpravy.sample();
            }
            if(typOpravy == 2) {
                int typStredneTazkejOpravy = -1;
                d = generator3.nextDouble();
                if(d<0.1) {
                    typStredneTazkejOpravy = 1;
                } else {
                    if(d < 0.7) {
                        typStredneTazkejOpravy = 2;
                    } else {
                        if(d < 1.0) {
                            typStredneTazkejOpravy = 3;
                        }
                    }
                }
                
                if(typStredneTazkejOpravy == -1) {
                    System.out.println("pruser 3");
                }
                if(typStredneTazkejOpravy == 1) {
                    dlzkaOpravy = generatorDlzkyStredneTazkejOpravy1.sample();
                }
                if(typStredneTazkejOpravy == 2) {
                    dlzkaOpravy = generatorDlzkyStredneTazkejOpravy2.sample();
                }
                if(typStredneTazkejOpravy == 3) {
                    dlzkaOpravy = generatorDlzkyStredneTazkejOpravy3.sample();
                }
            }
            if(typOpravy == 3) {
                dlzkaOpravy = generatorDlzkyTazkejOpravy.sample();
            }
            dlzkaOpravyCelkovo += dlzkaOpravy;
        }
        sprava.zakaznik().setDlzkaOpravy(dlzkaOpravyCelkovo);
        
    }
    
}
