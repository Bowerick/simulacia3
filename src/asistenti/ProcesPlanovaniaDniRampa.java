/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPlanovaniaDniRampa extends Process {

    public ProcesPlanovaniaDniRampa (int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                sprava.setCode(Mc.koniecDnaRampa);
                hold((8d*60d*60d),sprava);
            break;
            
            case Mc.koniecDnaRampa:
                Sprava spravaCopy = (Sprava) sprava.createCopy();
                hold((8d*60d*60d),spravaCopy);
                
                assistantFinished(sprava);
                
            break;
                    
        }
    }
    
}
