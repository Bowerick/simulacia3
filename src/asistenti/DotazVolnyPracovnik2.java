/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Query;
import OSPABA.Simulation;
import agenti.AgentDielne;
import entity.Pracovnik;
import java.util.List;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class DotazVolnyPracovnik2 extends Query {

    public DotazVolnyPracovnik2(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void execute(MessageForm message) {
        Sprava sprava = (Sprava) message;
        Pracovnik volnyPracovnik = null;
        List<Pracovnik> pracovnici = myAgent().pracovnci2();
        for(Pracovnik p: pracovnici) {
            if(!p.jePracujuci()) {
                volnyPracovnik = p;
                volnyPracovnik.obsadPracovnika(mySim().currentTime());
                break;
            }
        }
        sprava.setPriradenyPracovnik(volnyPracovnik);
    }
    
    @Override
    public AgentDielne myAgent() {
        return (AgentDielne) super.myAgent();
    }
    
}
