/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesOpravy extends Process {

    public ProcesOpravy(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        
        switch(sprava.code()) {
            
            case Mc.start:
                sprava.setCode(Mc.koniecOpravy);
                sprava.priradenyPracovnik().setDlzkaOpravy(sprava.zakaznik().dlzkaOpravy());
                hold(sprava.zakaznik().dlzkaOpravy(), sprava);
            break;
            
            case Mc.koniecOpravy:
                //System.out.println("koniec opravujem "+sprava.zakaznik().id()+", "+mySim().currentTime()+", "+mySim().currentReplication());
                
                assistantFinished(sprava);
            break;
        }
    }
    
}
