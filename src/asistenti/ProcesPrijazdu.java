/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asistenti;

import OSPABA.CommonAgent;
import OSPABA.MessageForm;
import OSPABA.Process;
import OSPABA.Simulation;
import OSPRNG.DeterministicRNG;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ProcesPrijazdu extends Process {

    private DeterministicRNG prijazd = new DeterministicRNG((0.0185d/5d)*3600d);
    
    public ProcesPrijazdu(int id, Simulation mySim, CommonAgent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.start:
                sprava.setCode(Mc.koniecPrijazdu);
                hold(prijazd.sample(),sprava);
            break;
            
            case Mc.koniecPrijazdu:
                assistantFinished(sprava);
            break;
        }
    }
    
}
