/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import OSPDataStruct.SimQueue;
import OSPStat.Stat;
import OSPStat.WStat;
import asistenti.DotazVolnyPracovnik1;
import asistenti.ProcesCakaniaNaVolneMiesto;
import asistenti.ProcesPlanovaniaDniKancelaria;
import asistenti.ProcesPrevzatiaAutaOdZakaznika;
import asistenti.ProcesPrevzatiaOpravenehoAuta;
import asistenti.ProcesZadaniaObjednavky;
import entity.Auto;
import entity.Pracovnik;
import entity.Zakaznik;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import manazeri.ManazerKancelarie;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class AgentKancelarie extends Agent {
    
    private List<Pracovnik> pracovnici1;
    private int pocetPracovnikov1;
    
    private int prioritaPreparkovaniaZParkoviska2;
    private int prioritaZadaniaObjednavky;
    
    private SimQueue<Sprava> frontaZakaznikovNaZadanieObjednavky;
    private SimQueue<Sprava> frontaAutNaParkPredServisom;
    
    private DotazVolnyPracovnik1 dotazVolnyPracovnik1;
    
    private int pocetNeobsluzenych;
    private int pocetObsluzenych;
    private int pocetCakajucichNaOpravu;
    private int pocetCakajucichNaZadanieObjednavky;
    private int pocetZadavajucichObjednavku;
    
    private Stat casCakaniaNaOpravuStat;
    private Stat casCakaniaVRadeNaZadaniaObjednavkyStat;
    private Stat dlzkaOpravyStat;
    
    //priemerna vytazenost
    
    
    public AgentKancelarie(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        //manazer
        new ManazerKancelarie(Id.manazerKancelarie, mySim, this);
        
        //asistenti
        dotazVolnyPracovnik1 = new DotazVolnyPracovnik1(Id.dotazVolnyPracovnik1, mySim, this);
        new ProcesZadaniaObjednavky(Id.procesZadaniaObjednavky, mySim, this);
        new ProcesPrevzatiaAutaOdZakaznika(Id.procesPrevzatiaAutaOdZakaznika, mySim, this);
        
        new ProcesCakaniaNaVolneMiesto(Id.procesCakaniaNaVolneMiesto, mySim, this);
        
        new ProcesPlanovaniaDniKancelaria(Id.procesPlanovaniaDniKancelaria, mySim, this);
        
        new ProcesPrevzatiaOpravenehoAuta(Id.procesPrevzatiaOpravenehoAuta, mySim, this);
        
        //spravy
        addOwnMessage(Mc.vstupDoKancelarie);
        addOwnMessage(Mc.parkovacieMiesto1Zistene);
        addOwnMessage(Mc.zadavanieObjednavky);
        
        addOwnMessage(Mc.koniecZadaniaObjednavky);
        addOwnMessage(Mc.koniecPrevzatiaAutaOdZakaznika);
        
        addOwnMessage(Mc.zaparkovanie1Ukoncene);
        
        
        addOwnMessage(Mc.parkovacieMiesto2Zistene);
        addOwnMessage(Mc.koniecCakania10MinNaVolneMiesto);
        addOwnMessage(Mc.opravaAjZaparkovanieSkoncene);
        addOwnMessage(Mc.odparkovanieAutaZParkoviska2Ukoncene);
        addOwnMessage(Mc.prevzatiaOpravenehoAuta);
        
        addOwnMessage(Mc.odchodZakaznikaPo10Minutach);
        addOwnMessage(Mc.parkovacieMiesto1Uvolnene);
        
        addOwnMessage(Mc.zacniPlanovanieDnovKancelaria);
        addOwnMessage(Mc.koniecDnaKancelaria);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        
        pocetNeobsluzenych = 0;
        pocetObsluzenych = 0;
        pocetCakajucichNaOpravu = 0;
        pocetCakajucichNaZadanieObjednavky = 0;
        pocetZadavajucichObjednavku = 0;
        
        pracovnici1 = new ArrayList<Pracovnik>(pocetPracovnikov1);
        for(int i=0;i<pocetPracovnikov1;i++) {
            pracovnici1.add(new Pracovnik(mySim()));
        }
        
        frontaZakaznikovNaZadanieObjednavky = new SimQueue<>(new WStat(mySim()));
        frontaAutNaParkPredServisom = new SimQueue<>(new WStat(mySim()));
	casCakaniaNaOpravuStat = new Stat();
        casCakaniaVRadeNaZadaniaObjednavkyStat = new Stat();
        dlzkaOpravyStat = new Stat();
    }

    /**
     * @return the dlzkaOpravyStat
     */
    public Stat dlzkaOpravyStat() {
        return dlzkaOpravyStat;
    }

    /**
     * @return the casCakaniaNaOpravuStat
     */
    public Stat casCakaniaNaOpravuStat() {
        return casCakaniaNaOpravuStat;
    }

    /**
     * @return the casCakaniaVRadeNaZadaniaObjednavkyStat
     */
    public Stat casCakaniaVRadeNaZadaniaObjednavkyStat() {
        return casCakaniaVRadeNaZadaniaObjednavkyStat;
    }
    
    public List<Pracovnik> pracovnici1() {
        return pracovnici1;
    }
    
    public SimQueue<Sprava> frontaZakaznikovNaZadanieObjednavky() {
        return this.frontaZakaznikovNaZadanieObjednavky;
    }
    
    public WStat dlzkaRadu() {
        return this.frontaZakaznikovNaZadanieObjednavky.lengthStatistic();
    }
    
    public SimQueue<Sprava> frontAutNaParkPredServisom() {
        return this.frontaAutNaParkPredServisom;
    }
    
    public WStat pocetAutNaParkPredServisom() {
        return this.frontaZakaznikovNaZadanieObjednavky.lengthStatistic();
    }
    
    public DotazVolnyPracovnik1 dotazVolnyPracovnik1() {
        return dotazVolnyPracovnik1;
    }


    /**
     * @param pocetPracovnikov1 the pocetPracovnikov1 to set
     */
    public void setPocetPracovnikov1(int pocetPracovnikov1) {
        this.pocetPracovnikov1 = pocetPracovnikov1;
    }

    /**
     * @return the prioritaPreparkovaniaZParkoviska2
     */
    public int prioritaPreparkovaniaZParkoviska2() {
        return prioritaPreparkovaniaZParkoviska2;
    }

    /**
     * @return the prioritaZadaniaObjednavky
     */
    public int prioritaZadaniaObjednavky() {
        return prioritaZadaniaObjednavky;
    }

    /**
     * @param prioritaPreparkovaniaZParkoviska2 the prioritaPreparkovaniaZParkoviska2 to set
     */
    public void setPrioritaPreparkovaniaZParkoviska2(int prioritaPreparkovaniaZParkoviska2) {
        this.prioritaPreparkovaniaZParkoviska2 = prioritaPreparkovaniaZParkoviska2;
    }

    /**
     * @param prioritaZadaniaObjednavky the prioritaZadaniaObjednavky to set
     */
    public void setPrioritaZadaniaObjednavky(int prioritaZadaniaObjednavky) {
        this.prioritaZadaniaObjednavky = prioritaZadaniaObjednavky;
    }
    
    public void pridajNeobsluzeneho() {
        this.pocetNeobsluzenych++;
    }

    public int pocetNeobsluzenych() {
        return pocetNeobsluzenych;
    }
    
    public void pridajObsluzeneho() {
        this.pocetObsluzenych++;
    }

    public int pocetObsluzenych() {
        return pocetObsluzenych;
    }
    
    public void pridajCakajucehoNaOpravu() {
        this.pocetCakajucichNaOpravu++;
    }
    
    public void odoberCakajucehoNaOpravu() {
        this.pocetCakajucichNaOpravu--;
    }

    public int pocetCakajucichNaOpravu() {
        return pocetCakajucichNaOpravu;
    }
    
    public void pridajCakajucehoNaZadanieObjednavky() {
        this.pocetCakajucichNaZadanieObjednavky++;
    }
    
    public void odoberCakajucehoZadanieObjednavky() {
        this.pocetCakajucichNaZadanieObjednavky--;
    }

    public int pocetCakajucichZadanieObjednavky() {
        return pocetCakajucichNaZadanieObjednavky;
    }

    public void vynulujPocetCakajucichNaZO() {
        pocetCakajucichNaZadanieObjednavky = 0;
    }
    
    public void pridajPocetZadavajucichObjednavku() {
        this.pocetZadavajucichObjednavku++;
    }
    
    public void odoberPocetZadavajucichoObjednavku() {
        this.pocetZadavajucichObjednavku--;
    }

    public int pocetZadavajucichObjednavku() {
        return pocetZadavajucichObjednavku;
    }

    /**
     * @return the pocetPracovnikov1
     */
    public int pocetPracovnikov1() {
        return pocetPracovnikov1;
    }
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Zakaznik> zakaznici() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {
            if(frontaZakaznikovNaZadanieObjednavky == null || pracovnici1 == null) {
                //System.out.println("dva "+frontaZakaznikovNaZadanieObjednavky+", "+pracovnici1);
                return zakaznici;
            }
        
            for(MessageForm m: frontaZakaznikovNaZadanieObjednavky) {
                Sprava sprava = (Sprava) m;
                zakaznici.add(sprava.zakaznik());
            }
            
            for(Pracovnik p: pracovnici1) {
                if(p.jePracujuci()) {
                    zakaznici.add(p.obsluhovanyZakaznik());
                }
            }
            
        //});
        
        return zakaznici;
    }
    /**
     *
     * @return zakaznici v kancelarii
     */
    public int zakazniciSize() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {
            int a = frontaZakaznikovNaZadanieObjednavky.size();
            for(Pracovnik p: pracovnici1) {
                if(p.jePracujuci()) {
                    a++;
                }
            }
            
        //});
        
        return a;
    }
    
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Auto> auta() {
        List<Auto> auta = new LinkedList();
        
        //mySim().invokeSync(() -> {
            for(MessageForm m: frontaZakaznikovNaZadanieObjednavky) {
                Sprava sprava = (Sprava) m;
                auta.add(sprava.zakaznik().auto());
            }
            
            for(Pracovnik p: pracovnici1) {
                if(p.jePracujuci()) {
                    auta.add(p.obsluhovanyZakaznik().auto());
                }
            }
            
        //});
        
        return auta;
    }

    public int autaSize() {
        int pocet = frontaZakaznikovNaZadanieObjednavky.size();
            
        for(Pracovnik p: pracovnici1) {
            if(p.jePracujuci()) {
                pocet++;
            }
        }
        return pocet;
    }
}
