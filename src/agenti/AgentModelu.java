/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.Simulation;
import manazeri.ManazerModelu;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class AgentModelu extends Agent {
    
    public AgentModelu(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        new ManazerModelu(Id.manazerModelu, mySim, this);
        
        addOwnMessage(Mc.init);
        addOwnMessage(Mc.prichodZakaznika);
        addOwnMessage(Mc.odchodZakaznika);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        
        Sprava message = new Sprava(mySim());
        message.setCode(Mc.init);
        message.setAddressee(this);
        manager().notice(message);
    }
    
}
