/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import OSPDataStruct.SimQueue;
import OSPStat.WStat;
import asistenti.ProcesPlanovaniaDniRampa;
import asistenti.ProcesPrejazduRampou;
import asistenti.ProcesPrejazduVystupnouRampou;
import entity.Auto;
import entity.Rampa;
import entity.Zakaznik;
import java.util.LinkedList;
import java.util.List;
import manazeri.ManazerRampy;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class AgentRampy extends Agent {
    
    private SimQueue<MessageForm> frontZakaznikovPredRampouPriPrichode;
    private SimQueue<MessageForm> frontZakaznikovPredRampouPriOdchode;
    private Rampa rampa;
    private Rampa rampaVystupna;
    private int pocetPredServisom;
    private int pocetPredOdchodomZoServisu;
    
    public AgentRampy(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        new ManazerRampy(Id.manazerRampy, mySim, this);
        
        new ProcesPrejazduRampou(Id.procesPrejazduRampou, mySim, this);
        new ProcesPrejazduVystupnouRampou(Id.procesPrejazduVystupnouRampou, mySim, this);
        new ProcesPlanovaniaDniRampa(Id.procesPlanovaniaDniRampa, mySim, this);
        
        addOwnMessage(Mc.prechodVstupnouRampou);
        addOwnMessage(Mc.koniecPrechoduVstupnouRampou);
        
        addOwnMessage(Mc.prechodVystupnouRampou);
        addOwnMessage(Mc.koniecPrechoduVystupnouRampou);
        addOwnMessage(Mc.zacniPlanovanieDnovRampa);
        addOwnMessage(Mc.koniecDnaRampa);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        frontZakaznikovPredRampouPriPrichode = new SimQueue<>(new WStat(mySim()));
        frontZakaznikovPredRampouPriOdchode = new SimQueue<>(new WStat(mySim()));
        rampa = new Rampa(mySim());
        rampaVystupna = new Rampa(mySim());
        
        pocetPredServisom = 0;
        pocetPredOdchodomZoServisu = 0;
    }

    /**
     * @return the frontZakaznikovPredRampouPriPrichode
     */
    public SimQueue<MessageForm> frontZakaznikovPredRampouPriPrichode() {
        return frontZakaznikovPredRampouPriPrichode;
    }
    
    public WStat dlzkaFrontuZakaznikovPredRampouPriPrichode() {
        return frontZakaznikovPredRampouPriPrichode.lengthStatistic();
    }

    /**
     * @return the frontZakaznikovPredRampouPriPrichode
     */
    public SimQueue<MessageForm> frontZakaznikovPredRampouPriOdchode() {
        return frontZakaznikovPredRampouPriOdchode;
    }
    
    public WStat dlzkaFrontuZakaznikovPredRampouPriOdchode() {
        return frontZakaznikovPredRampouPriOdchode.lengthStatistic();
    }

    /**
     * @return the rampa
     */
    public Rampa rampa() {
        return rampa;
    }
    
    public Rampa rampaVystupna() {
        return rampaVystupna;
    }
    

    /**
     * @return the pocetPredServisom
     */
    public int pocetPredServisom() {
        return pocetPredServisom;
    }
    
    public void pridajPocetPredServisom() {
        this.pocetPredServisom++;
    }
    
    public void odoberPocetPredServisom() {
        this.pocetPredServisom--;
    }

    public void vynulujPocetPredServisom() {
        this.pocetPredServisom = 0;
    }

    public int pocetPredOdchodomZoServisu() {
        return pocetPredOdchodomZoServisu;
    }
    
    public void pridajPocetPredOdchodomZoServisu() {
        this.pocetPredOdchodomZoServisu++;
    }
    
    public void odoberPocetPredOdchodomZoServisu() {
        this.pocetPredOdchodomZoServisu--;
    }
    
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Zakaznik> zakaznici() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {
            if(frontZakaznikovPredRampouPriOdchode == null || frontZakaznikovPredRampouPriPrichode == null 
                    || rampa == null) {
                //System.out.println("jedna "+frontZakaznikovPredRampouPriOdchode+", "+frontZakaznikovPredRampouPriPrichode+
                //        ", "+rampa);
                return zakaznici;
            }
            
            for(MessageForm message: frontZakaznikovPredRampouPriPrichode) {
                Sprava sprava = (Sprava) message;
                zakaznici.add(sprava.zakaznik());
            }
            
            for(MessageForm message: frontZakaznikovPredRampouPriOdchode) {
                Sprava sprava = (Sprava) message;
                zakaznici.add(sprava.zakaznik());
            }
            
            Zakaznik prichadzajuci = rampa.zakaznikPriVstupe();
            
            if(prichadzajuci != null) {
                zakaznici.add(prichadzajuci);
            }
            
            Zakaznik odchadzajuci = rampa.zakaznikPriOdchode();
            if(odchadzajuci != null) {
                zakaznici.add(odchadzajuci);
            }
            //System.out.println("rampa zakaznici: "+zakaznici.size());
        //});
        
        return zakaznici;
    }

    public int zakazniciSize() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {
            if(frontZakaznikovPredRampouPriOdchode == null || frontZakaznikovPredRampouPriPrichode == null 
                    || rampa == null) {
                //System.out.println("jedna "+frontZakaznikovPredRampouPriOdchode+", "+frontZakaznikovPredRampouPriPrichode+
                //        ", "+rampa);
                return 0;
            }
            
            for(MessageForm message: frontZakaznikovPredRampouPriPrichode) {
                Sprava sprava = (Sprava) message;
                zakaznici.add(sprava.zakaznik());
            }
            
            for(MessageForm message: frontZakaznikovPredRampouPriOdchode) {
                Sprava sprava = (Sprava) message;
                zakaznici.add(sprava.zakaznik());
            }
            
            Zakaznik prichadzajuci = rampa.zakaznikPriVstupe();
            
            if(prichadzajuci != null) {
                zakaznici.add(prichadzajuci);
            }
            
            Zakaznik odchadzajuci = rampa.zakaznikPriOdchode();
            if(odchadzajuci != null) {
                zakaznici.add(odchadzajuci);
            }
            //System.out.println("rampa zakaznici: "+zakaznici.size());
        //});
        
        return zakaznici.size();
    }
    
    
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Auto> auta() {
        List<Auto> auta = new LinkedList();
        
        //mySim().invokeSync(() -> {
            if(frontZakaznikovPredRampouPriOdchode == null || frontZakaznikovPredRampouPriPrichode == null 
                    || rampa == null) {
                //System.out.println("jedna "+frontZakaznikovPredRampouPriOdchode+", "+frontZakaznikovPredRampouPriPrichode+
                //        ", "+rampa);
                return auta;
            }
            
            for(MessageForm message: frontZakaznikovPredRampouPriPrichode) {
                Sprava sprava = (Sprava) message;
                auta.add(sprava.auto());
            }
            
            for(MessageForm message: frontZakaznikovPredRampouPriOdchode) {
                Sprava sprava = (Sprava) message;
                auta.add(sprava.auto());
            }
            
            Zakaznik prichadzajuci = rampa.zakaznikPriVstupe();
            
            if(prichadzajuci != null) {
                auta.add(prichadzajuci.auto());
            }
            
            Zakaznik odchadzajuci = rampa.zakaznikPriOdchode();
            if(odchadzajuci != null) {
                auta.add(odchadzajuci.auto());
            }
            //System.out.println("rampa zakaznici: "+zakaznici.size());
        //});
        
        return auta;
    }

    public int autaSize() {
         if(frontZakaznikovPredRampouPriOdchode == null || frontZakaznikovPredRampouPriPrichode == null 
                    || rampa == null) {
                //System.out.println("jedna "+frontZakaznikovPredRampouPriOdchode+", "+frontZakaznikovPredRampouPriPrichode+
                //        ", "+rampa);
                return 0;
            }
            
            int pocet = frontZakaznikovPredRampouPriPrichode.size();
            
            pocet += frontZakaznikovPredRampouPriOdchode.size();
            
            Zakaznik prichadzajuci = rampa.zakaznikPriVstupe();
            
            if(prichadzajuci != null) {
                pocet++;
            }
            
            Zakaznik odchadzajuci = rampa.zakaznikPriOdchode();
            if(odchadzajuci != null) {
                pocet++;
            }
            return pocet;
    }
}
