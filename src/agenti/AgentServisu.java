/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import asistenti.ProcesOdjazdu;
import asistenti.ProcesPrijazdu;
import entity.Auto;
import entity.Zakaznik;
import java.util.LinkedList;
import java.util.List;
import manazeri.ManazerServisu;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class AgentServisu extends Agent {
    
    private LinkedList<Auto> prichadzajuceAuta;
    private LinkedList<Auto> odchadzajuceAuta;
    
    public AgentServisu(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        //manazer
        new ManazerServisu(Id.manazerServisu, mySim, this);
        
        new ProcesPrijazdu(Id.procesPrijazdu, mySim, this);
        new ProcesOdjazdu(Id.procesOdjazdu, mySim, this);
        
        addOwnMessage(Mc.obsluhaZakaznika);
        addOwnMessage(Mc.vstupDoKancelarie);
        addOwnMessage(Mc.zakaznikVybaveny);
        addOwnMessage(Mc.vstupDoServisu);
        addOwnMessage(Mc.koniecPrijazdu);
        addOwnMessage(Mc.opustaServis);
        addOwnMessage(Mc.koniecOdjazdu);
        addOwnMessage(Mc.zacniPlanovanieDnov);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        
        prichadzajuceAuta = new LinkedList();
        odchadzajuceAuta = new LinkedList();
    }
    
    public void pridajPrichadzajuceAuto(Auto a){
        this.prichadzajuceAuta.add(a);
    }
    
    public void odoberPrichadzajuceAuto(Auto a){
        this.prichadzajuceAuta.remove(a);
    }
    
    public void pridajOdchadzajuceAuto(Auto a){
        this.odchadzajuceAuta.add(a);
    }
    
    public void odoberOdchadzajuceAuto(Auto a){
        this.odchadzajuceAuta.remove(a);
    }
    
    
    public List<Auto> auta() {
        List<Auto> auta = new LinkedList();
        
        for(Auto a: prichadzajuceAuta)
            auta.add(a);
        
        for(Auto a: odchadzajuceAuta)
            auta.add(a);
        
        return auta;
    }
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Zakaznik> zakaznici() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        for(Auto a: prichadzajuceAuta)
            zakaznici.add(a.majitel());
        
        for(Auto a: odchadzajuceAuta)
            zakaznici.add(a.majitel());
        
        
        return zakaznici;
    }

    public int autaSize() {
        int pocet = prichadzajuceAuta.size();
        pocet += odchadzajuceAuta.size();
        return  pocet;
    }
}
