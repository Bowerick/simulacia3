/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.Simulation;
import OSPDataStruct.SimQueue;
import OSPStat.WStat;
import asistenti.DotazVolneParkovacieMiesto1;
import asistenti.DotazVolneParkovacieMiesto2;
import asistenti.ProcesPreparkovaniaAutaNaParkovisko1;
import asistenti.ProcesPreparkovaniaAutaNaParkovisko2;
import asistenti.ProcesPreparkovaniaAutaZParkoviska2;
import entity.Auto;
import entity.Parkovisko1;
import entity.Zakaznik;
import java.util.LinkedList;
import java.util.List;
import manazeri.ManazerParkovisk;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;
import sun.java2d.SurfaceData;

/**
 *
 * @author Robo
 */
public class AgentParkovisk extends Agent {
    
    //todo agent parkovisk
    private Parkovisko1 parkovisko1;
    private int pocetParkovacichMiest1;
    
    private Parkovisko1 parkovisko2;
    private int pocetParkovacichMiest2;
    
    private DotazVolneParkovacieMiesto1 dotazVolneParkovacieMiesto1;
    private DotazVolneParkovacieMiesto2 dotazVolneParkovacieMiesto2;
    
    //private SimQueue<Zakaznik> frontaAutNaPreparkovanieZDielne;
    private SimQueue<Sprava> frontaAutNaPreparkovanieZDielne;
    
    private SimQueue<Sprava> frontaAutNaPreparkovanieZParkoviska2;
    private boolean vymenene;
    
    public AgentParkovisk(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        new ManazerParkovisk(Id.manazerParkovisk, mySim, this);
        
        dotazVolneParkovacieMiesto1 = new DotazVolneParkovacieMiesto1(Id.dotazVolneParkovacieMiesto1, mySim, this);
        dotazVolneParkovacieMiesto2 = new DotazVolneParkovacieMiesto2(Id.dotazVolneParkovacieMiesto2, mySim, this);
        
        new ProcesPreparkovaniaAutaNaParkovisko1(Id.procesPreparkovaniAutaNaParkovisko1, mySim, this);
        new ProcesPreparkovaniaAutaNaParkovisko2(Id.procesPreparkovaniAutaNaParkovisko2, mySim, this);
        new ProcesPreparkovaniaAutaZParkoviska2(Id.procesPreparkovaniaAutaZParkoviska2, mySim, this);
        
        
        addOwnMessage(Mc.zistenieParkovaciehoMiesta1);
        addOwnMessage(Mc.zistenieParkovaciehoMiesta2);
        
        addOwnMessage(Mc.uvolniParkovacieMiesto1);
        
        addOwnMessage(Mc.koniecPreparkovaniaAutaNaParkovisko1);
        addOwnMessage(Mc.koniecPreparkovaniaAutaNaParkovisko2);//1
        addOwnMessage(Mc.koniecPreparkovaniaAutaZParkoviska2);
        
        addOwnMessage(Mc.zaparkujAutoNaParkovisko1);
        addOwnMessage(Mc.zaparkujAutoNaParkovisko2);//1
        addOwnMessage(Mc.odparkujAutaZParkoviska2);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        
        parkovisko1 = new Parkovisko1(mySim(),pocetParkovacichMiest1);
        parkovisko2 = new Parkovisko1(mySim(),pocetParkovacichMiest2);
        
        frontaAutNaPreparkovanieZDielne = new SimQueue<>(new WStat(mySim()));
        frontaAutNaPreparkovanieZParkoviska2 = new SimQueue<>(new WStat(mySim()));
    }
    
    public SimQueue<Sprava> frontaAutNaPreparkovanieZDielne() {
        return this.frontaAutNaPreparkovanieZDielne;
    }
    
    public SimQueue<Sprava> frontaAutNaPreparkovanieZParkoviska2() {
        return this.frontaAutNaPreparkovanieZParkoviska2;
    }

    public DotazVolneParkovacieMiesto1 dotazVolneParkovacieMiesto1() {
        return dotazVolneParkovacieMiesto1;
    }

    public DotazVolneParkovacieMiesto2 dotazVolneParkovacieMiesto2() {
        return dotazVolneParkovacieMiesto2;
    }
    
    public Parkovisko1 parkovisko1() {
        return parkovisko1;
    }
    
    public Parkovisko1 parkovisko2() {
        return parkovisko2;
    }
    
    /**
     * @param pocetParkovacichMiest1 the pocetParkovacichMiest1 to set
     */
    public void setPocetParkovacichMiest1(int pocetParkovacichMiest1) {
        this.pocetParkovacichMiest1 = pocetParkovacichMiest1;
    }
    
    /**
     * @param pocetParkovacichMiest2 the pocetParkovacichMiest1 to set
     */
    public void setPocetParkovacichMiest2(int pocetParkovacichMiest2) {
        this.pocetParkovacichMiest2 = pocetParkovacichMiest2;
    }

    /**
     * @return the pocetParkovacichMiest1
     */
    public int pocetParkovacichMiest1() {
        return pocetParkovacichMiest1;
    }

    /**
     * @return the pocetParkovacichMiest2
     */
    public int pocetParkovacichMiest2() {
        return pocetParkovacichMiest2;
    }

    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Zakaznik> zakaznici() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {.invokeSync(() -> {
            
            for(int i=0;i<parkovisko1.miesto().length;i++) {
                if(!parkovisko1.miesto()[i]) {
                    if(parkovisko1.rezervovane(i) != null) {
                        //zakaznici.add(parkovisko1.rezervovane(i).majitel());
                    } else {
                        zakaznici.add(parkovisko1.zaparkovaneAuto(i).majitel());
                    }
                    
                }
            }
            
            for(Sprava s: frontaAutNaPreparkovanieZDielne) {
                zakaznici.add(s.zakaznik());
            }
            
            for(Sprava s: frontaAutNaPreparkovanieZParkoviska2) {
                zakaznici.add(s.zakaznik());
            }
        //});
        
        return zakaznici;
    }

    public int zakazniciSize() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {.invokeSync(() -> {
            
            for(int i=0;i<parkovisko1.miesto().length;i++) {
                if(!parkovisko1.miesto()[i]) {
                    if(parkovisko1.rezervovane(i) != null) {
                        //zakaznici.add(parkovisko1.rezervovane(i).majitel());
                    } else {
                        zakaznici.add(parkovisko1.zaparkovaneAuto(i).majitel());
                    }
                    
                }
            }
            
            for(Sprava s: frontaAutNaPreparkovanieZDielne) {
                zakaznici.add(s.zakaznik());
            }
            
            for(Sprava s: frontaAutNaPreparkovanieZParkoviska2) {
                zakaznici.add(s.zakaznik());
            }
        //});
        
        return zakaznici.size();
    }
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Auto> auta() {
        List<Auto> auta = new LinkedList();
        
        if(frontaAutNaPreparkovanieZDielne == null)
            return auta;
        
        //mySim().invokeSync(() -> {.invokeSync(() -> {
            for(Sprava s: frontaAutNaPreparkovanieZDielne) {
                auta.add(s.auto());
            }
            for(Sprava s: frontaAutNaPreparkovanieZParkoviska2) {
                auta.add(s.auto());
            }
        //});
        
        return auta;
    }

    public int autaSize() {
        if(frontaAutNaPreparkovanieZDielne == null)
            return 0;
        
        int pocet = frontaAutNaPreparkovanieZDielne.size();
        pocet += frontaAutNaPreparkovanieZParkoviska2.size();
        return pocet;
    }

    public boolean vymeneneParkoviska() {
        return vymenene;
    }

    /**
     * @param vymenene the vymenene to set
     */
    public void setVymenene(boolean vymenene) {
        this.vymenene = vymenene;
    }

    
}
