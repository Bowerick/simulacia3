/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.ContinualAssistant;
import OSPABA.Simulation;
import asistenti.PlanovacPrichodovZakaznikov;
import manazeri.ManazerOkolia;
import simulacia.Id;
import simulacia.Mc;

/**
 *
 * @author Robo
 */
public class AgentOkolia extends Agent {
    
    private ContinualAssistant planovacPrichodovZakaznikov;
    private int pocetZakaznikov;
    private double reklama;
    
    public AgentOkolia(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        new ManazerOkolia(Id.manazerOkolia, mySim, this);
        
        planovacPrichodovZakaznikov = new PlanovacPrichodovZakaznikov(Id.planovacPrichodovZakaznikov, mySim, this);
        
        addOwnMessage(Mc.init);
        addOwnMessage(Mc.novyZakaznik);
        addOwnMessage(Mc.odchodZakaznika);
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        pocetZakaznikov = 0;
    }
    
    public void pridajZakaznika() {
        this.pocetZakaznikov++;
    }
    
    public int pocetZakaznikov() {
        return pocetZakaznikov;
    }

    /**
     * @return the reklama
     */
    public double reklama() {
        return reklama;
    }

    /**
     * @param reklama the reklama to set
     */
    public void setReklama(double reklama) {
        this.reklama = reklama;
    }
    
}
