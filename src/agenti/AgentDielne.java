/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenti;

import OSPABA.Agent;
import OSPABA.Simulation;
import OSPDataStruct.SimQueue;
import OSPStat.WStat;
import asistenti.DotazVolnyPracovnik2;
import asistenti.ProcesOpravy;
import entity.Auto;
import entity.Pracovnik;
import entity.Zakaznik;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import manazeri.ManazerDielne;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class AgentDielne extends Agent {

    private List<Pracovnik> pracovnici2;
    private int pocetPracovnikov2;
    
    private DotazVolnyPracovnik2 dotazVolnyPracovnik2;

    private SimQueue<Sprava> frontaAutNaOpravu;
    
    private int pocetOpravenych;
    private double priemerCasOpravyOpravenych;
    
    private double trzba;
    
    public AgentDielne(int id, Simulation mySim, Agent parent) {
        super(id, mySim, parent);
        
        //manazer
        new ManazerDielne(Id.manazerDielne, mySim, this);
        
        //asistenti
        dotazVolnyPracovnik2 = new DotazVolnyPracovnik2(Id.dotazVolnyPracovnik2, mySim, this);
        new ProcesOpravy(Id.procesOpravy, mySim, this);
        
        //spravy
        //addOwnMessage(Mc.preparkovanieZParkoviska2);
        //addOwnMessage(Mc.parkovacieMiesto2Zistene);
        addOwnMessage(Mc.novaOprava);
        addOwnMessage(Mc.koniecOpravy);
        addOwnMessage(Mc.zaparkovanie2Ukoncene);
        
    }
    
    @Override
    public void prepareReplication() {
        super.prepareReplication();
        
        pracovnici2 = new ArrayList<Pracovnik>(pocetPracovnikov2);
        for(int i=0;i<pocetPracovnikov2;i++) {
            pracovnici2.add(new Pracovnik(mySim()));
        }
        
        frontaAutNaOpravu = new SimQueue<>(new WStat(mySim()));
        
        pocetOpravenych = 0; 
        priemerCasOpravyOpravenych = .0;
        trzba = .0;
    }
    
    public SimQueue<Sprava> frontaAutNaOpravu() {
        return this.frontaAutNaOpravu;
    }
    
    public List<Pracovnik> pracovnci2() {
        return pracovnici2;
    }
    

    /**
     * @param pocetPracovnikov2 the pocetPracovnikov2 to set
     */
    public void setPocetPracovnikov2(int pocetPracovnikov2) {
        this.pocetPracovnikov2 = pocetPracovnikov2;
    }

    /**
     * @return the dotazVolnyPracovnik2
     */
    public DotazVolnyPracovnik2 dotazVolnyPracovnik2() {
        return dotazVolnyPracovnik2;
    }
    
    public void pridajTrzbu(double trzba) {
        this.trzba += trzba;
    }
    
    public double trzba() {
        return this.trzba;
    }
    
    public void pridajOpraveneho(double cas) {
        this.priemerCasOpravyOpravenych +=cas;
        this.pocetOpravenych++;
    }
    
    public int pocetOpravenych() {
        return pocetOpravenych;
    }
    
    public double priemerCasOpravyOpraveneho() {
        return priemerCasOpravyOpravenych/(double)pocetOpravenych;
    }

    /**
     * @return the pracovnici2
     */
    public List<Pracovnik> pracovnici2() {
        return pracovnici2;
    }

    /**
     * @return the pocetPracovnikov2
     */
    public int pocetPracovnikov2() {
        return pocetPracovnikov2;
    }
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Zakaznik> zakaznici() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {
            
            for(Pracovnik p: pracovnici2) {
                if(p.jePracujuci()) {
                    zakaznici.add(p.obsluhovanyZakaznik());
                }
            }
            
            for(Sprava s: frontaAutNaOpravu) {
                zakaznici.add(s.zakaznik());
            }
            
        //});
        
        return zakaznici;
    }

    public int zakazniciSize() {
        List<Zakaznik> zakaznici = new LinkedList();
        
        //mySim().invokeSync(() -> {
            
            for(Pracovnik p: pracovnici2) {
                if(p.jePracujuci()) {
                    zakaznici.add(p.obsluhovanyZakaznik());
                }
            }
            
            for(Sprava s: frontaAutNaOpravu) {
                zakaznici.add(s.zakaznik());
            }
            
        //});
        return zakaznici.size();
    }
    
    /**
     *
     * @return zakaznici v kancelarii
     */
    public List<Auto> auta() {
        List<Auto> auta = new LinkedList();
        
        //mySim().invokeSync(() -> {
            
            for(Pracovnik p: pracovnici2) {
                if(p.jePracujuci()) {
                    auta.add(p.obsluhovanyZakaznik().auto());
                }
            }
            
            for(Sprava s: frontaAutNaOpravu) {
                auta.add(s.zakaznik().auto());
            }
            
        //});
        
        return auta;
    }

    public int autaSize() {
        int pocet = 0;
        for(Pracovnik p: pracovnici2) {
            if(p.jePracujuci()) {
                pocet++;
            }
        }
            
        pocet += frontaAutNaOpravu.size();
        return pocet;
    }
}
