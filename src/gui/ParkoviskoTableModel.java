/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Parkovisko1;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class ParkoviskoTableModel extends MyTableModel {
    
    final int parkovisko1 = 0;
    final int auto = 1;

    public ParkoviskoTableModel(Simulation sim, AgentServisu agentServisu, AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
        super(sim, agentServisu, agentRampy, agentKancelarie, agentDielne, agentParkovisk);
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case parkovisko1:
                return "parkovisko1 (miesto)";
            case auto:
                return "auto";
        }
        return null;
    }

    @Override
    public int getRowCount() {
        SimulaciaServis sim =(SimulaciaServis) agentParkovisk().mySim();
        int a = sim.invokeSync(() -> agentParkovisk().pocetParkovacichMiest1());
        return a;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SimulaciaServis sim =(SimulaciaServis) agentParkovisk().mySim();
        boolean [] parkovisko = sim.invokeSync(() -> agentParkovisk().parkovisko1().miesto());
        Parkovisko1 p = sim.invokeSync(() -> agentParkovisk().parkovisko1());
        if(p == null)
            return null;
        switch(columnIndex) {
            
            case parkovisko1:
                return (rowIndex+1);
            case auto:
                if(parkovisko[rowIndex])
                    return "voľné";
                else {
                    if(p.zaparkovaneAuto(rowIndex) == null) {
                        if(p.rezervovane(rowIndex).majitel() == null)
                            return null;
                        return "rezervácia pre auto "+p.rezervovane(rowIndex).id()+" zákazníka "+p.rezervovane(rowIndex).majitel().id();
                    } else {
                        if(p.zaparkovaneAuto(rowIndex).majitel() == null)
                            return null;
                        return "auto "+p.zaparkovaneAuto(rowIndex).id()+" zákazníka "+p.rezervovane(rowIndex).majitel().id();
                    }
                }
                
            default:
                return null;
        }
    }
    
}
