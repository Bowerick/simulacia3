/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Pracovnik;
import java.util.List;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class Pracovnici1TableModel extends MyTableModel {

    final int id = 0;
    final int stav = 1;
    
    public Pracovnici1TableModel(Simulation sim, AgentServisu agentServisu, AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
        super(sim, agentServisu, agentRampy, agentKancelarie, agentDielne, agentParkovisk);
    }
    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case id:
                return "id";
            case stav:
                return "stav";
        }
        return null;
    }

    @Override
    public int getRowCount() {
        SimulaciaServis sim = (SimulaciaServis) agentKancelarie().mySim();
        return sim.invokeSync(() -> agentKancelarie().pracovnici1().size());
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        double aktCas = agentKancelarie().mySim().invokeSync(() ->  agentKancelarie().mySim().currentTime());
        List<Pracovnik> pracovnici = agentKancelarie().mySim().invokeSync(() ->  agentKancelarie().pracovnici1());
        if(pracovnici == null)
            return null;
        
        if(pracovnici.size() <= rowIndex) {
            return null;
        }
        Pracovnik pracovnik = pracovnici.get(rowIndex);
        switch(columnIndex) {

            case id:
                return pracovnik.id();

            case stav:
                if(pracovnik.stav().contains("na parkovisko 1")) {
                    return pracovnik.stav()+" "+" ("+(Math.round(((aktCas-pracovnik.zaciatokParkovaniaNa1())/(pracovnik.dlzkaParkovaniaNa1()))*10000)/100d)+"% )";
                } else
                    if(pracovnik.stav().contains("po oprave na parkovisko pred servisom")) {
                        return pracovnik.stav()+" "+" ("+(Math.round(((aktCas-pracovnik.zaciatokParkovaniaZ2())/(pracovnik.dlzkaParkovaniaZ2()))*10000)/100d)+"% )";
                    } else {
                        if(pracovnik.stav().contains("odovzdáva auto zákazníkovi")) {
                            return pracovnik.stav()+" "+" ("+(Math.round(((aktCas-pracovnik.casZacatiaPrevzatiaOpravenehoAuta())/(pracovnik.dlzkaPrevzatiaOpravenehoAuta()))*10000)/100d)+"% )";
                        } else 
                            return pracovnik.stav();
                    }
            default:
                return null;
        }
    }
    
}
