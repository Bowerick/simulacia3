/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Zakaznik;
import java.util.List;
import simulacia.Id;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class ZakazniciTableModel extends MyTableModel {

    final int id = 0;
    final int stav = 1;
    final int casPrichodu = 2;
    final int casSkoncenieZO = 3;

    public ZakazniciTableModel(Simulation sim, AgentServisu agentServisu, AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
        super(sim, agentServisu, agentRampy, agentKancelarie, agentDielne, agentParkovisk);
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case id:
                return "id";
            case stav:
                return "stav";
            case casPrichodu:
                return "cas prichodu";
            case casSkoncenieZO:
                return "cas skoncenia zo";
        }
        return null;
    }
    
    @Override
    public int getRowCount() {
        /*
        SimulaciaServis sim =(SimulaciaServis) agentRampy().mySim();
        int d = agentParkovisk().mySim().invokeSync(() -> agentParkovisk().zakaznici().size());
        int c = agentDielne().mySim().invokeSync(() -> agentDielne().zakaznici().size());
        int b = agentKancelarie().mySim().invokeSync(() -> agentKancelarie().zakaznici().size());
        int a = sim.invokeSync(() -> agentRampy().zakaznici().size());
        return (a+b+c+d);
        */
        int d = sim().invokeSync(() -> ((AgentParkovisk)sim().findAgent(Id.agentParkovisk)).zakazniciSize());
        int c = sim().invokeSync(() -> ((AgentDielne)sim().findAgent(Id.agentDielne)).zakazniciSize());
        int b = sim().invokeSync(() -> ((AgentKancelarie)sim().findAgent(Id.agentKancelarie)).zakazniciSize());
        int a = sim().invokeSync(() -> ((AgentRampy)sim().findAgent(Id.agentRampy)).zakazniciSize());
        return (a+b+c+d);

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        double aktCas = agentKancelarie().mySim().invokeSync(() ->  agentKancelarie().mySim().currentTime());
        List<Zakaznik> zakaznici = agentParkovisk().mySim().invokeSync(() -> agentParkovisk().zakaznici());
        if(zakaznici == null)
            return null;
        List<Zakaznik> zakaznici2 = agentDielne().mySim().invokeSync(() -> agentDielne().zakaznici());
        for(Zakaznik z: zakaznici2) {
            if(!zakaznici.contains(z))
                zakaznici.add(z);
        }
        zakaznici.addAll(agentKancelarie().mySim().invokeSync(() -> agentKancelarie().zakaznici()));

        
        zakaznici.addAll(agentRampy().mySim().invokeSync(() ->  agentRampy().zakaznici()));
        
        zakaznici.addAll(agentServisu().mySim().invokeSync(() ->  agentServisu().zakaznici()));
        
        if(zakaznici.size() <= rowIndex) {
            return null;
        }
        Zakaznik zakaznik = zakaznici.get(rowIndex);
        if(zakaznik == null) {
            return null;
        }
          
        switch(columnIndex) {

            case id:
                return zakaznik.id();

            case stav:
                if(zakaznik.stav().contains("zadáva objednávku")) {
                    return zakaznik.stav()+" "+" ("+(Math.round(((aktCas-zakaznik.casZadaniaObjednavky())/(zakaznik.dlzkaZO()))*10000)/100d)+"% )";
                } else 
                    if(zakaznik.stav().contains("odovzdáva auto pracovníkovi")) {
                        return zakaznik.stav()+" "+" ("+(Math.round(((aktCas-zakaznik.casZacatiaPrevzatiaAuta())/(zakaznik.dlzkaPrevzatia()))*10000)/100d)+"% )";
                    }
                return zakaznik.stav();

            case casPrichodu:
                return zakaznik.casPrichodu();
                
            case casSkoncenieZO:
                return zakaznik.casZacatiaPrevzatiaAuta();
            default:
                return null;
        }
    }
}
