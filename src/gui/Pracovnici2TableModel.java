/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Pracovnik;
import java.util.List;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class Pracovnici2TableModel extends MyTableModel {

    final int id = 0;
    final int stav = 1;
    
    public Pracovnici2TableModel(Simulation sim, AgentServisu agentServisu, AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
        super(sim, agentServisu, agentRampy, agentKancelarie, agentDielne, agentParkovisk);
    }
    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case id:
                return "id";
            case stav:
                return "stav";
        }
        return null;
    }

    @Override
    public int getRowCount() {
        SimulaciaServis sim = (SimulaciaServis) agentDielne().mySim();
        return sim.invokeSync(() -> agentDielne().pracovnici2().size());
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        double aktCas = agentKancelarie().mySim().invokeSync(() ->  agentKancelarie().mySim().currentTime());
        List<Pracovnik> pracovnici = agentDielne().mySim().invokeSync(() ->  agentDielne().pracovnici2());
        if(pracovnici == null)
            return null;
        
        if(pracovnici.size() <= rowIndex) {
            return null;
        }
        Pracovnik pracovnik = pracovnici.get(rowIndex);
        switch(columnIndex) {

            case id:
                return pracovnik.id();

            case stav:
                if(pracovnik.stav().contains("opravuje")) {
                    return pracovnik.stav()+" "+" ("+(Math.round(((aktCas-pracovnik.zaciatokOpravy())/(pracovnik.dlzkaOpravy()))*10000)/100d)+"% )";
                } else {
                    return pracovnik.stav();
                }

            default:
                return null;
        }
    }
    
}
