/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.ISimDelegate;
import OSPABA.SimState;
import OSPABA.Simulation;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import simulacia.Konstanty;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class Frame implements ISimDelegate, ActionListener {
    
    private SimulaciaServis simulacia;
    private SimulaciaServis simulacia2;
    
    private Color[] farby;
    private Random generatorR;
    private Random generatorG;
    private Random generatorB;
    private double cenaReklamy;
    
    
    private JTable tabulkaZakaznici = new JTable() {
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col);
            if(value != null && col==0) {
                int id = (int) value;
                for(int i=0;i<getColumnCount();i++)
                    comp.setBackground(farby[(id%200)/2]);
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaZakaznici2 = new JTable() {
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col);
            if(value != null && col==0) {
                int id = (int) value;
                for(int i=0;i<getColumnCount();i++)
                    comp.setBackground(farby[(id%200)/2]);
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaPracovnici1 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("príjma") && slova[1].contains("objednávku")) {
                    id = Integer.parseInt(slova[3]);
                    //for(int i=0;i<getColumnCount();i++) {
                        comp = super.prepareRenderer(renderer, row, 0);
                        comp.setBackground(farby[(id%200)/2]);
                    //}
                }
                else if(slova[0].contains("príjma") && slova[1].contains("auto")) {
                    id = Integer.parseInt(slova[4]);
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("parkuje")) {
                    id = Integer.parseInt(slova[4]);
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(farby[(id%200)/2]);
                }
                else if(!slova[0].contains("-") && !slova[0].contains("nepracuje") && slova[1].contains("odovzdáva")) {
                    id = Integer.parseInt(slova[4]);
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("nepracuje") || slova[0].contains("-")) {
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaPracovnici12 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("príjma") && slova[1].contains("objednávku")) {
                    id = Integer.parseInt(slova[3]);
                    //for(int i=0;i<getColumnCount();i++) {
                        comp = super.prepareRenderer(renderer, row, 0);
                        comp.setBackground(farby[(id%200)/2]);
                    //}
                }
                else if(slova[0].contains("príjma") && slova[1].contains("auto")) {
                    id = Integer.parseInt(slova[4]);
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("parkuje")) {
                    id = Integer.parseInt(slova[4]);
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("nepracuje") || slova[0].contains("-")) {
                    for(int i=0;i<getColumnCount();i++)
                        comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaPracovnici2 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("opravuje")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("parkuje")) {
                    id = Integer.parseInt(slova[4]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("nepracuje") || slova[0].contains("-")) {
                    comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaPracovnici22 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("opravuje")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("parkuje")) {
                    id = Integer.parseInt(slova[4]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("nepracuje") || slova[0].contains("-")) {
                    comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaAuta = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                //System.out.println(slova[0]+", "+slova[1]+", "+slova[2]+", "+slova[3]+", "+slova[4]+", "+slova[5]+", ");
                if(slova[1].contains("parkuje")) {
                    id = Integer.parseInt(slova[4]);
                    comp.setBackground(farby[(id%200)/2]);
                }  
                else if(slova[1].contains("zákazníka")) {
                    id = Integer.parseInt(slova[2]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[1].contains("čaká") && slova[2].contains("na") && slova[3].contains("uvoľnenie")) {
                    id = Integer.parseInt(slova[13]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("auto") && slova[1].contains("zákazníka")) {
                    id = Integer.parseInt(slova[2]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[1].contains("odovzdáva") && slova[6].contains("oprave")) {
                    id = Integer.parseInt(slova[4]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("zákazník")) {
                    id = Integer.parseInt(slova[1]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("opravené")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[6].contains("zadanie") && slova[7].contains("objednávky")) {
                    id = Integer.parseInt(slova[9]);
                    comp.setBackground(farby[(id%200)/2]);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaAuta2 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                //System.out.println(slova[0]+", "+slova[1]+", "+slova[2]+", "+slova[3]+", "+slova[4]+", "+slova[5]+", ");
                if(slova[1].contains("parkuje")) {
                    id = Integer.parseInt(slova[4]);
                    comp.setBackground(farby[(id%200)/2]);
                }  
                else if(slova[1].contains("zákazníka")) {
                    id = Integer.parseInt(slova[2]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[1].contains("čaká") && slova[2].contains("na") && slova[3].contains("uvoľnenie")) {
                    id = Integer.parseInt(slova[13]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("auto") && slova[1].contains("zákazníka")) {
                    id = Integer.parseInt(slova[2]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[1].contains("odovzdáva") && slova[6].contains("oprave")) {
                    id = Integer.parseInt(slova[4]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("zákazník")) {
                    id = Integer.parseInt(slova[1]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("opravené")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[6].contains("zadanie") && slova[7].contains("objednávky")) {
                    id = Integer.parseInt(slova[9]);
                    comp.setBackground(farby[(id%200)/2]);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaParkovisko1 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("rezervácia")) {
                    id = Integer.parseInt(slova[5]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("auto")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("voľné")) {
                    comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaParkovisko12 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("rezervácia")) {
                    id = Integer.parseInt(slova[5]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("auto")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("voľné")) {
                    comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaParkovisko2 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("rezervácia")) {
                    id = Integer.parseInt(slova[5]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("auto")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("voľné")) {
                    comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTable tabulkaParkovisko22 = new JTable(){
        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
            Component comp = super.prepareRenderer(renderer, row, col);
            Object value = getModel().getValueAt(row, col+1);
            if(value != null && col==0) {
                String[] slova = ((String)value).split(" ");
                int id = -1;
                if(slova[0].contains("rezervácia")) {
                    id = Integer.parseInt(slova[5]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("auto")) {
                    id = Integer.parseInt(slova[3]);
                    comp.setBackground(farby[(id%200)/2]);
                }
                else if(slova[0].contains("voľné")) {
                    comp.setBackground(Color.WHITE);
                }
            }
            int rendererWidth = comp.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(col);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return comp;
        }
    };
    private JTextArea textArea = new JTextArea("");
    private JTextArea textArea2 = new JTextArea("");
    
    private JFrame frame;
    private JButton startButton = new JButton("start");
    private JButton startButton2 = new JButton("start");
    private JButton pauseButton = new JButton("pause");
    private JCheckBox rezimCheckBox = new JCheckBox("Zrýchlený režim");
    private JSlider rychlost = new JSlider(1,50000,1);
    //private JProgressBar replikacieProgres = new JProgressBar();
    private ChangeListener changeListener;
    
    //private JComboBox reklamaComboBox = new JComboBox(new Double[]{0d,1000d,2000d,3000d,4000d,5000d});
    private JTextArea reklamaArea1 = new JTextArea("0",1,5);
    private JTextArea prac1Area = new JTextArea("1",1,2);
    private JTextArea prac2Area = new JTextArea("3",1,2);
    
    //private JComboBox reklamaComboBox2 = new JComboBox(new Double[]{0d,1000d,2000d,3000d,4000d,5000d});
    private JTextArea reklamaArea2 = new JTextArea("0",1,5);
    private JTextArea prac1Area2 = new JTextArea("1",1,2);
    private JTextArea prac2Area2Min = new JTextArea("1",1,2);
    private JTextArea prac2Area2Max = new JTextArea("10",1,2);
    
    private JTextArea park1Area = new JTextArea("6",1,2);
    private JTextArea park2Area = new JTextArea("10",1,2);
    private JCheckBox vymeneneCheckBox = new JCheckBox();   
    
    private JTextArea park1Area2 = new JTextArea("6",1,2);
    private JTextArea park2Area2 = new JTextArea("10",1,2);
    private JCheckBox vymeneneCheckBox2 = new JCheckBox();
    
    private JTextArea park1Area3 = new JTextArea("6",1,2);
    private JTextArea park2Area3 = new JTextArea("10",1,2);
    private JCheckBox vymeneneCheckBox3 = new JCheckBox();
    
    //private JComboBox reklamaComboBox3 = new JComboBox(new Double[]{0d,1000d,2000d,3000d,4000d,5000d});
    private JTextArea reklamaArea3 = new JTextArea("0",1,5);
    private JTextArea prac1Area3 = new JTextArea("2",1,2);
    private JTextArea prac2Area3Min = new JTextArea("1",1,2);
    private JTextArea prac2Area3Max = new JTextArea("10",1,2);
    
    private JTextArea simCasArea1 = new JTextArea("",1,20);
    private JTextArea simCasArea2 = new JTextArea("",1,20);
    private JTextArea pocetObsluzenychArea = new JTextArea("",1,4);
    private JTextArea pocetObsluzenychArea2 = new JTextArea("",1,4);
    private JTextArea pocetNeobsluzenychArea = new JTextArea("",1,4);
    private JTextArea pocetNeobsluzenychArea2 = new JTextArea("",1,4);
    private JProgressBar vyuzitiePrac1Progres = new JProgressBar();
    private JProgressBar vyuzitiePrac2Progres = new JProgressBar();
    private JTextArea priemDlzkaRadu1Area = new JTextArea("",1,13);
    private JTextArea priemDlzkaRadu2Area = new JTextArea("",1,13);
    private JTextArea trzbyArea = new JTextArea("",1,13);
    private JTextArea trzbyArea2 = new JTextArea("",1,13);
    private JTextArea vydavkyArea = new JTextArea("",1,13);
    private JTextArea vydavkyArea2 = new JTextArea("",1,13);
    private JTextArea ziskArea = new JTextArea("",1,13);
    private JTextArea ziskArea2 = new JTextArea("",1,13);
    
    //private JTextField pomField = new JTextField("");
    
    private JFreeChart chartCasCakaniaNaOpravu;
    private XYSeriesCollection dataset = new XYSeriesCollection();
    private XYSeries series1 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series2 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is1 = new JTextArea("",1,85);
    private JTextArea is12 = new JTextArea("",1,85);
    
    private JFreeChart chartCasCakaniaVRade;
    private XYSeriesCollection dataset2 = new XYSeriesCollection();
    private XYSeries series21 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series22 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is2 = new JTextArea("",1,85);
    private JTextArea is22 = new JTextArea("",1,85);
    
    private JFreeChart chartZisk;
    private XYSeriesCollection dataset3 = new XYSeriesCollection();
    private XYSeries series31 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series32 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is3 = new JTextArea("",1,85);
    private JTextArea is32 = new JTextArea("",1,85);
    
    private JFreeChart chartPocetLudiVRade;
    private XYSeriesCollection dataset4 = new XYSeriesCollection();
    private XYSeries series41 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series42 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is4 = new JTextArea("",1,85);
    private JTextArea is42 = new JTextArea("",1,85);
    
    private JFreeChart chartVytazenostPrac1;
    private XYSeriesCollection dataset5 = new XYSeriesCollection();
    private XYSeries series51 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series52 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is5 = new JTextArea("",1,85);
    private JTextArea is52 = new JTextArea("",1,85);
    
    private JFreeChart chartVytazenostPrac2;
    private XYSeriesCollection dataset6 = new XYSeriesCollection();
    private XYSeries series61 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series62 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is6 = new JTextArea("",1,85);
    private JTextArea is62 = new JTextArea("",1,85);
    
    private JFreeChart chartObsluzeny;
    private XYSeriesCollection dataset7 = new XYSeriesCollection();
    private XYSeries series71 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series72 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is7 = new JTextArea("",1,85);
    private JTextArea is72 = new JTextArea("",1,85);
    
    private JFreeChart chartNeobsluzeny;
    private XYSeriesCollection dataset8 = new XYSeriesCollection();
    private XYSeries series81 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series82 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is8 = new JTextArea("",1,85);
    private JTextArea is82 = new JTextArea("",1,85);
    
    private JFreeChart chartVytazenostPark1;
    private XYSeriesCollection dataset9 = new XYSeriesCollection();
    private XYSeries series91 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series92 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is9 = new JTextArea("",1,85);
    private JTextArea is92 = new JTextArea("",1,85);
    
    private JFreeChart chartVytazenostPark2;
    private XYSeriesCollection dataset10 = new XYSeriesCollection();
    private XYSeries series101 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series102 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is10 = new JTextArea("",1,85);
    private JTextArea is102 = new JTextArea("",1,85);
    
    private JFreeChart chartPocetAutPredServisom;
    private XYSeriesCollection dataset11 = new XYSeriesCollection();
    private XYSeries series111 = new XYSeries("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky");
    private XYSeries series112 = new XYSeries("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2");
    private JTextArea is11 = new JTextArea("",1,85);
    private JTextArea is112 = new JTextArea("",1,85);
    
    
    private JFreeChart[] chartHospodarskyVysledok = new JFreeChart[4];
    private XYSeriesCollection[] datasetHospodarskyVysledok = new XYSeriesCollection[4];
    private XYSeries[] seriesCelkNaklady = new XYSeries[4];//("41");
    private XYSeries[] seriesTrzby = new XYSeries[4];
    private XYSeries[] seriesZisk = new XYSeries[4];
    private XYSeries[] seriesFixneNaklady = new XYSeries[4];
    private JTextArea[] textAreaHospodarskvyVysledok = new JTextArea[4];
    
    
    public static void main(String[] args) {
        
        new Frame();
        
    }
    
    public Frame() {
        initColors();
        initCharts();
        initComponents();
        initSimulation();
    }

    private void initComponents() {   
    /*
        tabulkaPracovnici1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabulkaPracovnici2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabulkaParkovisko1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabulkaParkovisko2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabulkaZakaznici.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tabulkaAuta.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    */
        vymeneneCheckBox.setSelected(false);
        frame = new JFrame("Autoservis");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                
        startButton.addActionListener(this);
        startButton2.addActionListener(this);
        pauseButton.addActionListener(this);
        rezimCheckBox.addActionListener(this);
        vymeneneCheckBox.addActionListener(this);
        vymeneneCheckBox2.addActionListener(this);
        vymeneneCheckBox3.addActionListener(this);
        changeListener = (ChangeEvent e) -> {
            zmenRychlost();
        };
        rychlost.addChangeListener(changeListener);
        
        
        textArea.setFocusable(false);
        textArea2.setFocusable(false);
        textArea.setRows(15);
        textArea2.setRows(15);
        
        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.setLayout(new BorderLayout());
        JPanel botPanel = new JPanel(new BorderLayout());
        botPanel.setLayout(new BorderLayout());
        JPanel botPanel2 = new JPanel(new BorderLayout());
        botPanel2.setLayout(new BorderLayout());
        
        //-------------------------------
        JPanel topTopPanel = new JPanel();
        topTopPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        JPanel topTopPanel2 = new JPanel();
        topTopPanel2.setLayout(new FlowLayout(FlowLayout.LEADING));
        
        JPanel topBotPanel = new JPanel(new BorderLayout());
        topBotPanel.setLayout(new BoxLayout(topBotPanel, BoxLayout.PAGE_AXIS));
        
        JPanel topBotPanel2 = new JPanel(new BorderLayout());
        topBotPanel2.setLayout(new BoxLayout(topBotPanel2, BoxLayout.LINE_AXIS));
        //-------------------------------
        JPanel botTopPanel = new JPanel(new BorderLayout());
        botTopPanel.setLayout(new BoxLayout(botTopPanel, BoxLayout.LINE_AXIS));
        
        JPanel botBotPanel = new JPanel(new BorderLayout());
        botBotPanel.setLayout(new BoxLayout(botBotPanel, BoxLayout.LINE_AXIS));
        
        JPanel botTopPanel2 = new JPanel(new BorderLayout());
        botTopPanel2.setLayout(new BoxLayout(botTopPanel2, BoxLayout.LINE_AXIS));
        
        JPanel botBotPanel2 = new JPanel(new BorderLayout());
        botBotPanel2.setLayout(new BoxLayout(botBotPanel2, BoxLayout.LINE_AXIS));
        
        JTabbedPane tabbePane = new JTabbedPane();
        
        //graf
        JPanel panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelCasCakaniaNaOpravu = new ChartPanel(chartCasCakaniaNaOpravu);
        panel.add(chartPanelCasCakaniaNaOpravu);
        is1.setMaximumSize(is1.getPreferredSize());
        is12.setMaximumSize(is12.getPreferredSize());
        panel.add(is1);
        panel.add(is12);
        tabbePane.addTab("Čakanie na opravu",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelCasCakaniaVRade = new ChartPanel(chartCasCakaniaVRade);
        panel.add(chartPanelCasCakaniaVRade);
        is2.setMaximumSize(is2.getPreferredSize());
        is22.setMaximumSize(is22.getPreferredSize());
        panel.add(is2);
        panel.add(is22);
        tabbePane.addTab("Čakanie v rade",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelZisk = new ChartPanel(chartZisk);
        panel.add(chartPanelZisk);
        is3.setMaximumSize(is3.getPreferredSize());
        is32.setMaximumSize(is32.getPreferredSize());
        panel.add(is3);
        panel.add(is32);
        tabbePane.addTab("Zisk",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelPocetLudiVRade = new ChartPanel(chartPocetLudiVRade);
        panel.add(chartPanelPocetLudiVRade);
        is4.setMaximumSize(is4.getPreferredSize());
        is42.setMaximumSize(is42.getPreferredSize());
        panel.add(is4);
        panel.add(is42);
        tabbePane.addTab("Fronta zákazníkov",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelVytazenostPrac1 = new ChartPanel(chartVytazenostPrac1);
        panel.add(chartPanelVytazenostPrac1);
        is5.setMaximumSize(is5.getPreferredSize());
        is52.setMaximumSize(is52.getPreferredSize());
        panel.add(is5);
        panel.add(is52);
        tabbePane.addTab("Vyťaženosť prac.1",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelVytazenostPrac2 = new ChartPanel(chartVytazenostPrac2);
        panel.add(chartPanelVytazenostPrac2);
        is6.setMaximumSize(is6.getPreferredSize());
        is62.setMaximumSize(is62.getPreferredSize());
        panel.add(is6);
        panel.add(is62);
        tabbePane.addTab("Vyťaženosť prac.2",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelObsluzeny = new ChartPanel(chartObsluzeny);
        panel.add(chartPanelObsluzeny);
        is7.setMaximumSize(is7.getPreferredSize());
        is72.setMaximumSize(is72.getPreferredSize());
        panel.add(is7);
        panel.add(is72);
        tabbePane.addTab("Obslúžení zákaznici",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelNeobsluzeny = new ChartPanel(chartNeobsluzeny);
        panel.add(chartPanelNeobsluzeny);
        is8.setMaximumSize(is8.getPreferredSize());
        is82.setMaximumSize(is82.getPreferredSize());
        panel.add(is8);
        panel.add(is82);
        tabbePane.addTab("Neobslúžení zákaznici",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelVytazenostPark1 = new ChartPanel(chartVytazenostPark1);
        panel.add(chartPanelVytazenostPark1);
        is9.setMaximumSize(is9.getPreferredSize());
        is92.setMaximumSize(is92.getPreferredSize());
        panel.add(is9);
        panel.add(is92);
        tabbePane.addTab("Vyťaženosť park1",panel);
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelVytazenostPark2 = new ChartPanel(chartVytazenostPark2);
        panel.add(chartPanelVytazenostPark2);
        is10.setMaximumSize(is10.getPreferredSize());
        is102.setMaximumSize(is102.getPreferredSize());
        panel.add(is10);
        panel.add(is102);
        tabbePane.addTab("Vyťaženosť park2",panel);
        
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        ChartPanel chartPanelPocetAutPredServisom = new ChartPanel(chartPocetAutPredServisom);
        panel.add(chartPanelPocetAutPredServisom);
        is11.setMaximumSize(is11.getPreferredSize());
        is112.setMaximumSize(is112.getPreferredSize());
        panel.add(is11);
        panel.add(is112);
        tabbePane.addTab("Park. pred servisom",panel);
        
        JPanel grafPanel = new JPanel(new BorderLayout());//new BorderLayout()
        grafPanel.setLayout(new BoxLayout(grafPanel, BoxLayout.PAGE_AXIS));
        
        JPanel textGraf = new JPanel();//new BorderLayout()
        textGraf.setLayout(new FlowLayout(FlowLayout.CENTER));
        
        panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel label2 = new JLabel("Hospodársky výsledok - porovnanie   ");
        panel.add(label2,BorderLayout.PAGE_START);
        panel.add(startButton2);
        textGraf.add(panel);
        /*
        panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        JLabel label2 = new JLabel("Počet pracovníkov 1. skupiny    ");
        panel.add(label2,BorderLayout.PAGE_START);
        panel.add(prac1Area2,BorderLayout.PAGE_START);
        textGraf.add(panel);
        
        panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Počet pracovníkov 2. skupiny    ");
        panel.add(label2,BorderLayout.PAGE_START);
        panel.add(prac2Area2,BorderLayout.PAGE_START);
        textGraf.add(panel);
        
        panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Reklama    ");
        panel.add(label2,BorderLayout.PAGE_START);
        panel.add(reklamaComboBox2,BorderLayout.PAGE_END);
        textGraf.add(panel);
        */
        //////////////////////////////////////////////////////////////////////////////
        
        JPanel topGraf = new JPanel(new BorderLayout());//new BorderLayout()
        topGraf.setLayout(new BoxLayout(topGraf, BoxLayout.LINE_AXIS));
        JPanel botGraf = new JPanel(new BorderLayout());//new BorderLayout()
        botGraf.setLayout(new BoxLayout(botGraf, BoxLayout.LINE_AXIS));
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
        //panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        
        JPanel panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Počet pracovníkov 1. skupiny    ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(prac1Area2,BorderLayout.PAGE_START);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Min. počet pracovníkov 2. skupiny    ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(prac2Area2Min,BorderLayout.PAGE_START);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Max. počet pracovníkov 2. skupiny    ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(prac2Area2Max,BorderLayout.PAGE_START);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Reklama ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(reklamaArea2,BorderLayout.PAGE_END);
        //panelPrac1.add(reklamaComboBox2,BorderLayout.PAGE_END);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Parkovisko 1   ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(park1Area2,BorderLayout.PAGE_END);
        park1Area2.setEditable(false);
        panel.add(panelPrac1);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Parkovisko 2   ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(park2Area2,BorderLayout.PAGE_END);
        park2Area2.setEditable(false);
        panel.add(panelPrac1);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Vymen parkoviska   ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(vymeneneCheckBox2,BorderLayout.PAGE_END);
        panel.add(panelPrac1);
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        topGraf.add(panel);
        
        
        
        
        ChartPanel chartPanel3 = new ChartPanel(chartHospodarskyVysledok[0]);
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(chartPanel3);
        textAreaHospodarskvyVysledok[0] = new JTextArea("",1,38);
        textAreaHospodarskvyVysledok[0].setMaximumSize(textAreaHospodarskvyVysledok[0].getPreferredSize());
        panel.add(textAreaHospodarskvyVysledok[0]);
        topGraf.add(panel);
        
        ChartPanel chartPanel4 = new ChartPanel(chartHospodarskyVysledok[1]);
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(chartPanel4);
        textAreaHospodarskvyVysledok[1] = new JTextArea("",1,38);
        textAreaHospodarskvyVysledok[1].setMaximumSize(textAreaHospodarskvyVysledok[1].getPreferredSize());
        panel.add(textAreaHospodarskvyVysledok[1]);
        topGraf.add(panel);
        ////////////////////////////////////////////
        
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
        //panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Počet pracovníkov 1. skupiny    ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(prac1Area3,BorderLayout.PAGE_START);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Min. počet pracovníkov 2. skupiny    ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(prac2Area3Min,BorderLayout.PAGE_START);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Max. počet pracovníkov 2. skupiny    ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(prac2Area3Max,BorderLayout.PAGE_START);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Reklama ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(reklamaArea3,BorderLayout.PAGE_END);
        //panelPrac1.add(reklamaComboBox3,BorderLayout.PAGE_END);
        panel.add(panelPrac1,BorderLayout.CENTER);
        
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Parkovisko 1   ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(park1Area3,BorderLayout.PAGE_END);
        park1Area3.setEditable(false);
        panel.add(panelPrac1);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Parkovisko 2   ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(park2Area3,BorderLayout.PAGE_END);
        park2Area3.setEditable(false);
        panel.add(panelPrac1);
        
        panelPrac1 = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label2 = new JLabel("Vymen parkoviska   ");
        panelPrac1.add(label2,BorderLayout.PAGE_START);
        panelPrac1.add(vymeneneCheckBox3,BorderLayout.PAGE_END);
        panel.add(panelPrac1);
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        botGraf.add(panel);
        
        
        ChartPanel chartPanel5 = new ChartPanel(chartHospodarskyVysledok[2]);
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(chartPanel5);
        textAreaHospodarskvyVysledok[2] = new JTextArea("",1,38);
        textAreaHospodarskvyVysledok[2].setMaximumSize(textAreaHospodarskvyVysledok[2].getPreferredSize());
        panel.add(textAreaHospodarskvyVysledok[2]);
        botGraf.add(panel);
        ChartPanel chartPanel6 = new ChartPanel(chartHospodarskyVysledok[3]);
        panel = new JPanel(new BorderLayout());
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(chartPanel6);
        textAreaHospodarskvyVysledok[3] = new JTextArea("",1,38);
        textAreaHospodarskvyVysledok[3].setMaximumSize(textAreaHospodarskvyVysledok[3].getPreferredSize());
        panel.add(textAreaHospodarskvyVysledok[3]);
        botGraf.add(panel);
        
        grafPanel.add(textGraf,BorderLayout.PAGE_START);
        grafPanel.add(topGraf,BorderLayout.CENTER);
        //mainPanel.add(botPanel, BorderLayout.CENTER);
        //grafPanel.add(botGraf,BorderLayout.CENTER);
        grafPanel.add(botGraf,BorderLayout.PAGE_END);
        tabbePane.addTab("Okolie",grafPanel);
        
        
        
        
        //tabulky
        tabbePane.addTab("Vykreslovanie1", botPanel);
        tabbePane.addTab("Vykreslovanie2", botPanel2);
        
        mainPanel.add(topPanel,BorderLayout.PAGE_START);
        //mainPanel.add(botPanel, BorderLayout.CENTER);
        mainPanel.add(tabbePane,BorderLayout.CENTER);
        
        topPanel.add(topTopPanel, BorderLayout.PAGE_START);
        topPanel.add(topTopPanel2, BorderLayout.CENTER);
        
        botPanel.add(topBotPanel, BorderLayout.PAGE_START);
        botPanel.add(botTopPanel, BorderLayout.CENTER);
        botPanel.add(botBotPanel, BorderLayout.PAGE_END);
        
        botPanel2.add(topBotPanel2, BorderLayout.PAGE_START);
        botPanel2.add(botTopPanel2, BorderLayout.CENTER);
        botPanel2.add(botBotPanel2, BorderLayout.PAGE_END);
        
        JScrollPane scrollPane;
        JScrollPane scrollPane2;

        topTopPanel.add(startButton);
        topTopPanel.add(pauseButton);
        
        topTopPanel.add(rezimCheckBox);    
        
        JLabel label = new JLabel();
        label = new JLabel("Rýchlosť");
        topTopPanel.add(label);
        topTopPanel.add(rychlost);
        
        /*
        label = new JLabel("Replikácie  ");
        topTopPanel.add(label);
        topTopPanel.add(replikacieProgres);
        */
        
/////////////////////////////////////////////////////////////////////////////////
        JPanel pan = new JPanel(new FlowLayout(FlowLayout.LEADING));
        label = new JLabel("Počet pracovníkov 1. skupiny    ");
        pan.add(label,BorderLayout.PAGE_START);
        pan.add(prac1Area,BorderLayout.PAGE_END);
        topTopPanel2.add(pan);
        
        label = new JLabel("Počet pracovníkov 2. skupiny    ");
        pan.add(label,BorderLayout.PAGE_START);
        pan.add(prac2Area,BorderLayout.PAGE_END);
        topTopPanel2.add(pan);
        
        label = new JLabel("Reklama    ");
        pan.add(label,BorderLayout.PAGE_START);
        //pan.add(reklamaComboBox,BorderLayout.PAGE_END);
        pan.add(reklamaArea1,BorderLayout.PAGE_END);
        topTopPanel2.add(pan);
        
        label = new JLabel("Parkovisko 1   ");
        pan.add(label,BorderLayout.PAGE_START);
        pan.add(park1Area,BorderLayout.PAGE_END);
        park1Area.setEditable(false);
        topTopPanel2.add(pan);
        
        label = new JLabel("Parkovisko 2   ");
        pan.add(label,BorderLayout.PAGE_START);
        pan.add(park2Area,BorderLayout.PAGE_END);
        park2Area.setEditable(false);
        topTopPanel2.add(pan);
        
        label = new JLabel("Vymen parkoviska   ");
        pan.add(label,BorderLayout.PAGE_START);
        pan.add(vymeneneCheckBox,BorderLayout.PAGE_END);
        topTopPanel2.add(pan);
/////////////////////////////////////////////////////////////////////////////////
        /*
        label = new JLabel("Trzby: ");
        topTopPanel.add(label);
        topTopPanel.add(trzbyField);
        
        label = new JLabel("Vydavky: ");
        topTopPanel.add(label);
        topTopPanel.add(vydavkyField);
        
        label = new JLabel("Zisk: ");
        topTopPanel.add(label);
        topTopPanel.add(zisField);
        
        */
        
        JPanel topBotPanel11 = new JPanel();
        topBotPanel11.setLayout(new FlowLayout(FlowLayout.LEADING));
        JPanel topBotPanel12 = new JPanel();
        topBotPanel12.setLayout(new FlowLayout(FlowLayout.LEADING));
        
        JPanel topBotPanel21 = new JPanel();
        topBotPanel21.setLayout(new FlowLayout(FlowLayout.LEADING));
        JPanel topBotPanel22 = new JPanel();
        topBotPanel22.setLayout(new FlowLayout(FlowLayout.LEADING));
        
        label = new JLabel("Simulačný čas  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(simCasArea1);
        topBotPanel11.add(scrollPane);
        
        label = new JLabel("Simulačný čas  ");
        topBotPanel21.add(label);
        scrollPane = new JScrollPane(simCasArea2);
        topBotPanel21.add(scrollPane);
        /*
        label = new JLabel("Počet obslúžených  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(pocetObsluzenychArea);
        topBotPanel11.add(scrollPane);
        
        label = new JLabel("Počet neobslúžených  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(pocetNeobsluzenychArea);
        topBotPanel11.add(scrollPane);
        
        label = new JLabel("Priemerná dĺžka radu  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(priemDlzkaRadu1Area);
        topBotPanel11.add(scrollPane);
        
        label = new JLabel("Vyťaženosť prac. skupiny 1  ");
        topBotPanel12.add(label);
        topBotPanel12.add(vyuzitiePrac1Progres);
        
        label = new JLabel("Vyťaženosť prac. skupiny 2  ");
        topBotPanel12.add(label);
        topBotPanel12.add(vyuzitiePrac2Progres);
        
        label = new JLabel("Tržby  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(trzbyArea);
        topBotPanel11.add(scrollPane);
        
        label = new JLabel("Výdavky  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(vydavkyArea);
        topBotPanel11.add(scrollPane);
        
        label = new JLabel("Zisk  ");
        topBotPanel11.add(label);
        scrollPane = new JScrollPane(ziskArea);
        topBotPanel11.add(scrollPane);
        */
        
        topBotPanel.add(topBotPanel11);
        topBotPanel.add(topBotPanel12);
        
        topBotPanel2.add(topBotPanel21);
        topBotPanel2.add(topBotPanel22);
        
        scrollPane= new JScrollPane(tabulkaZakaznici);
        scrollPane2= new JScrollPane(tabulkaZakaznici2);
        botTopPanel.add(scrollPane);
        botTopPanel2.add(scrollPane2);
        
        scrollPane= new JScrollPane(tabulkaPracovnici1);
        scrollPane2= new JScrollPane(tabulkaPracovnici12);
        botTopPanel.add(scrollPane);
        botTopPanel2.add(scrollPane2);
        
        scrollPane= new JScrollPane(tabulkaPracovnici2);
        scrollPane2= new JScrollPane(tabulkaPracovnici22);
        botTopPanel.add(scrollPane);
        botTopPanel2.add(scrollPane2);
        
        scrollPane= new JScrollPane(tabulkaAuta);
        scrollPane2= new JScrollPane(tabulkaAuta2);
        botTopPanel.add(scrollPane);
        botTopPanel2.add(scrollPane2);
        
        scrollPane= new JScrollPane(tabulkaParkovisko1);
        scrollPane2= new JScrollPane(tabulkaParkovisko12);
        botBotPanel.add(scrollPane);
        botBotPanel2.add(scrollPane2);
        
        scrollPane= new JScrollPane(tabulkaParkovisko2);
        scrollPane2= new JScrollPane(tabulkaParkovisko22);
        botBotPanel.add(scrollPane);
        botBotPanel2.add(scrollPane2);
        
        frame.add(mainPanel);
        frame.pack();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        
        
        
        
    }

    private void initSimulation() {
        
        simulacia = new SimulaciaServis();
        simulacia.setHlavna(true);
        simulacia2 = new SimulaciaServis();
        simulacia2.setHlavna(false);
        
        simulacia.registerDelegate(this);
        simulacia2.registerDelegate(this);
        
        simulacia.onSimulationWillStart((sim) -> {
            //System.out.println("Zaciatok simulacie");
        });
        simulacia2.onSimulationWillStart((sim) -> {
            //System.out.println("Zaciatok simulacie");
        });
        simulacia.onReplicationWillStart((sim) -> {
            /*
            if(rezimCheckBox.isSelected()) {
                simulacia.setMaxSimSpeed();
            } else {
                //simulacia.setSimSpeed(0.1d, 0.0000000005d);
                simulacia.setSimSpeed(10000000.1d, 0.0000000005d);
            }
            */
            zmenRychlost();
            //System.out.println("Zaciatok replikacie c. "+simulacia.currentReplication());
        });
        simulacia2.onReplicationWillStart((sim) -> {
            /*
            if(rezimCheckBox.isSelected()) {
                simulacia2.setMaxSimSpeed();
            } else {
                //simulacia.setSimSpeed(0.1d, 0.0000000005d);
                simulacia2.setSimSpeed(10000000.1d, 0.0000000005d);
            }
            */
            zmenRychlost();
        });
        
        simulacia.onReplicationDidFinish((sim) -> {
            //System.out.println("Koniec replikacie c. "+simulacia.currentReplication());
            /*
            if(simulacia2.isPaused() && simulacia.currentReplication() >= simulacia2.currentReplication()) {
                simulacia2.resumeSimulation();
            }
            */
        });
        simulacia2.onReplicationDidFinish((sim) -> {
            //System.out.println("Koniec replikacie c. "+simulacia2.currentReplication());
            /*
            if(simulacia2.currentReplication() >= simulacia.currentReplication()) {
                simulacia2.pauseSimulation();
            }
            */
            //jTextArea1.setText(jTextArea1.getText()+"\n\n");
        });
        simulacia.onSimulationDidFinish((sim) -> {
            //System.out.println("Koniec simulacie");
        });
        simulacia2.onSimulationDidFinish((sim) -> {
            //System.out.println("Koniec simulacie");
            startButton.setText("start");
        });
    }

    @Override
    public void simStateChanged(Simulation sim, SimState state) {
        switch(state) { 
            case running:
                //System.out.println("sim state changed running");
                if(!rezimCheckBox.isSelected()) {
                    if(((SimulaciaServis)sim).isHlavna()) {
                        SwingUtilities.invokeLater(() -> {
                            tabulkaZakaznici.setModel(new ZakazniciTableModel(simulacia,simulacia.agentServisu(), simulacia.agentRampy(), simulacia.agentKancelarie(), simulacia.agentDielne(), simulacia.agentParkovisk()));
                            tabulkaPracovnici1.setModel(new Pracovnici1TableModel(simulacia,simulacia.agentServisu(), simulacia.agentRampy(), simulacia.agentKancelarie(), simulacia.agentDielne(), simulacia.agentParkovisk()));
                            tabulkaPracovnici2.setModel(new Pracovnici2TableModel(simulacia,simulacia.agentServisu(), simulacia.agentRampy(), simulacia.agentKancelarie(), simulacia.agentDielne(), simulacia.agentParkovisk()));
                            tabulkaAuta.setModel(new AutaTableModel(simulacia,simulacia.agentServisu(), simulacia.agentRampy(), simulacia.agentKancelarie(), simulacia.agentDielne(), simulacia.agentParkovisk()));
                            tabulkaParkovisko1.setModel(new ParkoviskoTableModel(simulacia,simulacia.agentServisu(), simulacia.agentRampy(), simulacia.agentKancelarie(), simulacia.agentDielne(), simulacia.agentParkovisk()));
                            tabulkaParkovisko2.setModel(new Parkovisko2TableModel(simulacia,simulacia.agentServisu(), simulacia.agentRampy(), simulacia.agentKancelarie(), simulacia.agentDielne(), simulacia.agentParkovisk()));

                        });
                    } else {
                        SwingUtilities.invokeLater(() -> {
                            tabulkaZakaznici2.setModel(new ZakazniciTableModel(simulacia2,simulacia2.agentServisu(), simulacia2.agentRampy(), simulacia2.agentKancelarie(), simulacia2.agentDielne(), simulacia2.agentParkovisk()));
                            tabulkaPracovnici12.setModel(new Pracovnici1TableModel(simulacia2,simulacia2.agentServisu(), simulacia2.agentRampy(), simulacia2.agentKancelarie(), simulacia2.agentDielne(), simulacia2.agentParkovisk()));
                            tabulkaPracovnici22.setModel(new Pracovnici2TableModel(simulacia2,simulacia2.agentServisu(), simulacia2.agentRampy(), simulacia2.agentKancelarie(), simulacia2.agentDielne(), simulacia2.agentParkovisk()));
                            tabulkaAuta2.setModel(new AutaTableModel(simulacia2,simulacia2.agentServisu(), simulacia2.agentRampy(), simulacia2.agentKancelarie(), simulacia2.agentDielne(), simulacia2.agentParkovisk()));
                            tabulkaParkovisko12.setModel(new ParkoviskoTableModel(simulacia2,simulacia2.agentServisu(), simulacia2.agentRampy(), simulacia2.agentKancelarie(), simulacia2.agentDielne(), simulacia2.agentParkovisk()));
                            tabulkaParkovisko22.setModel(new Parkovisko2TableModel(simulacia2,simulacia2.agentServisu(), simulacia2.agentRampy(), simulacia2.agentKancelarie(), simulacia2.agentDielne(), simulacia2.agentParkovisk()));

                        });
                    }
                }
            break;

            case replicationRunning:
               //System.out.println("replication running");
                
            break;

            case replicationStopped:
                //System.out.println("replication stopped");
                aktualizujGrafy(sim);
                aktualizujTextField(sim);
                aktualizujIntervaliSpolahlivosti(sim);
                
            break;

            case stopped:
                //System.out.println("simulation stopped");
            break;
        }
    }

    @Override
    public void refresh(Simulation sim) {
        SimulaciaServis s = (SimulaciaServis)sim;
        if(!rezimCheckBox.isSelected()) {
            if(s.isHlavna()) {
                double cas = s.invokeSync(() -> s.agentOkolia().mySim().currentTime());
                int den =(int) cas / 28800;//28800 = 8 hodin
                cas = cas%28800;
                int hodina =(int) (cas / 3600);// 3600 hodina
                cas = cas%3600;
                int minuta = (int) (cas /60);
                cas = cas%60;
                int sekunda = (int)(cas);
                int milisekunda;
                if(sekunda >0) 
                    milisekunda= (int)((cas%sekunda)*1000000000);
                else
                    milisekunda = (int) (cas*1000000000) ;

                String text = den+". deň"+"   "+hodina+"h:"+minuta+"m:"+sekunda+"."+milisekunda+"s";
                simCasArea1.setText(text);

                tabulkaZakaznici.revalidate();
                tabulkaPracovnici1.revalidate();
                tabulkaPracovnici2.revalidate();
                tabulkaAuta.revalidate();
                tabulkaParkovisko1.revalidate();
                tabulkaParkovisko2.revalidate();
                
                frame.repaint();

            } else {
                double cas = s.invokeSync(() -> s.agentOkolia().mySim().currentTime());
                int den =(int) cas / 28800;//28800 = 8 hodin
                cas = cas%28800;
                int hodina =(int) (cas / 3600);// 3600 hodina
                cas = cas%3600;
                int minuta = (int) (cas /60);
                cas = cas%60;
                int sekunda = (int)(cas);
                int milisekunda;
                if(sekunda >0) 
                    milisekunda= (int)((cas%sekunda)*1000000000);
                else
                    milisekunda = (int) (cas*1000000000) ;

                String text = den+". deň"+"   "+hodina+"h:"+minuta+"m:"+sekunda+"."+milisekunda+"s";
                simCasArea2.setText(text);

                tabulkaZakaznici2.revalidate();
                tabulkaPracovnici12.revalidate();
                tabulkaPracovnici22.revalidate();
                tabulkaAuta2.revalidate();
                tabulkaParkovisko12.revalidate();
                tabulkaParkovisko22.revalidate();

                frame.repaint();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == startButton) {
            if(simulacia.isRunning()) {
                
                simulacia.stopSimulation();
                simulacia2.stopSimulation();
                startButton.setText("start");
                
            } else {
                
                series1.clear();
                series21.clear();
                series2.clear();
                series22.clear();
                series31.clear();
                series32.clear();
                series41.clear();
                series42.clear();
                series51.clear();
                series52.clear();
                series61.clear();
                series62.clear();
                series71.clear();
                series72.clear();
                series81.clear();
                series82.clear();
                series91.clear();
                series92.clear();
                series101.clear();
                series102.clear();
                series111.clear();
                series112.clear();
                /*
                for(int i=0;i<4;i++) {
                    seriesCelkNaklady[i].clear();
                    seriesTrzby[i].clear();
                    seriesZisk[i].clear();
                }
                */
                
                startButton.setText("stop");
                simulate();
                
            }
        }
        else if(e.getSource() == pauseButton) {
            
            if(simulacia.isRunning()) {
                if(simulacia.isPaused()) {
                    
                    pauseButton.setText("pause");
                    simulacia.resumeSimulation();
                    simulacia2.resumeSimulation();
                    
                } else {
                    
                    pauseButton.setText("resume");
                    simulacia.pauseSimulation();
                    simulacia2.pauseSimulation();
                
                 }
            } 
        }
        else if(e.getSource() == rezimCheckBox) {
            zmenRychlost();
        }
        else if(e.getSource() == startButton2) {
            for(int i=0;i<4;i++) {
                seriesCelkNaklady[i].clear();
                seriesTrzby[i].clear();
                seriesZisk[i].clear();
            }
            startButton2.setEnabled(false);
            simulate2();
        }
        else if(e.getSource() == vymeneneCheckBox) {
            if(vymeneneCheckBox.isSelected()) {
                park1Area.setText("10");
                park2Area.setText("6");
            } else {
                park1Area.setText("6");
                park2Area.setText("10");
            }
            
        }
        else if(e.getSource() == vymeneneCheckBox2) {
            if(vymeneneCheckBox2.isSelected()) {
                park1Area2.setText("10");
                park2Area2.setText("6");
            } else {
                park1Area2.setText("6");
                park2Area2.setText("10");
            }
            
        }
        else if(e.getSource() == vymeneneCheckBox3) {
            if(vymeneneCheckBox3.isSelected()) {
                park1Area3.setText("10");
                park2Area3.setText("6");
            } else {
                park1Area3.setText("6");
                park2Area3.setText("10");
            }
            
        }
    }

    private void simulate() {
        int pocetPrac1 = Integer.parseInt(prac1Area.getText());
        int pocetPrac2 = Integer.parseInt(prac2Area.getText());
        int pocetParkMiest1 = Integer.parseInt(park1Area.getText());//sem 2 =10000
        int pocetParkMiest2 = Integer.parseInt(park2Area.getText());//sem 2 = 1000
        int prioritaZadaniaObjednavky = 0;
        int prioritaPreparkovaniaAutaZDielne = 1;
        int prioritaZadaniaObjednavky2 = 1;
        int prioritaPreparkovaniaAutaZDielne2 = 0;
        boolean vymenenePark;
        if(vymeneneCheckBox.isSelected()) {
            vymenenePark = true;
        } else {
            vymenenePark = false;
        }
        cenaReklamy = Double.parseDouble(reklamaArea1.getText());
        //int reklama = (Integer.parseInt(reklamaArea1.getText()))/1000;
        simulacia.nastavParametre(pocetPrac1,pocetPrac2,pocetParkMiest1,pocetParkMiest2,prioritaZadaniaObjednavky,prioritaPreparkovaniaAutaZDielne,cenaReklamy,vymenenePark);
        simulacia2.nastavParametre(pocetPrac1,pocetPrac2,pocetParkMiest1,pocetParkMiest2,prioritaZadaniaObjednavky2,prioritaPreparkovaniaAutaZDielne2,cenaReklamy,vymenenePark);
        
        simulacia.simulateAsync(1000, Konstanty.dlzkaReplikacie);
        simulacia2.simulateAsync(1000, Konstanty.dlzkaReplikacie);
        
        //simulacia.simulate(1000, (80d*8d*60d*60d));
    }
    private void simulate2() {

        simulujHospodarskyVysledok(0);
        simulujHospodarskyVysledok(1);
        simulujHospodarskyVysledok(2);
        simulujHospodarskyVysledok(3);
    }

    private void initCharts() {
        chartCasCakaniaNaOpravu = ChartFactory.createXYLineChart("Priemerný čas čakania na opravu", "Replikácia", "Čas  [s]", dataset, PlotOrientation.VERTICAL, true, true, false);
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        chartCasCakaniaNaOpravu.getPlot().setBackgroundPaint(Color.WHITE);
        chartCasCakaniaNaOpravu.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartCasCakaniaNaOpravu.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartCasCakaniaNaOpravu.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        /////////////
        chartCasCakaniaVRade = ChartFactory.createXYLineChart("Priemerný čas čakania v rade", "Replikácia", "Čas  [s]", dataset2, PlotOrientation.VERTICAL, true, true, false);
        dataset2.addSeries(series21);
        dataset2.addSeries(series22);
        chartCasCakaniaVRade.getPlot().setBackgroundPaint(Color.WHITE);
        chartCasCakaniaVRade.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartCasCakaniaVRade.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartCasCakaniaVRade.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartZisk = ChartFactory.createXYLineChart("Priemerný zisk", "Replikácia", "Zisk  [€]", dataset3, PlotOrientation.VERTICAL, true, true, false);
        dataset3.addSeries(series31);
        dataset3.addSeries(series32);
        chartZisk.getPlot().setBackgroundPaint(Color.WHITE);
        chartZisk.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartZisk.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartZisk.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartPocetLudiVRade = ChartFactory.createXYLineChart("Priemerný počet ľudí v rade", "Replikácia", "Počet", dataset4, PlotOrientation.VERTICAL, true, true, false);
        dataset4.addSeries(series41);
        dataset4.addSeries(series42);
        chartPocetLudiVRade.getPlot().setBackgroundPaint(Color.WHITE);
        chartPocetLudiVRade.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartPocetLudiVRade.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartPocetLudiVRade.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartVytazenostPrac1 = ChartFactory.createXYLineChart("Vyťaženosť pracovníkov skupiny 1", "Replikácia", "Vyťaženosť  [%]", dataset5, PlotOrientation.VERTICAL, true, true, false);
        dataset5.addSeries(series51);
        dataset5.addSeries(series52);
        chartVytazenostPrac1.getPlot().setBackgroundPaint(Color.WHITE);
        chartVytazenostPrac1.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartVytazenostPrac1.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartVytazenostPrac1.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartVytazenostPrac2 = ChartFactory.createXYLineChart("Vyťaženosť pracovníkov skupiny 2", "Replikácia", "Vyťaženosť  [%]", dataset6, PlotOrientation.VERTICAL, true, true, false);
        dataset6.addSeries(series61);
        dataset6.addSeries(series62);
        chartVytazenostPrac2.getPlot().setBackgroundPaint(Color.WHITE);
        chartVytazenostPrac2.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartVytazenostPrac2.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartVytazenostPrac2.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartObsluzeny = ChartFactory.createXYLineChart("Obslúžení zákazníci", "Replikácia", "Počet", dataset7, PlotOrientation.VERTICAL, true, true, false);
        dataset7.addSeries(series71);
        dataset7.addSeries(series72);
        chartObsluzeny.getPlot().setBackgroundPaint(Color.WHITE);
        chartObsluzeny.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartObsluzeny.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartObsluzeny.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartNeobsluzeny = ChartFactory.createXYLineChart("Neobslúžení zákazníci", "Replikácia", "Počet", dataset8, PlotOrientation.VERTICAL, true, true, false);
        dataset8.addSeries(series81);
        dataset8.addSeries(series82);
        chartNeobsluzeny.getPlot().setBackgroundPaint(Color.WHITE);
        chartNeobsluzeny.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartNeobsluzeny.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartNeobsluzeny.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartVytazenostPark1 = ChartFactory.createXYLineChart("Vyťaženosť parkoviska 1", "Replikácia", "Vyťaženosť  [%]", dataset9, PlotOrientation.VERTICAL, true, true, false);
        dataset9.addSeries(series91);
        dataset9.addSeries(series92);
        chartVytazenostPark1.getPlot().setBackgroundPaint(Color.WHITE);
        chartVytazenostPark1.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartVytazenostPark1.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartVytazenostPark1.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartVytazenostPark2 = ChartFactory.createXYLineChart("Vyťaženosť parkoviska 2", "Replikácia", "Vyťaženosť  [%]", dataset10, PlotOrientation.VERTICAL, true, true, false);
        dataset10.addSeries(series101);
        dataset10.addSeries(series102);
        chartVytazenostPark2.getPlot().setBackgroundPaint(Color.WHITE);
        chartVytazenostPark2.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartVytazenostPark2.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartVytazenostPark2.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        //////////////
        chartPocetAutPredServisom = ChartFactory.createXYLineChart("Počet aut pred servisom", "Replikácia", "Počet", dataset11, PlotOrientation.VERTICAL, true, true, false);
        dataset11.addSeries(series111);
        dataset11.addSeries(series112);
        chartPocetAutPredServisom.getPlot().setBackgroundPaint(Color.WHITE);
        chartPocetAutPredServisom.getXYPlot().setRangeGridlinePaint(Color.BLACK);
        chartPocetAutPredServisom.getXYPlot().setDomainGridlinePaint(Color.BLACK);
        ((NumberAxis)chartPocetAutPredServisom.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        
        //////////////
        String[] nazov = new String[4];
        int prac1 = Integer.parseInt(prac1Area2.getText());
        int prac11 = Integer.parseInt(prac1Area3.getText());
        int prac2Min = Integer.parseInt(prac2Area2Min.getText());;
        int prac2Max = Integer.parseInt(prac2Area2Max.getText());;
        int prac3Min = Integer.parseInt(prac2Area3Min.getText());;
        int prac3Max = Integer.parseInt(prac2Area3Max.getText());;
        nazov[0] = "Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac1+"\npočet pracovníkov skupiny 2:  <"+prac2Min+", "+prac2Max+">";
        nazov[1] = "Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac1+"\npočet pracovníkov skupiny 2:  <"+prac2Min+", "+prac2Max+">";
        nazov[2] = "Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac11+"\npočet pracovníkov skupiny 2:  <"+prac3Min+", "+prac3Max+">";
        nazov[3] = "Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac11+"\npočet pracovníkov sk.upiny 2:  <"+prac3Min+", "+prac3Max+">";      
        
        
        for(int i=0;i<4;i++) {
            seriesCelkNaklady[i] = new XYSeries("celkové náklady");
            seriesTrzby[i] = new XYSeries("tržby");
            seriesZisk[i] = new XYSeries("zisk");
            //seriesFixneNaklady[i] = new XYSeries(i+"fixné náklady");
            
            datasetHospodarskyVysledok[i] = new XYSeriesCollection();
            
            chartHospodarskyVysledok[i] = ChartFactory.createXYLineChart(nazov[i], "Počet pracovníkov sk.2", "€", datasetHospodarskyVysledok[i], PlotOrientation.VERTICAL, true, true, false);
            datasetHospodarskyVysledok[i].addSeries(seriesCelkNaklady[i]);
            datasetHospodarskyVysledok[i].addSeries(seriesTrzby[i]);
            datasetHospodarskyVysledok[i].addSeries(seriesZisk[i]);
            //datasetHospodarskyVysledok[i].addSeries(seriesFixneNaklady[i]);
            chartHospodarskyVysledok[i].getPlot().setBackgroundPaint(Color.WHITE);
            chartHospodarskyVysledok[i].getXYPlot().setRangeGridlinePaint(Color.BLACK);
            chartHospodarskyVysledok[i].getXYPlot().setDomainGridlinePaint(Color.BLACK);
            ((NumberAxis)chartHospodarskyVysledok[i].getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        }
    }

    private void aktualizujGrafy(Simulation sim) {
        if(sim.currentReplication() > sim.replicationCount()*0.1) {
            SimulaciaServis s = (SimulaciaServis) sim;
            if(s.isHlavna()) {
                series1.add(s.currentReplication(),s.casCakaniaNaOpravyStat().mean());
                series21.add(s.currentReplication(),s.casCakaniaVRadeNaZadanieObjednavkyStat().mean());
                double zisk = s.trzbyStat().mean() - s.getPocetPracovnikovSkupiny1()*1150d - s.getPocetPracovnikovSkupiny2()*1600d -7000d - cenaReklamy;
                series31.add(s.currentReplication(),zisk);
                series41.add(s.currentReplication(),s.dlzkaRaduNaZO().mean());
                series51.add(s.currentReplication(),s.priemerVyuzitiePrac1().mean()*100d);
                series61.add(s.currentReplication(),s.priemerVyuzitiePrac2().mean()*100d);
                series71.add(s.currentReplication(),s.priemerObsluzenychStat().mean());
                series81.add(s.currentReplication(),s.priemerNeobsluzenychStat().mean());
                series91.add(s.currentReplication(),s.priemerVyuzPark1().mean()*100d);
                series101.add(s.currentReplication(),s.priemerVyuzPark2().mean()*100d);
                series111.add(s.currentReplication(),s.priemerPocetAutPredServisom().mean());
            } else {
                series2.add(s.currentReplication(),s.casCakaniaNaOpravyStat().mean());
                series22.add(s.currentReplication(),s.casCakaniaVRadeNaZadanieObjednavkyStat().mean());
                double zisk = s.trzbyStat().mean() - s.getPocetPracovnikovSkupiny1()*1150d - s.getPocetPracovnikovSkupiny2()*1600d -7000d- cenaReklamy;
                series32.add(s.currentReplication(),zisk);
                series42.add(s.currentReplication(),s.dlzkaRaduNaZO().mean());
                series52.add(s.currentReplication(),s.priemerVyuzitiePrac1().mean()*100d);
                series62.add(s.currentReplication(),s.priemerVyuzitiePrac2().mean()*100d);
                series72.add(s.currentReplication(),s.priemerObsluzenychStat().mean());
                series82.add(s.currentReplication(),s.priemerNeobsluzenychStat().mean());
                series92.add(s.currentReplication(),s.priemerVyuzPark1().mean()*100d);
                series102.add(s.currentReplication(),s.priemerVyuzPark2().mean()*100d);
                series112.add(s.currentReplication(),s.priemerPocetAutPredServisom().mean());
            }
        }
    }

    private void initColors() {
        farby = new Color[100];
        generatorR = new Random();
        generatorG = new Random();
        generatorB = new Random();
        for(int i=0;i<farby.length;i++) {
            //100+ iba svetlejsie
            farby[i] = new Color(100+generatorR.nextInt(155),100+generatorG.nextInt(155),100+generatorB.nextInt(155));
        }
    }

    private void zmenRychlost() {
        int value = rychlost.getValue();
        if(!rezimCheckBox.isSelected()) {
            simulacia.setSimSpeed( value * 0.001d, 0.001d);
            simulacia2.setSimSpeed(value * 0.001d, 0.001d);
 /*
            if(value < 1) {
                simulacia.setSimSpeed(0.00001d, 0.00001d);
                simulacia2.setSimSpeed(0.001d, 0.001d);
            } else {
                if(value < 2) {
                    simulacia.setSimSpeed(0.01d, 0.0005d);
                    simulacia2.setSimSpeed(0.01d, 0.0005d);
                } else {
                    if(value < 3) {
                        simulacia.setSimSpeed(0.1d, 0.0005d);
                        simulacia2.setSimSpeed(0.01d, 0.0005d);
                    } else {
                        simulacia.setSimSpeed(10.1d, 0.00005d);
                        simulacia2.setSimSpeed(10.1d, 0.00005d);
                    }
                }
                
            }
*/
        } else {
            simulacia.setMaxSimSpeed();
            simulacia2.setMaxSimSpeed();
        }
        
    }

    private void aktualizujTextField(Simulation sim) {
        SimulaciaServis s = (SimulaciaServis) sim;
        if(s.isHlavna()) {      
            double trzba = s.trzbyStat().mean();
            double vydavky = s.agentDielne().pocetPracovnikov2()*1600d+s.agentKancelarie().pocetPracovnikov1()*1150d+7000d;
            double zisk = trzba - vydavky;
            
            trzbyArea.setText(trzba+"");
            vydavkyArea.setText(vydavky+"");
            ziskArea.setText(zisk+"");
        } else {      
            double trzba = s.trzbyStat().mean();
            double vydavky = s.agentDielne().pocetPracovnikov2()*1600d+s.agentKancelarie().pocetPracovnikov1()*1150d+7000d;
            double zisk = trzba - vydavky;
            
            trzbyArea2.setText(trzba+"");
            vydavkyArea2.setText(vydavky+"");
            ziskArea2.setText(zisk+"");
            
        }
    }

    //priorita 0 
    //priorita 1
    public void simulujHospodarskyVysledok(int i) {
        int prac1 = Integer.parseInt(prac1Area2.getText());
        int prac11 = Integer.parseInt(prac1Area3.getText());
        
        int prac2Min = Integer.parseInt(prac2Area2Min.getText());;
        int prac2Max = Integer.parseInt(prac2Area2Max.getText());;
        int prac3Min = Integer.parseInt(prac2Area3Min.getText());;
        int prac3Max = Integer.parseInt(prac2Area3Max.getText());;
        int park11 = Integer.parseInt(park1Area2.getText());
        int park21 = Integer.parseInt(park2Area2.getText());
        int park12 = Integer.parseInt(park1Area3.getText());
        int park22 = Integer.parseInt(park2Area3.getText());
        double reklama11 = (Double.parseDouble(reklamaArea2.getText()));
        double reklama21 = (Double.parseDouble(reklamaArea3.getText()));
        SimulaciaServis app = new SimulaciaServis();
        if(i == 0) {
            chartHospodarskyVysledok[0].setTitle("Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac1+"\npočet pracovníkov skupiny 2:  <"+prac2Min+", "+prac2Max+">");
            app.nastavParametre(prac1, prac2Min, park11, park21, 0, 1, reklama11,false);
        }
        if(i == 1) {
            chartHospodarskyVysledok[1].setTitle("Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac1+"\npočet pracovníkov skupiny 2:  <"+prac2Min+", "+prac2Max+">");
            app.nastavParametre(prac1, prac2Min, park11, park21, 1, 0, reklama11,true);
        }
        if(i == 2) {
            chartHospodarskyVysledok[2].setTitle("Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac11+"\npočet pracovníkov skupiny 2:  <"+prac3Min+", "+prac2Max+">");
            app.nastavParametre(prac11, prac3Min, park12, park22, 0, 1, reklama21,false);
        }
        if(i == 3) {
            chartHospodarskyVysledok[3].setTitle("Hospodársky výsledok\npočet pracovníkov skupiny 1: "+prac11+"\npočet pracovníkov skupiny 2:  <"+prac3Min+", "+prac3Max+">");
            app.nastavParametre(prac11, prac3Min, park12, park22, 1, 0, reklama21,false);
        }
        app.simulateAsync(200, Konstanty.dlzkaReplikacie);
        app.onSimulationDidFinish((t) -> {
            double vydavky1 = app.getPocetPracovnikovSkupiny1()*1150d;
            double vydavky2 = app.getPocetPracovnikovSkupiny2()*1600d;
            double fixne = 7000;
            double celkoveNaklady = fixne + vydavky1 + vydavky2;
            if(i<2)
                celkoveNaklady += reklama11;
            else
                celkoveNaklady += reklama21;
            double trzby = app.trzbyStat().mean();
            seriesCelkNaklady[i].add(app.getPocetPracovnikovSkupiny2(),celkoveNaklady);
            seriesTrzby[i].add(app.getPocetPracovnikovSkupiny2(),trzby);
            seriesZisk[i].add(app.getPocetPracovnikovSkupiny2(),trzby -celkoveNaklady);
            //seriesFixneNaklady[i].add(app.getPocetPracovnikovSkupiny2(),fixne);
            if(i == 0) { 
                app.nastavParametre(prac1, app.getPocetPracovnikovSkupiny2()+1, park11, park21, 0, 1, reklama11,false);
            }
            if(i == 1) { 
                app.nastavParametre(prac1, app.getPocetPracovnikovSkupiny2()+1, park11, park21, 1, 0, reklama11,true);
            }
            if(i == 2) { 
                app.nastavParametre(prac11, app.getPocetPracovnikovSkupiny2()+1, park12, park22, 0, 1, reklama21,false);
            }
            if(i == 3) { 
                app.nastavParametre(prac11, app.getPocetPracovnikovSkupiny2()+1, park12, park22, 1, 0, reklama21,false);
            }
            double max = -999999;
            int index = -1;
            for(int idx=0;idx<seriesZisk[i].getItemCount();idx++) {
                if(seriesZisk[i].getY(idx).doubleValue() > max) {
                    max = seriesZisk[i].getY(idx).doubleValue();
                    index = idx;
                }
            }
            if(i<2)
                textAreaHospodarskvyVysledok[i].setText("Maximálny zisk je: "+seriesZisk[i].getY(index)+" pre počet pracovníkov skupiny 2: "+(index+prac2Min));
            else
                textAreaHospodarskvyVysledok[i].setText("Maximálny zisk je: "+seriesZisk[i].getY(index)+" pre počet pracovníkov skupiny 2: "+(index+prac3Min)); 
            if(i<2 && (app.getPocetPracovnikovSkupiny2())<=prac2Max) {
                app.simulateAsync(200, Konstanty.dlzkaReplikacie);
            } else {
                if(i>1 && (app.getPocetPracovnikovSkupiny2())<=prac3Max) {
                    app.simulateAsync(200, Konstanty.dlzkaReplikacie);
                } else {
                    startButton2.setText("start");
                    startButton2.setEnabled(true);
                    /*vypis pre grafy excelu
                    for(int a=0;a<4;a++) {

                        for(int j=0;j<seriesZisk[a].getItemCount();j++) {
                            System.out.println(seriesZisk[a].getY(j));
                        }

                        for(int j=0;j<seriesCelkNaklady[a].getItemCount();j++) {
                            System.out.println(seriesCelkNaklady[a].getY(j));
                        }

                        for(int j=0;j<seriesTrzby[a].getItemCount();j++) {
                            System.out.println(seriesTrzby[a].getY(j));
                        }
                    
                    }
                    */
                    
                }
            }
            
        });
    }

    private void aktualizujIntervaliSpolahlivosti(Simulation sim) {
        SimulaciaServis s = (SimulaciaServis) sim;
        if(s.currentReplication() > 3) {
            if(s.isHlavna()) {
                is1.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+s.casCakaniaNaOpravyStat().confidenceInterval_90()[0]+"; "+s.casCakaniaNaOpravyStat().confidenceInterval_90()[1]+")");
                is2.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+s.casCakaniaVRadeNaZadanieObjednavkyStat().confidenceInterval_90()[0]+"; "+s.casCakaniaVRadeNaZadanieObjednavkyStat().confidenceInterval_90()[1]+")");
                
                double vydavky =s.getPocetPracovnikovSkupiny1()*1150d + s.getPocetPracovnikovSkupiny2()*1600d +7000d+cenaReklamy;
                is3.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.trzbyStat().confidenceInterval_90()[0] - vydavky)+"; "+(s.trzbyStat().confidenceInterval_90()[1] - vydavky)+")");
                is4.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.dlzkaRaduNaZO().confidenceInterval_90()[0] )+"; "+(s.dlzkaRaduNaZO().confidenceInterval_90()[1] )+")");
                is5.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerVyuzitiePrac1().confidenceInterval_90()[0]*100d )+"; "+(s.priemerVyuzitiePrac1().confidenceInterval_90()[1]*100d )+")");
                is6.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerVyuzitiePrac2().confidenceInterval_90()[0]*100d )+"; "+(s.priemerVyuzitiePrac2().confidenceInterval_90()[1]*100d )+")");
                is7.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerObsluzenychStat().confidenceInterval_90()[0] )+"; "+(s.priemerObsluzenychStat().confidenceInterval_90()[1] )+")");
                is8.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerNeobsluzenychStat().confidenceInterval_90()[0] )+"; "+(s.priemerNeobsluzenychStat().confidenceInterval_90()[1] )+")");
                is9.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerVyuzPark1().confidenceInterval_90()[0]*100d )+"; "+(s.priemerVyuzPark1().confidenceInterval_90()[1]*100d )+")");
                is10.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerVyuzPark2().confidenceInterval_90()[0]*100d )+"; "+(s.priemerVyuzPark2().confidenceInterval_90()[1]*100d )+")");
                is11.setText("Pracovník 1 uprednostňuje preparkovanie z parkoviska 2 pred zadaním novej objednávky:  90% interval spoľahlivosti ("+(s.priemerPocetAutPredServisom().confidenceInterval_90()[0])+"; "+(s.priemerPocetAutPredServisom().confidenceInterval_90()[1] )+")");
            
            } else {
                is12.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+s.casCakaniaNaOpravyStat().confidenceInterval_90()[0]+"; "+s.casCakaniaNaOpravyStat().confidenceInterval_90()[1]+")");
                is22.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+s.casCakaniaVRadeNaZadanieObjednavkyStat().confidenceInterval_90()[0]+"; "+s.casCakaniaVRadeNaZadanieObjednavkyStat().confidenceInterval_90()[1]+")");
                double vydavky =s.getPocetPracovnikovSkupiny1()*1150d + s.getPocetPracovnikovSkupiny2()*1600d +7000d+cenaReklamy;
                is32.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.trzbyStat().confidenceInterval_90()[0] - vydavky)+"; "+(s.trzbyStat().confidenceInterval_90()[1] - vydavky)+")");
                is42.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.dlzkaRaduNaZO().confidenceInterval_90()[0])+"; "+(s.dlzkaRaduNaZO().confidenceInterval_90()[1])+")");
                is52.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerVyuzitiePrac1().confidenceInterval_90()[0]*100d)+"; "+(s.priemerVyuzitiePrac1().confidenceInterval_90()[1]*100d)+")");
                is62.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerVyuzitiePrac2().confidenceInterval_90()[0]*100d)+"; "+(s.priemerVyuzitiePrac2().confidenceInterval_90()[1]*100d)+")");
                is72.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerObsluzenychStat().confidenceInterval_90()[0])+"; "+(s.priemerObsluzenychStat().confidenceInterval_90()[1])+")");
                is82.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerNeobsluzenychStat().confidenceInterval_90()[0])+"; "+(s.priemerNeobsluzenychStat().confidenceInterval_90()[1])+")");
                is92.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerVyuzPark1().confidenceInterval_90()[0]*100d)+"; "+(s.priemerVyuzPark1().confidenceInterval_90()[1]*100d)+")");
                is102.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerVyuzPark2().confidenceInterval_90()[0]*100d)+"; "+(s.priemerVyuzPark2().confidenceInterval_90()[1]*100d)+")");
                is112.setText("Pracovník 1 uprednostňuje zadanie novej objednávky pred preparkovaním z parkoviska 2:  90% interval spoľahlivosti ("+(s.priemerPocetAutPredServisom().confidenceInterval_90()[0])+"; "+(s.priemerPocetAutPredServisom().confidenceInterval_90()[1])+")");
            }
        }
    }
}
