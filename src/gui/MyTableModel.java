/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import java.awt.Color;
import java.util.Random;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Robo
 */
public abstract class MyTableModel implements TableModel {
    
    private AgentServisu agentServisu;
    private AgentRampy agentRampy;
    private AgentKancelarie agentKancelarie;
    private AgentDielne agentDielne;
    private AgentParkovisk agentParkovisk;
    
    private Simulation sim;

        public MyTableModel(Simulation sim, AgentServisu agentServisu,AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
            this.sim = sim;
            this.agentServisu = agentServisu;
            this.agentRampy = agentRampy;
            this.agentKancelarie = agentKancelarie;
            this.agentDielne = agentDielne;
            this.agentParkovisk = agentParkovisk;
        }
        
	@Override
	public Class<?> getColumnClass(int columnIndex)
	{
		return String.class;
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) { 
            return false; 
        }

	@Override
	public void addTableModelListener(TableModelListener arg0) { 
            
        }
	@Override
	public void removeTableModelListener(TableModelListener arg0) {
            
        }
	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
            
        }

    /**
     * @return the agentRampy
     */
    public AgentRampy agentRampy() {
        return agentRampy;
    }

    /**
     * @return the agentKancelarie
     */
    public AgentKancelarie agentKancelarie() {
        return agentKancelarie;
    }

    /**
     * @return the agentDielne
     */
    public AgentDielne agentDielne() {
        return agentDielne;
    }
    
    public AgentParkovisk agentParkovisk() {
        return agentParkovisk;
    }

    /**
     * @return the agentServisu
     */
    public AgentServisu agentServisu() {
        return agentServisu;
    }

    /**
     * @return the sim
     */
    public Simulation sim() {
        return sim;
    }
}
