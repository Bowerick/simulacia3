/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Auto;
import entity.Zakaznik;
import java.util.List;
import simulacia.Id;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class AutaTableModel extends MyTableModel {

    final int id = 0;
    final int stav = 1;

    public AutaTableModel(Simulation sim, AgentServisu agentServisu,AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
        super(sim, agentServisu, agentRampy, agentKancelarie, agentDielne, agentParkovisk);
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case id:
                return "id";
            case stav:
                return "stav";
        }
        return null;
    }
    
    @Override
    public int getRowCount() {
        /*10.05.2017
        SimulaciaServis sim =(SimulaciaServis) agentRampy().mySim();
        int a = sim.invokeSync(() -> agentRampy().auta().size());
        int b = agentKancelarie().mySim().invokeSync(() -> agentKancelarie().auta().size());
        int c = agentParkovisk().mySim().invokeSync(() -> agentParkovisk().auta().size());
        int d = agentDielne().mySim().invokeSync(() -> agentDielne().auta().size());
        int e = agentServisu().mySim().invokeSync(() -> agentServisu().auta().size());
        return (a+b+c+d+e);
        */
        int a = sim().invokeSync(() -> ((AgentRampy)sim().findAgent(Id.agentRampy)).autaSize());
        int b = sim().invokeSync(() -> ((AgentKancelarie)sim().findAgent(Id.agentKancelarie)).autaSize());
        int c = sim().invokeSync(() -> ((AgentParkovisk)sim().findAgent(Id.agentParkovisk)).autaSize());
        int d = sim().invokeSync(() -> ((AgentDielne)sim().findAgent(Id.agentDielne)).autaSize());
        int e = sim().invokeSync(() -> ((AgentServisu)sim().findAgent(Id.agentServisu)).autaSize());
        return (a+b+c+d+e);

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        double aktCas = agentRampy().mySim().invokeSync(() ->  agentRampy().mySim().currentTime());
        List<Auto> auta = sim().invokeSync(() -> ((AgentRampy)sim().findAgent(Id.agentRampy)).auta());
        
        auta.addAll(sim().invokeSync(() -> ((AgentServisu)sim().findAgent(Id.agentServisu)).auta()));
        
        auta.addAll(sim().invokeSync(() -> ((AgentDielne)sim().findAgent(Id.agentDielne)).auta()));
        auta.addAll(sim().invokeSync(() -> ((AgentKancelarie)sim().findAgent(Id.agentKancelarie)).auta()));
        
        auta.addAll(sim().invokeSync(() -> ((AgentParkovisk)sim().findAgent(Id.agentParkovisk)).auta()));
        
        if(auta== null)
            return null;
        if(auta.size() <= rowIndex) {
            return null;
        }
        Auto auto = auta.get(rowIndex);
        if(auto == null) {
            System.out.println("wtf");
            return null;
        }
        switch(columnIndex) {

            case id:
                return auto.id();

            case stav:
                if(auto.stav().contains("prichádza do servisu")) {
                    //System.out.println("... "+(aktCas+", "+auto.zaciatokPrijazdu()));
                    return auto.stav()+" ("+(Math.round(((aktCas-auto.zaciatokPrijazdu())/(13.32d))*10000)/100d)+"% )"+ " "+((Math.round(((aktCas-auto.zaciatokPrijazdu())/(13.32d))*1850d)/100d))+"m ";
                } else {
                    if(auto.stav().contains("prechádza vstupnou rampou")) {
                        return auto.stav()+" ("+(Math.round(((aktCas-auto.zaciatokPrejazduVstupnou())/(7d))*10000)/100d)+"% )";
                    } else {
                        if(auto.stav().contains("vystupnou rampou")) {
                            return auto.stav()+" ("+(Math.round(((aktCas-auto.zaciatokPrejazduVystupnou())/(7d))*10000)/100d)+"% )";
                        } else {
                            if(auto.stav().contains("odchádza zo servisu")) {
                                return auto.stav()+" ("+(Math.round(((aktCas-auto.zaciatokOdjazdu())/(13.32d))*10000)/100d)+"% )"+ " "+((Math.round(((aktCas-auto.zaciatokOdjazdu())/(13.32d))*1850d)/100d))+"m ";
                            } else
                                return auto.stav();
                        }
                            
                    }
                }
                
            default:
                return null;
        }
    }
}
