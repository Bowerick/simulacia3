/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import OSPABA.Simulation;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Parkovisko1;
import simulacia.SimulaciaServis;

/**
 *
 * @author Robo
 */
public class Parkovisko2TableModel extends MyTableModel {
    
    final int parkovisko2 = 0;
    final int obsadenost = 1;

    public Parkovisko2TableModel(Simulation sim, AgentServisu agentServisu, AgentRampy agentRampy, AgentKancelarie agentKancelarie, AgentDielne agentDielne, AgentParkovisk agentParkovisk) {
        super(sim, agentServisu, agentRampy, agentKancelarie, agentDielne, agentParkovisk);
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case parkovisko2:
                return "parkovisko2 (miesto)";
            case obsadenost:
                return "obsadenosť";
        }
        return null;
    }

    @Override
    public int getRowCount() {
        SimulaciaServis sim =(SimulaciaServis) agentParkovisk().mySim();
        int a = sim.invokeSync(() -> agentParkovisk().pocetParkovacichMiest2());
        return a;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        double aktCas = agentDielne().mySim().invokeSync(() ->  agentDielne().mySim().currentTime());
        SimulaciaServis sim =(SimulaciaServis) agentParkovisk().mySim();
        boolean [] parkovisko = sim.invokeSync(() -> agentParkovisk().parkovisko2().miesto());
        Parkovisko1 p = sim.invokeSync(() -> agentParkovisk().parkovisko2());
        if(p == null)
            return null;
        switch(columnIndex) {
            
            case parkovisko2:
                return (rowIndex+1);
            case obsadenost:
                if(parkovisko[rowIndex])
                    return "voľné";
                else {
                    if(p.zaparkovaneAuto(rowIndex) != null) {
                        if(p.zaparkovaneAuto(rowIndex).majitel() == null)
                            return null;
                        return "auto "+p.zaparkovaneAuto(rowIndex).id()+
                                " zákazníka "+p.zaparkovaneAuto(rowIndex).majitel().id();
                    }
                }
                
            default:
                return null;
        }
    }
    
}
