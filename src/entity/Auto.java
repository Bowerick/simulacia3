/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import OSPABA.Entity;
import OSPABA.Simulation;

/**
 *
 * @author Robo
 */
public class Auto extends Entity {
    
    private Zakaznik majitel;
    private String stav;
    private double zaciatokPrijazdu;
    private double zaciatokOdjazdu;
    private double zaciatokPrejazduVstupnou;
    private double zaciatokPrejazduVystupnou;
    
    public Auto(Simulation mySim) {
        super(mySim);
        majitel = null;
        stav = "-";
        zaciatokPrijazdu = .0;
        zaciatokPrejazduVstupnou =.0;
        zaciatokPrejazduVystupnou =.0;
    }

    /**
     * @return the majitel
     */
    public Zakaznik majitel() {
        return majitel;
    }

    /**
     * @param majitel the majitel to set
     */
    public void setMajitel(Zakaznik majitel) {
        this.majitel = majitel;
    }

    /**
     * @return the stav
     */
    public String stav() {
        return stav;
    }

    /**
     * @param stav the stav to set
     */
    public void setStav(String stav) {
        this.stav = stav;
    }

    /**
     * @return the zaciatokPrijazdu
     */
    public double zaciatokPrijazdu() {
        return zaciatokPrijazdu;
    }

    /**
     * @param zaciatokPrijazdu the zaciatokPrijazdu to set
     */
    public void setZaciatokPrijazdu(double zaciatokPrijazdu) {
        this.zaciatokPrijazdu = zaciatokPrijazdu;
    }

    public void setZaciatokPrejazduVstupnou(double currentTime) {
        this.zaciatokPrejazduVstupnou = currentTime;
    }

    /**
     * @return the zaciatokPrejazduVstupnou
     */
    public double zaciatokPrejazduVstupnou() {
        return zaciatokPrejazduVstupnou;
    }

    public void setZaciatokPrejazduVystupnou(double currentTime) {
        this.zaciatokPrejazduVystupnou = currentTime;
    }

    /**
     * @return the zaciatokPrejazduVstupnou
     */
    public double zaciatokPrejazduVystupnou() {
        return zaciatokPrejazduVystupnou;
    }

    /**
     * @return the zaciatokOdjazdu
     */
    public double zaciatokOdjazdu() {
        return zaciatokOdjazdu;
    }

    /**
     * @param zaciatokOdjazdu the zaciatokOdjazdu to set
     */
    public void setZaciatokOdjazdu(double zaciatokOdjazdu) {
        this.zaciatokOdjazdu = zaciatokOdjazdu;
    }
    
}
