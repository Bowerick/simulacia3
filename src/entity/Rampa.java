/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import OSPABA.Entity;
import OSPABA.Simulation;

/**
 *
 * @author Robo
 */
public class Rampa extends Entity {
    
    private Zakaznik zakaznikPriVstupe;
    private Zakaznik zakaznikPriOdchode;
    private boolean vstupna;
    private boolean vystupna;
    private String stav;
    
    public Rampa(Simulation mySim) {
        super(mySim);
        zakaznikPriVstupe = null;
        zakaznikPriOdchode = null;
        vstupna = true;
        vystupna = true;
        stav = "-";
    }

    /**
     * @return the zakaznikPriVstupe
     */
    public Zakaznik zakaznikPriVstupe() {
        return zakaznikPriVstupe;
    }

    /**
     * @param zakaznikPriVstupe the zakaznikPriVstupe to set
     */
    public void setZakaznikPriVstupe(Zakaznik zakaznikPriVstupe) {
        this.zakaznikPriVstupe = zakaznikPriVstupe;
    }

    /**
     * @return the zakaznikPriOdchode
     */
    public Zakaznik zakaznikPriOdchode() {
        return zakaznikPriOdchode;
    }

    /**
     * @param zakaznikPriOdchode the zakaznikPriOdchode to set
     */
    public void setZakaznikPriOdchode(Zakaznik zakaznikPriOdchode) {
        this.zakaznikPriOdchode = zakaznikPriOdchode;
    }

    /**
     * @return the vstupna
     */
    public boolean jeVstupnaVolna() {
        return vstupna;
    }
    
    public void obsadVstupnu() {
        this.vstupna = false;
    }
    
    public void uvolniVstupnu() {
        this.vstupna = true;
    }

    /**
     * @return the vystupna
     */
    public boolean jeVystupnaVolna() {
        return vystupna;
    }
    
    public void obsadVystupnu() {
        this.vystupna = false;
    }
    
    public void uvolniVystupnu() {
        this.vystupna = true;
    }

    /**
     * @return the stav
     */
    public String stav() {
        return stav;
    }

    /**
     * @param stav the stav to set
     */
    public void setStav(String stav) {
        this.stav = stav;
    }
    
    
}
