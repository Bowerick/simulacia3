/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import OSPABA.Entity;
import OSPABA.Simulation;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author Robo
 */
public class Parkovisko1 extends Entity {
    
    private Auto[] auta;
    private Auto[] rezervacia;
    private boolean[] miesto;
    private double[] dlzkaobsadenia;
    private double[] zaciatokObsadenia;
    private LinkedList<Auto> listAut;
    
    public Parkovisko1(Simulation mySim, int pocetParkovacichMiest) {
        super(mySim);
        miesto = new boolean[pocetParkovacichMiest];
        auta = new Auto[pocetParkovacichMiest];
        rezervacia = new Auto[pocetParkovacichMiest];
        Arrays.fill(miesto, true);
        dlzkaobsadenia = new double[pocetParkovacichMiest];
        zaciatokObsadenia = new double[pocetParkovacichMiest];
        Arrays.fill(dlzkaobsadenia, .0);
        Arrays.fill(zaciatokObsadenia, .0);
        listAut = new LinkedList();
    }
    
    public int dajVolneMiesto(Simulation sim) {
        int idx = -1;
        for(int i=0;i<miesto.length;i++) {
            if(miesto[i]) {
                miesto[i] = false;
                zaciatokObsadenia[i] = sim.currentTime();
                return i;
            }
        }
        return idx;
    }
    
    public void zaparkujAuto(int idx, Auto auto) {
        auta[idx] = auto;
    }
    
    public void rezervuj(int idx, Auto auto) {
        rezervacia[idx] = auto;
    }
    
    public Auto rezervovane(int idx) {
        return rezervacia[idx];
    }
        
    
    public Auto zaparkovaneAuto(int idx) {
        return auta[idx];
    }
    
    public int dajObsadeneMiesto() {
        int idx = -1;
        for(int i=0;i<miesto.length;i++) {
            if(!miesto[i]) {
                return i;
            }
        }
        return idx;
    }
    
    public void uvolniMiesto(int idx, Simulation sim) {
        miesto[idx] = true;
        dlzkaobsadenia[idx] += sim.currentTime() - zaciatokObsadenia[idx];
    }
    
    public int dlzkaParkoviska() {
        return miesto.length;
    }

    /**
     * @return the miesto
     */
    public boolean[] miesto() {
        return miesto;
    }
    
    public LinkedList<Auto> listAut() {
        return this.listAut;
    }

    /**
     * @return the dlzkaobsadenia
     */
    public double[] dlzkaobsadenia() {
        return dlzkaobsadenia;
    }
    
}
