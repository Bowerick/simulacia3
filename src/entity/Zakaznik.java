/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import OSPABA.Entity;
import OSPABA.Simulation;

/**
 *
 * @author Robo
 */
public class Zakaznik extends Entity {
    
    private String stav;
    private Pracovnik vybavovatelObjednavky;
    private Pracovnik prevzalAuto;
    private Pracovnik opravar;
    private Pracovnik preparkovalOpravene;
    private Auto auto;
    private double casPrichodu;// = cas zacatia cakatia na zadanie objednavky
    
    private double casZacatiaPrevzatiaAuta;
    private double dlzkaPrevzatia;
    
    private double dlzkaOpravy;
    private double casZadaniaObjednavky;
    
    private double dlzkaZO;
    private double casUkonceniaPrevzatiaAuta;
    
    public Zakaznik(Simulation mySim) {
        super(mySim);
        stav = "-";
        vybavovatelObjednavky = null;
        prevzalAuto = null;
        opravar = null;
        preparkovalOpravene = null;
        casPrichodu = .0;
        casZacatiaPrevzatiaAuta =.0;
        dlzkaOpravy = .0;
        auto = null;
        casUkonceniaPrevzatiaAuta = .0;
        casZadaniaObjednavky = .0;
        dlzkaZO = .0;
        dlzkaPrevzatia = .0;
    }

    /**
     * @return the stav
     */
    public String stav() {
        return stav;
    }

    /**
     * @param stav the stav to set
     */
    
    public void setStav(String stav) {
        this.stav = stav;
    }
    

    /**
     * @return the vybavovatelObjednavky
     */
    public Pracovnik vybavovatelObjednavky() {
        return vybavovatelObjednavky;
    }

    /**
     * @param vybavovatelObjednavky the vybavovatelObjednavky to set
     */
    public void setVybavovatelObjednavky(Pracovnik vybavovatelObjednavky) {
        this.vybavovatelObjednavky = vybavovatelObjednavky;
    }

    /**
     * @return the prevzalAuto
     */
    public Pracovnik prevzalAuto() {
        return prevzalAuto;
    }

    /**
     * @param prevzalAuto the prevzalAuto to set
     */
    public void setPrevzalAuto(Pracovnik prevzalAuto) {
        this.prevzalAuto = prevzalAuto;
    }

    /**
     * @return the opravar
     */
    public Pracovnik opravar() {
        return opravar;
    }

    /**
     * @param opravar the opravar to set
     */
    public void setOpravar(Pracovnik opravar) {
        this.opravar = opravar;
    }

    /**
     * @return the preparkovalOpravene
     */
    public Pracovnik preparkovalOpravene() {
        return preparkovalOpravene;
    }

    /**
     * @param preparkovalOpravene the preparkovalOpravene to set
     */
    public void setPreparkovalOpravene(Pracovnik preparkovalOpravene) {
        this.preparkovalOpravene = preparkovalOpravene;
    }

    public void setCasPrichodu(double cas) {
        this.casPrichodu = cas;
    }

    /**
     * @return the casPrichodu
     */
    public double casPrichodu() {
        return casPrichodu;
    }

    public double dlzkaOpravy() {
        return this.dlzkaOpravy;
    }

    /**
     * @param dlzkaOpravy the dlzkaOpravy to set
     */
    public void setDlzkaOpravy(double dlzkaOpravy) {
        this.dlzkaOpravy = dlzkaOpravy;
    }

    /**
     * @return the auto
     */
    public Auto auto() {
        return auto;
    }

    /**
     * @param auto the auto to set
     */
    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    /**
     * @return the casUkonceniaPrevzatiaAuta
     */
    public double casUkonceniaPrevzatiaAuta() {
        return casUkonceniaPrevzatiaAuta;
    }

    /**
     * @param casUkonceniaPrevzatiaAuta the casUkonceniaPrevzatiaAuta to set
     */
    public void setCasUkonceniaPrevzatiaAuta(double casUkonceniaPrevzatiaAuta) {
        this.casUkonceniaPrevzatiaAuta = casUkonceniaPrevzatiaAuta;
    }

    /**
     * @return the casZadaniaObjednavky
     */
    public double casZadaniaObjednavky() {
        return casZadaniaObjednavky;
    }

    /**
     * @param casZadaniaObjednavky the casZadaniaObjednavky to set
     */
    public void setCasZadaniaObjednavky(double casZadaniaObjednavky) {
        this.casZadaniaObjednavky = casZadaniaObjednavky;
    }

    /**
     * @return the dlzkaZO
     */
    public double dlzkaZO() {
        return dlzkaZO;
    }

    /**
     * @param dlzkaZO the dlzkaZO to set
     */
    public void setDlzkaZO(double dlzkaZO) {
        this.dlzkaZO = dlzkaZO;
    }

    /**
     * @return the casZacatiaPrevzatiaAuta
     */
    public double casZacatiaPrevzatiaAuta() {
        return casZacatiaPrevzatiaAuta;
    }

    /**
     * @param casZacatiaPrevzatiaAuta the casZacatiaPrevzatiaAuta to set
     */
    public void setCasZacatiaPrevzatiaAuta(double casZacatiaPrevzatiaAuta) {
        this.casZacatiaPrevzatiaAuta = casZacatiaPrevzatiaAuta;
    }

    /**
     * @return the dlzkaPrevzatia
     */
    public double dlzkaPrevzatia() {
        return dlzkaPrevzatia;
    }

    /**
     * @param dlzkaPrevzatia the dlzkaPrevzatia to set
     */
    public void setDlzkaPrevzatia(double dlzkaPrevzatia) {
        this.dlzkaPrevzatia = dlzkaPrevzatia;
    }
    
}
