/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import OSPABA.Entity;
import OSPABA.Simulation;

/**
 *
 * @author Robo
 */
public class Pracovnik extends Entity {
    
    private String stav;
    private boolean pracuje;
    private Zakaznik obsluhovanyZakaznik;
    
    private double zaciatokParkovaniaNa1;
    private double dlzkaParkovaniaNa1;
    
    private double zaciatokParkovaniaZ2;
    private double dlzkaParkovaniaZ2;
    
    private double zaciatokOpravy;
    private double dlzkaOpravy;
    
    private double dlzkaPrevzatiaOpravenehoAuta;
    private double casZacatiaPrevzatiaOpravenehoAuta;
    
    private double celkovyCasPrace;
    private double zaciatokPrace;
    
    public Pracovnik(Simulation mySim) {
        super(mySim);
        pracuje = false;
        stav = "-";
        zaciatokParkovaniaNa1 = .0;
        dlzkaParkovaniaNa1 = .0;
        
        zaciatokParkovaniaZ2 = .0;
        dlzkaParkovaniaZ2 = .0;
        
        zaciatokOpravy = .0;
        dlzkaOpravy = 0;
        
        casZacatiaPrevzatiaOpravenehoAuta = .0;
        dlzkaPrevzatiaOpravenehoAuta =.0;
        
        celkovyCasPrace = .0;
        zaciatokPrace = .0;
    }

    public void obsadPracovnika(double cas) {
        pracuje = true;
        zaciatokPrace = cas;
    }
    
    public void uvolniPracovnika(double cas) {
        pracuje = false;
        celkovyCasPrace += (cas - zaciatokPrace);
        
        //ak by som volal uvolniPRacovnika zbytocne pocitalo by mi cas viackrat toto tomu zabrani
        zaciatokPrace = cas;
    }
    /**
     * @return the pracuje
     */
    public boolean jePracujuci() {
        return pracuje;
    }

    /**
     * @return the obsluhovanyZakaznik
     */
    public Zakaznik obsluhovanyZakaznik() {
        return obsluhovanyZakaznik;
    }

    /**
     * @param obsluhovanyZakaznik the obsluhovanyZakaznik to set
     */
    public void setObsluhovanyZakaznik(Zakaznik obsluhovanyZakaznik) {
        this.obsluhovanyZakaznik = obsluhovanyZakaznik;
    }

    /**
     * @return the stav
     */
    public String stav() {
        return stav;
    }

    /**
     * @param stav the stav to set
     */
    public void setStav(String stav) {
        this.stav = stav;
    }

    /**
     * @return the zaciatokParkovaniaNa1
     */
    public double zaciatokParkovaniaNa1() {
        return zaciatokParkovaniaNa1;
    }

    /**
     * @param zaciatokParkovaniaNa1 the zaciatokParkovaniaNa1 to set
     */
    public void setZaciatokParkovaniaNa1(double zaciatokParkovaniaNa1) {
        this.zaciatokParkovaniaNa1 = zaciatokParkovaniaNa1;
    }

    /**
     * @return the dlzkaParkovaniaNa1
     */
    public double dlzkaParkovaniaNa1() {
        return dlzkaParkovaniaNa1;
    }

    /**
     * @param dlzkaParkovaniaNa1 the dlzkaParkovaniaNa1 to set
     */
    public void setDlzkaParkovaniaNa1(double dlzkaParkovaniaNa1) {
        this.dlzkaParkovaniaNa1 = dlzkaParkovaniaNa1;
    }

    /**
     * @return the zaciatokParkovaniaZ2
     */
    public double zaciatokParkovaniaZ2() {
        return zaciatokParkovaniaZ2;
    }

    /**
     * @param zaciatokParkovaniaZ2 the zaciatokParkovaniaZ2 to set
     */
    public void setZaciatokParkovaniaZ2(double zaciatokParkovaniaZ2) {
        this.zaciatokParkovaniaZ2 = zaciatokParkovaniaZ2;
    }

    /**
     * @return the dlzkaParkovaniaZ2
     */
    public double dlzkaParkovaniaZ2() {
        return dlzkaParkovaniaZ2;
    }

    /**
     * @param dlzkaParkovaniaZ2 the dlzkaParkovaniaZ2 to set
     */
    public void setDlzkaParkovaniaZ2(double dlzkaParkovaniaZ2) {
        this.dlzkaParkovaniaZ2 = dlzkaParkovaniaZ2;
    }

    /**
     * @return the zaciatokOpravy
     */
    public double zaciatokOpravy() {
        return zaciatokOpravy;
    }

    /**
     * @param zaciatokOpravy the zaciatokOpravy to set
     */
    public void setZaciatokOpravy(double zaciatokOpravy) {
        this.zaciatokOpravy = zaciatokOpravy;
    }

    /**
     * @return the dlzkaOpravy
     */
    public double dlzkaOpravy() {
        return dlzkaOpravy;
    }

    /**
     * @param dlzkaOpravy the dlzkaOpravy to set
     */
    public void setDlzkaOpravy(double dlzkaOpravy) {
        this.dlzkaOpravy = dlzkaOpravy;
    }

    /**
     * @return the dlzkaPrevzatiaOpravenehoAuta
     */
    public double dlzkaPrevzatiaOpravenehoAuta() {
        return dlzkaPrevzatiaOpravenehoAuta;
    }

    /**
     * @param dlzkaPrevzatiaOpravenehoAuta the dlzkaPrevzatiaOpravenehoAuta to set
     */
    public void setDlzkaPrevzatiaOpravenehoAuta(double dlzkaPrevzatiaOpravenehoAuta) {
        this.dlzkaPrevzatiaOpravenehoAuta = dlzkaPrevzatiaOpravenehoAuta;
    }

    /**
     * @return the casZacatiaPrevzatiaOpravenehoAuta
     */
    public double casZacatiaPrevzatiaOpravenehoAuta() {
        return casZacatiaPrevzatiaOpravenehoAuta;
    }

    /**
     * @param casZacatiaPrevzatiaOpravenehoAuta the casZacatiaPrevzatiaOpravenehoAuta to set
     */
    public void setCasZacatiaPrevzatiaOpravenehoAuta(double casZacatiaPrevzatiaOpravenehoAuta) {
        this.casZacatiaPrevzatiaOpravenehoAuta = casZacatiaPrevzatiaOpravenehoAuta;
    }

    /**
     * @return the celkovyCasPrace
     */
    public double celkovyCasPrace() {
        return celkovyCasPrace;
    }
}
