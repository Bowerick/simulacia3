/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacia;

import OSPABA.Simulation;
import OSPStat.Stat;
import agenti.AgentDielne;
import agenti.AgentKancelarie;
import agenti.AgentModelu;
import agenti.AgentOkolia;
import agenti.AgentParkovisk;
import agenti.AgentRampy;
import agenti.AgentServisu;
import entity.Pracovnik;
import java.util.List;

/**
 *
 * @author Robo
 */
public class SimulaciaServis extends Simulation {
    
    private AgentOkolia agentOkolia;
    private AgentModelu agentModelu;
    private AgentServisu agentServisu;
    private AgentRampy agentRampy;
    private AgentKancelarie agentKancelarie;
    private AgentDielne agentDielne;
    private AgentParkovisk agentParkovisk;
    
    private int pocetPracovnikovSkupiny1;
    private int pocetPracovnikovSkupiny2;
    private int pocetParkovacichMiest1;
    private int pocetParkovacichMiest2;
    
    private Stat casCakaniaNaOpravyStat;
    private Stat casCakaniaVRadeNaZadanieObjednavkyStat;
    private Stat trzbyStat;
    private Stat dlzkaRaduNaZO;
    private Stat dlzkaOpravyStat;
    private Stat priemerVyuzitiePrac1;
    private Stat priemerVyuzitiePrac2;
    private Stat priemerObsluzenychStat;
    private Stat priemerNeobsluzenychStat;
    private Stat priemerVyuzPark1;
    private Stat priemerVyuzPark2;
    private Stat priemerPocetAutPredServisom;
    
    
    private boolean hlavna;
    
    public SimulaciaServis() {
        agentModelu = new AgentModelu(Id.agentModelu, this, null);
        agentOkolia = new AgentOkolia(Id.agentOkolia, this, agentModelu);
        agentServisu = new AgentServisu(Id.agentServisu, this, agentModelu);
        agentRampy = new AgentRampy(Id.agentRampy, this, agentServisu);
        agentKancelarie = new AgentKancelarie(Id.agentKancelarie, this, agentServisu);
        agentDielne = new AgentDielne(Id.agentDielne, this, agentServisu);
        agentParkovisk = new AgentParkovisk(Id.agentParkovisk, this, agentServisu);
    }
    
    public void nastavParametre(int pocetPracovnikovSkupiny1, int pocetPracovnikovSkupiny2, int pocetParkovacichMiest1, int pocetParkovacichMiest2, int prioritaZO, int prioritaPrepark, double reklama, boolean vymeneneParkoviska) {
        this.pocetPracovnikovSkupiny1 = pocetPracovnikovSkupiny1;
        this.pocetPracovnikovSkupiny2 = pocetPracovnikovSkupiny2;
        this.pocetParkovacichMiest1 = pocetParkovacichMiest1;
        this.pocetParkovacichMiest2 = pocetParkovacichMiest2;
        //System.out.println("reklama: "+reklama);
        agentOkolia.setReklama(reklama);
        
        agentKancelarie.setPocetPracovnikov1(this.pocetPracovnikovSkupiny1);
        agentDielne.setPocetPracovnikov2(this.pocetPracovnikovSkupiny2);
        
        agentKancelarie.setPrioritaZadaniaObjednavky(prioritaZO);
        agentKancelarie.setPrioritaPreparkovaniaZParkoviska2(prioritaPrepark);
        
        agentParkovisk.setPocetParkovacichMiest1(this.pocetParkovacichMiest1);
        agentParkovisk.setPocetParkovacichMiest2(this.pocetParkovacichMiest2);
        agentParkovisk.setVymenene(vymeneneParkoviska);
    }
    
    @Override
    protected void simulationFinished() {
        super.simulationFinished();
        //System.out.println("koniec simulacie");
    }

    @Override
    protected void prepareSimulation()
    {
            super.prepareSimulation();
            casCakaniaNaOpravyStat = new Stat();
            casCakaniaVRadeNaZadanieObjednavkyStat = new Stat();
            dlzkaOpravyStat = new Stat();
            priemerVyuzitiePrac1 = new Stat();
            priemerVyuzitiePrac2 = new Stat();
            dlzkaRaduNaZO = new Stat();
            trzbyStat = new Stat();
            priemerObsluzenychStat = new Stat();
            priemerNeobsluzenychStat = new Stat();
            priemerVyuzPark1 = new Stat();
            priemerVyuzPark2 = new Stat();
            priemerPocetAutPredServisom = new Stat();
    }

    @Override
    protected void prepareReplication()
    {
            super.prepareReplication();
    }

    @Override
    protected void replicationFinished()
    {
            super.replicationFinished();
            
            casCakaniaNaOpravyStat.addSample(agentKancelarie().casCakaniaNaOpravuStat().mean()); 
            //System.out.println("cas cakania na opravu celkovo "+casCakaniaNaOpravyStat.mean());
            
            casCakaniaVRadeNaZadanieObjednavkyStat.addSample(agentKancelarie().casCakaniaVRadeNaZadaniaObjednavkyStat().mean());
            //System.out.println("cas cakania na zo celkovo "+casCakaniaVRadeNaZadanieObjednavkyStat.mean());
            
            dlzkaOpravyStat.addSample(agentKancelarie().dlzkaOpravyStat().mean());
            //System.out.println("dlzka opravy  celkovo "+dlzkaOpravyStat.mean());
           
            List<Pracovnik> prac1 = agentKancelarie.pracovnici1();
            for(Pracovnik p: prac1) {
                //System.out.println("prac1 "+(p.celkovyCasPrace() +", "+ Konstanty.dlzkaReplikacie));
                priemerVyuzitiePrac1.addSample(p.celkovyCasPrace() / Konstanty.dlzkaReplikacie);
            }
            
            List<Pracovnik> prac2 = agentDielne.pracovnci2();
            for(Pracovnik p: prac2) {
                //System.out.println("prac2 "+(p.celkovyCasPrace() +", "+ Konstanty.dlzkaReplikacie));
                priemerVyuzitiePrac2.addSample(p.celkovyCasPrace() / Konstanty.dlzkaReplikacie);
            }
            double[] d = agentParkovisk.parkovisko1().dlzkaobsadenia();
            for(int i=0;i<d.length;i++) {
                priemerVyuzPark1.addSample(d[i] / Konstanty.dlzkaReplikacie);
            }
            d = agentParkovisk.parkovisko2().dlzkaobsadenia();
            for(int i=0;i<d.length;i++) {
                priemerVyuzPark2.addSample(d[i] / Konstanty.dlzkaReplikacie);
            }
            priemerPocetAutPredServisom.addSample(agentKancelarie.pocetAutNaParkPredServisom().mean());
            //System.out.println("priemer  "+priemerPocetAutPredServisom.mean());
            //System.out.println("priemer  "+priemerVyuzPark1.mean()+", "+priemerVyuzPark2.mean());
            dlzkaRaduNaZO.addSample(agentKancelarie.dlzkaRadu().mean());
            trzbyStat.addSample(agentDielne.trzba());
            
            priemerObsluzenychStat.addSample(agentKancelarie.pocetObsluzenych());
            priemerNeobsluzenychStat.addSample(agentKancelarie.pocetNeobsluzenych());
            //System.out.println("priemerna dlzka radu "+agentKancelarie.dlzkaRadu().mean());
            
            //System.out.println("trzba "+agentDielne.trzba());
            //System.out.println("pocet opravenych aut "+agentDielne.pocetOpravenych());
            //System.out.println("priemer. dlzka oprave opravenych "+agentDielne.priemerCasOpravyOpraveneho());
            /*
            System.out.println("pocet zakaznikov "+agentOkolia.pocetZakaznikov());
            System.out.println("obsluzeny "+agentKancelarie.pocetObsluzenych());
            System.out.println("neobsluzeny "+agentKancelarie.pocetNeobsluzenych());
            */
            //System.out.println("_________________");
    }
        
    public int pocetZakaznikov() {
        return agentOkolia.pocetZakaznikov();
    }
    
    public AgentModelu agentModelu() {
        return agentModelu;
    }
    
    public AgentOkolia agentOkolia() {
        return agentOkolia;
    }
    
    public AgentServisu agentServisu() {
        return agentServisu;
    }
    
    public AgentRampy agentRampy() {
        return agentRampy;
    }
    
    public AgentKancelarie agentKancelarie() {
        return agentKancelarie;
    }
    
    public AgentDielne agentDielne() {
        return agentDielne;
    }
    
    public AgentParkovisk agentParkovisk() {
        return agentParkovisk;
    }

    /**
     * @return the casCakaniaNaOpravyStat
     */
    public Stat casCakaniaNaOpravyStat() {
        return casCakaniaNaOpravyStat;
    }

    /**
     * @return the casCakaniaVRadeNaZadanieObjednavkyStat
     */
    public Stat casCakaniaVRadeNaZadanieObjednavkyStat() {
        return casCakaniaVRadeNaZadanieObjednavkyStat;
    }

    /**
     * @return the dlzkaOpravyStat
     */
    public Stat dlzkaOpravyStat() {
        return dlzkaOpravyStat;
    }

    /**
     * @return the priemerVyuzitiePrac2
     */
    public Stat priemerVyuzitiePrac2() {
        return priemerVyuzitiePrac2;
    }

    /**
     * @return the priemerVyuzitiePrac1
     */
    public Stat priemerVyuzitiePrac1() {
        return priemerVyuzitiePrac1;
    }

    /**
     * @return the hlavna
     */
    public boolean isHlavna() {
        return hlavna;
    }

    /**
     * @param hlavna the hlavna to set
     */
    public void setHlavna(boolean hlavna) {
        this.hlavna = hlavna;
    }

    /**
     * @return the dlzkaRaduNaZO
     */
    public Stat dlzkaRaduNaZO() {
        return dlzkaRaduNaZO;
    }

    /**
     * @return the trzbyStat
     */
    public Stat trzbyStat() {
        return trzbyStat;
    }

    /**
     * @return the pocetPracovnikovSkupiny1
     */
    public int getPocetPracovnikovSkupiny1() {
        return pocetPracovnikovSkupiny1;
    }

    /**
     * @return the pocetPracovnikovSkupiny2
     */
    public int getPocetPracovnikovSkupiny2() {
        return pocetPracovnikovSkupiny2;
    }

    /**
     * @return the priemerObsluzenychStat
     */
    public Stat priemerObsluzenychStat() {
        return priemerObsluzenychStat;
    }

    /**
     * @return the priemerNeobsluzenychStat
     */
    public Stat priemerNeobsluzenychStat() {
        return priemerNeobsluzenychStat;
    }

    /**
     * @return the priemerVyuzPark1
     */
    public Stat priemerVyuzPark1() {
        return priemerVyuzPark1;
    }

    /**
     * @return the priemerVyuzPark2
     */
    public Stat priemerVyuzPark2() {
        return priemerVyuzPark2;
    }

    /**
     * @return the priemerPocetAutPredServisom
     */
    public Stat priemerPocetAutPredServisom() {
        return priemerPocetAutPredServisom;
    }
}
