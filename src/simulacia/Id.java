/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacia;

import OSPABA.IdList;
import OSPABA.SimComponent;

/**
 *
 * @author Robo
 */
public class Id extends IdList {
    
	// agenti
	public static final int agentOkolia = 1;
	public static final int agentModelu = 2;
	public static final int agentServisu = 3;
	public static final int agentRampy = 4;
	public static final int agentKancelarie = 5;
	public static final int agentDielne = 6;
	public static final int agentParkovisk = 7;
    
	// manazeri
	public static final int manazerOkolia = 101;
	public static final int manazerModelu = 102;
	public static final int manazerServisu = 103;
	public static final int manazerRampy = 104;
	public static final int manazerKancelarie = 105;
	public static final int manazerDielne = 106;
	public static final int manazerParkovisk = 107;
        
        // asistenti
        public static final int planovacPrichodovZakaznikov = 1001;
        public static final int procesPrejazduRampou = 1002;
        
        public static final int dotazVolnyPracovnik1 = 1003;
        public static final int dotazVolnyPracovnik2 = 1004;
        
        public static final int procesZadaniaObjednavky = 1005;
        
        public static final int dotazVolneParkovacieMiesto1 = 1006;
        public static final int dotazVolneParkovacieMiesto2 = 1007;
        
        public static final int procesPrevzatiaAutaOdZakaznika = 1008;
        public static final int procesPreparkovaniAutaNaParkovisko1 = 1009;
        public static final int procesPreparkovaniAutaNaParkovisko2 = 1010;
         
        public static final int procesPreparkovaniaAutaZParkoviska2 = 1011;
        
        public static final int procesOpravy = 1012;
        
        public static final int procesCakaniaNaVolneMiesto = 1013;
        
        public static final int procesPlanovaniaDniKancelaria = 1014;
        public static final int procesPrevzatiaOpravenehoAuta = 1015;
        public static final int procesPrejazduVystupnouRampou = 1016;
        
        public static final int procesPrijazdu = 1017;
        public static final int procesOdjazdu = 1018;
        
        public static final int procesPlanovaniaDniRampa = 1019;
}
