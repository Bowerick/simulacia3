/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacia;

import OSPABA.MessageForm;
import OSPABA.Simulation;
import entity.Auto;
import entity.Pracovnik;
import entity.Rampa;
import entity.Zakaznik;

/**
 *
 * @author Robo
 */
public class Sprava extends MessageForm {

    private Rampa rampa;
    private Pracovnik priradenyPracovnik;
    private Zakaznik zakaznik;
    private int parkovacieMiesto1;
    private int parkovacieMiesto2;
    private Auto auto;
    
    public Sprava(Simulation mySim) {
        super(mySim);
        priradenyPracovnik = null;
        zakaznik = null;
        rampa = null;
        parkovacieMiesto1 = -1;
        parkovacieMiesto2 = -1;
        auto = null;
    }
    
    private Sprava(Sprava original) {
        super(original);
        priradenyPracovnik = original.priradenyPracovnik;
        zakaznik = original.zakaznik;
        parkovacieMiesto1 = original.parkovacieMiesto1;
        parkovacieMiesto2 = original.parkovacieMiesto2;
        rampa = original.rampa;
        auto = original.auto;
    }

    @Override
    public MessageForm createCopy() {
        return new Sprava(this);
    }

    /**
     * @param priradenyPracovnik the vybavovatelObjednavky to set
     */
    public void setPriradenyPracovnik(Pracovnik priradenyPracovnik) {
        this.priradenyPracovnik = priradenyPracovnik;
    }

    /**
     * @return the vybavovatelObjednavky
     */
    public Pracovnik priradenyPracovnik() {
        return priradenyPracovnik;
    }

    /**
     * @return the zakaznik
     */
    public Zakaznik zakaznik() {
        return zakaznik;
    }

    /**
     * @param zakaznik the zakaznik to set
     */
    public void setZakaznik(Zakaznik zakaznik) {
        this.zakaznik = zakaznik;
    }

    /**
     * @param parkovacieMiesto1 the parkovacieMiesto1 to set
     */
    public void setParkovacieMiesto1(int parkovacieMiesto1) {
        this.parkovacieMiesto1 = parkovacieMiesto1;
    }

    /**
     * @return the parkovacieMiesto1
     */
    public int parkovacieMiesto1() {
        return parkovacieMiesto1;
    }

    /**
     * @param parkovacieMiesto2 the parkovacieMiesto2 to set
     */
    public void setParkovacieMiesto2(int parkovacieMiesto2) {
        this.parkovacieMiesto2 = parkovacieMiesto2;
    }

    /**
     * @return the parkovacieMiesto2
     */
    public int parkovacieMiesto2() {
        return parkovacieMiesto2;
    }

    /**
     * @return the auto
     */
    public Auto auto() {
        return auto;
    }

    /**
     * @param auto the auto to set
     */
    public void setAuto(Auto auto) {
        this.auto = auto;
    }
    
}
