/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacia;

/**
 *
 * @author Robo
 */
public class Konstanty {
    
    public static final int prejazdRampou = 7;
    public static final double strednaHodnotaPrichodovZakaznikovSem2 = 300d;//sem2 = expo(300)
    public static final double strednaHodnotaPrichodovZakaznikovSem3 = 1110d;//sem3 = 2 + EXPO(1.11e+003)
    public static final double prevzatieObjednavkyMin = (190d-120d);
    public static final double prevzatieObjednavkyMax = (190d+120d);
    public static final double prevzatieAutaOdZakaznikaMin = (120d-40d);
    public static final double prevzatieAutaOdZakaznikaMax = (120d+40d);
    public static final double cakanieNaVolneMiesto = 60d*10d;
    public static final double prevzatieOpravenehoAutaMin = 190d-67d;
    public static final double prevzatieOpravenehoAutaMax = 190d+67d;
    
    public static final double dlzkaReplikacie =(21d*8d*60d*60d);
    
}
