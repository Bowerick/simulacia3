/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacia;

import OSPABA.IdList;

/**
 *
 * @author Robo
 */
public class Mc extends IdList {

    public static final int init = 1;
	
    public static final int prichodZakaznika = 2;
    public static final int novyZakaznik = 3;
    public static final int obsluhaZakaznika = 4;
    public static final int prechodVstupnouRampou = 5;
    public static final int koniecPrechoduVstupnouRampou = 6;
    public static final int vstupDoKancelarie = 7;
    public static final int koniecZadaniaObjednavky = 8;
    public static final int koniecPrevzatiaAutaOdZakaznika = 9;
    public static final int koniecCakania10MinNaVolneMiesto = 10;
    
    public static final int zistenieParkovaciehoMiesta1 = 11;
    public static final int zistenieParkovaciehoMiesta2 = 12;
    
    public static final int parkovacieMiesto1Zistene = 13;
    public static final int parkovacieMiesto2Zistene = 14;
    
    public static final int koniecPreparkovaniaAutaNaParkovisko1 = 15;
    public static final int koniecPreparkovaniaAutaNaParkovisko2 = 16;
    public static final int koniecPreparkovaniaAutaZParkoviska2 = 17;
    
    //public static final int preparkovanieZParkoviska2 = 18;
    //public static final int preparkovanieZParkoviska2Mozne = 19;
    
    public static final int uvolniParkovacieMiesto1 = 20;
    
    public static final int novaOprava = 22;
    public static final int koniecOpravy = 23;
    
    public static final int zaparkujAutoNaParkovisko1 = 24;
    public static final int zaparkujAutoNaParkovisko2 = 25;
    public static final int zaparkovanie1Ukoncene = 26;
    public static final int zaparkovanie2Ukoncene = 27;
    
    public static final int odparkujAutaZParkoviska2 = 28;
    public static final int odparkovanieAutaZParkoviska2Ukoncene = 29;
    
    public static final int opravaAjZaparkovanieSkoncene = 30;
    public static final int zakaznikVybaveny = 31;
    public static final int zadavanieObjednavky = 32;
    public static final int odchodZakaznikaPo10Minutach = 33;
    
    public static final int odchodZakaznika = 34;
    
    public static final int parkovacieMiesto1Uvolnene = 35;
    
    public static final int zacniPlanovanieDnov = 36;
    public static final int koniecDnaKancelaria = 37;
    public static final int prevzatiaOpravenehoAuta = 38;
    public static final int prechodVystupnouRampou = 39;
    public static final int koniecPrechoduVystupnouRampou = 40;
    public static final int opustaServis = 41;
    
    public static final int vstupDoServisu = 42;
    public static final int koniecPrijazdu = 43;
    public static final int koniecOdjazdu = 44;
    
    public static final int zacniPlanovanieDnovKancelaria = 45;
    public static final int zacniPlanovanieDnovRampa = 46;
    public static final int koniecDnaRampa = 47;
    
}
