/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agenti.AgentServisu;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerServisu extends Manager {

    public ManazerServisu(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            //prichod zakaznika - posiela request o prejazd vstupnou rampou
            case Mc.obsluhaZakaznika:
                sprava.setCode(Mc.prechodVstupnouRampou);
                sprava.setAddressee(Id.agentRampy);
                request(sprava);
            break;
            
            //response od AR po prejazde vstupnou rampou 
            case Mc.vstupDoServisu:
                sprava.setAddressee(Id.procesPrijazdu);
                sprava.auto().setZaciatokPrijazdu(mySim().currentTime());
                sprava.auto().setStav("auto zákazníka "+sprava.auto().majitel().id()+" prichádza do servisu");
                myAgent().pridajPrichadzajuceAuto(sprava.auto());
                startContinualAssistant(sprava);
            break;
            
            //response od AK po vybaveni zakaznika, 
            case Mc.zakaznikVybaveny:
                sprava.setAddressee(Id.procesOdjazdu);
                sprava.auto().setZaciatokOdjazdu(mySim().currentTime());
                sprava.auto().setStav("auto zákazníka "+sprava.auto().majitel().id()+" odchádza zo servisu");
                myAgent().pridajOdchadzajuceAuto(sprava.auto());
                startContinualAssistant(sprava);
            break;
            
            //respose od AR po prejazde vystupnou rampou - posiela response AM o odchode zakaznika
            case Mc.opustaServis:
                sprava.setCode(Mc.odchodZakaznika);
                response(sprava);
            break;
            
            case Mc.finish:
                switch(sprava.sender().id()) {
                    
                    //posiela request AK o vybavenie zakaznika
                    case Id.procesPrijazdu:
                        sprava.setAddressee(Id.agentKancelarie);
                        sprava.setCode(Mc.vstupDoKancelarie);
                        myAgent().odoberPrichadzajuceAuto(sprava.auto());
                        request(sprava);
                    break;
                    
                    //posiela request o prajazd vystupnou rampou
                    case Id.procesOdjazdu:
                        sprava.setCode(Mc.prechodVystupnouRampou);
                        myAgent().odoberOdchadzajuceAuto(sprava.auto());
                        sprava.setAddressee(Id.agentRampy);
                        request(sprava);
                    break;
                }
            break;
            
            case Mc.zacniPlanovanieDnov:
                sprava.setCode(Mc.zacniPlanovanieDnovKancelaria);
                sprava.setAddressee(Id.agentKancelarie);
                notice(sprava);
                
                Sprava copy = (Sprava) sprava.createCopy();
                copy.setCode(Mc.zacniPlanovanieDnovRampa);
                copy.setAddressee(Id.agentRampy);
                notice(copy);
            
        }
    }
    
    @Override
    public AgentServisu myAgent() {
        return (AgentServisu)super.myAgent();
    }
}
