/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agenti.AgentDielne;
import entity.Auto;
import entity.Pracovnik;
import entity.Zakaznik;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerDielne extends Manager {
    
    public ManazerDielne(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava  = (Sprava) message;
        Pracovnik p;
        Zakaznik z;
        Auto a = sprava.auto();
        
        switch(sprava.code()) {
            
            //request od AK o opravu auta
            //v pripade zacatia opravy posiela notice AP o uvolneni park miesta 1
            case Mc.novaOprava:
                myAgent().dotazVolnyPracovnik2().execute(sprava);
                p = sprava.priradenyPracovnik();
                if(p != null) {
                    z = sprava.zakaznik();
                    z.setOpravar(p);
                    p.setObsluhovanyZakaznik(sprava.zakaznik());
                    Sprava nextSprava = (Sprava) sprava.createCopy();
                    p.setStav("opravuje auto zákazníka "+sprava.zakaznik().id());
                    a.setStav("auto zákazníka "+a.majitel().id()+" opravuje pracovník "+p.id());
                    p.setZaciatokOpravy(mySim().currentTime());
                    sprava.setAddressee(Id.procesOpravy);
                    startContinualAssistant(sprava);
                    
                    nextSprava.setCode(Mc.uvolniParkovacieMiesto1);
                    nextSprava.setAddressee(Id.agentServisu);
                    notice(nextSprava);
                    
                } else {
                    myAgent().frontaAutNaOpravu().enqueue(sprava);
                }
                    
            break;
            
            case Mc.finish:
                switch(sprava.sender().id()) {
                    //koniec opravy - posiela notice AP o zaparkovanie orpavenho auta na park 2
                    case Id.procesOpravy:
                        sprava.setCode(Mc.zaparkujAutoNaParkovisko2);
                        sprava.setAddressee(Id.agentServisu);
                        notice(sprava);
                        
                        myAgent().pridajOpraveneho(sprava.zakaznik().dlzkaOpravy());
                        myAgent().pridajTrzbu(((sprava.zakaznik().dlzkaOpravy()/3600d)*25d));
                    break;
                }
            break;
            
            //notice od AP po zaparkovani opraveneho auta na park 2
            //posiela response AK na request o opravu auta
            case Mc.zaparkovanie2Ukoncene:
                a.setStav("auto zákazníka "+a.majitel().id()+ " čaká na preparkovanie na parkovisko pred servisom");
                p = sprava.priradenyPracovnik();
                p.setObsluhovanyZakaznik(null);
                p.uvolniPracovnika(mySim().currentTime());
                if(myAgent().frontaAutNaOpravu().size() > 0) {
                    Sprava nextSprava;
                    nextSprava = myAgent().frontaAutNaOpravu().dequeue();
                    nextSprava.setPriradenyPracovnik(p);
                    p.setStav("opravuje auto zákazníka "+nextSprava.zakaznik().id());
                    p.setZaciatokOpravy(mySim().currentTime());
                    p.obsadPracovnika(mySim().currentTime());
                    p.setObsluhovanyZakaznik(nextSprava.zakaznik());
                    nextSprava.auto().setStav("auto zákazníka "+nextSprava.auto().majitel().id()+" opravuje pracovník "+p.id());
                    nextSprava.setAddressee(Id.procesOpravy);
                    startContinualAssistant(nextSprava);
                    
                    Sprava nova = new Sprava(mySim());
                    nova.setParkovacieMiesto1(nextSprava.parkovacieMiesto1());
                    nova.setCode(Mc.uvolniParkovacieMiesto1);
                    nova.setAddressee(Id.agentServisu);
                    notice(nova);
                }
                sprava.setCode(Mc.opravaAjZaparkovanieSkoncene);
                response(sprava);
        }
    }
    
    @Override
    public AgentDielne myAgent() {
        return (AgentDielne) super.myAgent();
    }
    
}
