/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agenti.AgentRampy;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerRampy extends Manager {

    public ManazerRampy(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            
            case Mc.zacniPlanovanieDnovRampa:
                sprava.setAddressee(Id.procesPlanovaniaDniRampa);
                startContinualAssistant(sprava);
            break;
                
            case Mc.prechodVstupnouRampou:
                myAgent().pridajPocetPredServisom();
                if(myAgent().rampa().jeVstupnaVolna()) {
                    myAgent().rampa().setZakaznikPriVstupe(sprava.zakaznik());
                    myAgent().rampa().obsadVstupnu();
                    sprava.setAddressee(Id.procesPrejazduRampou);
                    //System.out.println("prechadza rampou "+sprava.zakaznik().id());
                    sprava.zakaznik().setStav("prechádza rampou");
                    sprava.auto().setZaciatokPrejazduVstupnou(mySim().currentTime());
                    sprava.auto().setStav("auto zákazníka "+sprava.auto().majitel().id() +" prechádza vstupnou rampou");
                    startContinualAssistant(sprava);
                    
                } else {
                    //System.out.println("caka v rade na rampu "+sprava.zakaznik().id());
                    sprava.zakaznik().setStav("čaká na uvoľnenie vstupnej rampy");
                    sprava.auto().setStav("auto zákazníka "+sprava.auto().majitel().id() +" čaká na uvoľnenie vstupnej rampy");
                    myAgent().frontZakaznikovPredRampouPriPrichode().enqueue(message);
                    
                }
            break;
            
            case Mc.prechodVystupnouRampou:
                myAgent().pridajPocetPredOdchodomZoServisu();
                if(myAgent().rampa().jeVystupnaVolna()) {
                    myAgent().rampa().setZakaznikPriOdchode(sprava.zakaznik());
                    myAgent().rampa().obsadVystupnu();
                    sprava.setAddressee(Id.procesPrejazduVystupnouRampou);
                    sprava.zakaznik().setStav("prechádza vystupnou rampou");
                    sprava.auto().setZaciatokPrejazduVystupnou(mySim().currentTime());
                    sprava.auto().setStav("auto zákazníka "+sprava.auto().majitel().id() +" prechádza vystupnou rampou");
                    startContinualAssistant(sprava);
                    
                } else {
                    //System.out.println("caka v rade na rampu "+sprava.zakaznik().id());
                    sprava.zakaznik().setStav("čaká na uvoľnenie vystupnej rampy");
                    sprava.auto().setStav("auto zákazníka "+sprava.auto().majitel().id() +" čaká na uvoľnenie vystupnej rampy");
                    myAgent().frontZakaznikovPredRampouPriOdchode().enqueue(message);
                    
                }
            
            case Mc.finish:
                switch(sprava.sender().id()) {
                    
                    case Id.procesPrejazduRampou:
                        myAgent().odoberPocetPredServisom();
                        myAgent().rampa().uvolniVstupnu();
                        myAgent().rampa().setZakaznikPriVstupe(null);

                        if(myAgent().frontZakaznikovPredRampouPriPrichode().size() > 0) {

                            Sprava nextMessage = (Sprava) myAgent().frontZakaznikovPredRampouPriPrichode().dequeue();
                            myAgent().rampa().obsadVstupnu();
                            myAgent().rampa().setZakaznikPriVstupe(nextMessage.zakaznik());
                            nextMessage.setAddressee(Id.procesPrejazduRampou);
                            startContinualAssistant(nextMessage);

                        }
                        sprava.setCode(Mc.vstupDoServisu);
                        response(sprava);
                    break;
                    
                    case Id.procesPrejazduVystupnouRampou:
                        myAgent().odoberPocetPredOdchodomZoServisu();
                        myAgent().rampa().uvolniVystupnu();
                        myAgent().rampa().setZakaznikPriOdchode(null);

                        if(myAgent().frontZakaznikovPredRampouPriOdchode().size() > 0) {

                            Sprava nextMessage = (Sprava) myAgent().frontZakaznikovPredRampouPriOdchode().dequeue();
                            myAgent().rampa().obsadVystupnu();
                            myAgent().rampa().setZakaznikPriOdchode(nextMessage.zakaznik());
                            nextMessage.setAddressee(Id.procesPrejazduVystupnouRampou);
                            startContinualAssistant(nextMessage);

                        }
                        sprava.setCode(Mc.opustaServis);
                        response(sprava);
                    break;
                    
                    case Id.procesPlanovaniaDniRampa:
                        myAgent().frontZakaznikovPredRampouPriPrichode().clear();
                        myAgent().vynulujPocetPredServisom();
                }
                    
            break;
        }
    }
    
    @Override
    public AgentRampy myAgent() {
        return (AgentRampy)super.myAgent();
    }
    
}
