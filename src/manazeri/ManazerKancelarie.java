/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agenti.AgentKancelarie;
import entity.Auto;
import entity.Pracovnik;
import entity.Zakaznik;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerKancelarie extends Manager {

    public ManazerKancelarie(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        Pracovnik p;
        Zakaznik z = sprava.zakaznik();
        Auto a = sprava.auto();
        
        switch(sprava.code()) {
            //notice od AS po zacati simulacie - resetovanie radu zakaznikov na konci dna
            case Mc.zacniPlanovanieDnovKancelaria:
                sprava.setAddressee(Id.procesPlanovaniaDniKancelaria);
                startContinualAssistant(sprava);
            break;
            
            //request od AS o vybavenie zakaznika
            case Mc.vstupDoKancelarie:
                z.setCasPrichodu(mySim().currentTime());
                z.setStav("čaká v rade na zadanie objednávky");
                a.setStav("čaká na parkovisku pred servisom na zadanie objednávky zákazníkom "+a.majitel().id());
                myAgent().frontaZakaznikovNaZadanieObjednavky().enqueue(sprava);
                myAgent().frontAutNaParkPredServisom().enqueue(sprava);
                myAgent().pridajCakajucehoNaZadanieObjednavky();
                
                sprava.setAddressee(Id.procesCakaniaNaVolneMiesto);
                startContinualAssistant(sprava);
                pridelPracu(sprava);
            break;
            
            //response od AP na request AK ci mam volne miesto na park1
            case Mc.parkovacieMiesto1Zistene:
                int idx = sprava.parkovacieMiesto1();
                if(idx > -1) {
                    p = sprava.priradenyPracovnik();
                    sprava = myAgent().frontaZakaznikovNaZadanieObjednavky().dequeue();
                    myAgent().odoberCakajucehoZadanieObjednavky();
                    myAgent().pridajPocetZadavajucichObjednavku();
                    sprava.setPriradenyPracovnik(p);
                    p.setObsluhovanyZakaznik(sprava.zakaznik());
                    sprava.setParkovacieMiesto1(idx);
                    /*
                    sprava.setAddressee(Id.procesCakaniaNaVolneMiesto);
                    breakContinualAssistant(sprava);
                    */
                    //ma naplanovany hold musim novu spravu...
                    Sprava nextSprava = (Sprava) sprava.createCopy();
                    nextSprava.setAddressee(Id.procesCakaniaNaVolneMiesto);
                    breakContinualAssistant(nextSprava);
                    
                } else {
                    //mam pracovnika volne a priorita ZO je vyssia,
                    //ale nemam miesto na park1 tak zisti ci je na park2 auto na preparkovanie
                    if(myAgent().prioritaZadaniaObjednavky() > myAgent().prioritaPreparkovaniaZParkoviska2()) {
        
                        sprava.setCode(Mc.zistenieParkovaciehoMiesta2);
                        sprava.setAddressee(Id.agentServisu);
                        request(sprava);
                    } else {
                        sprava.priradenyPracovnik().uvolniPracovnika(mySim().currentTime());
                    }
                }
            break;
            
            //notice od AP o uvolneni park miesta 1 - pracovnik 1 moze zacat preberat objednavku
            case Mc.parkovacieMiesto1Uvolnene:
                pridelPracu(sprava);
            break;
          
            //notice od CA, ze preslo 10 minut, odkedy prisel zakaznik, ak som ho medzitym neobluzil, poslem ho domov
            case Mc.odchodZakaznikaPo10Minutach:
                //toDo odkomentovat
                // kontrola sem2 neposielam zakaznikov domov
                
                if(myAgent().frontaZakaznikovNaZadanieObjednavky().contains(sprava)) {
                    sprava = myAgent().frontaZakaznikovNaZadanieObjednavky().dequeue();
                    myAgent().frontAutNaParkPredServisom().dequeue();
                    myAgent().pridajNeobsluzeneho();
                    myAgent().odoberCakajucehoZadanieObjednavky();
                    a.setStav("aut odchádza nevybavené");
                    sprava.setCode(Mc.zakaznikVybaveny);
                    response(sprava);
                }
            break;
            
            //response od AP po zaparkovani auta na park 1 na request AK
            //posiela request AD o oprave
            case Mc.zaparkovanie1Ukoncene:
                p = sprava.priradenyPracovnik();
                p.uvolniPracovnika(mySim().currentTime());
                p.setStav("nepracuje");
                sprava.setCode(Mc.novaOprava);
                sprava.setAddressee(Id.agentServisu);
                request(sprava);
                pridelPracu(sprava);
            break;
            
            //response od AP na request AK ci je na parkovisku2 auto na preparkovanie
            case Mc.parkovacieMiesto2Zistene:
                idx = sprava.parkovacieMiesto2();
                if(idx != -1) {
                    if(sprava.priradenyPracovnik() != null) {
                        sprava.setCode(Mc.odparkujAutaZParkoviska2);
                        sprava.setAddressee(Id.agentServisu);
                        request(sprava);
                        
                    }
                } else {
                    if(myAgent().prioritaPreparkovaniaZParkoviska2() > myAgent().prioritaZadaniaObjednavky()) {
                        if(myAgent().frontaZakaznikovNaZadanieObjednavky().size() > 0) {
                            Sprava pom = myAgent().frontaZakaznikovNaZadanieObjednavky().peek();
                            sprava.setAuto(pom.auto());
                            sprava.setCode(Mc.zistenieParkovaciehoMiesta1);
                            sprava.setAddressee(Id.agentServisu);

                            request(sprava);
                        } else {
                            sprava.priradenyPracovnik().uvolniPracovnika(mySim().currentTime());
                        }
                    } else {
                       //System.out.println("nema co robit konci22 "+sprava.zakaznik().id()+", "+mySim().currentTime());
                        sprava.priradenyPracovnik().uvolniPracovnika(mySim().currentTime());
                    }
                }
            break;
            
            //response AP na request o odparkovanie auta z park 2
            case Mc.odparkovanieAutaZParkoviska2Ukoncene:
                p = sprava.priradenyPracovnik();
                myAgent().odoberCakajucehoNaOpravu();
                a.setStav(p.id()+" odovzdáva auto zákazníkovi "+z.id()+" po oprave "); 
                p.setStav(p.id()+" odovzdáva auto zákazníkovi "+z.id()+" po oprave ");
                z.setStav("preberá auto "+a.id()+" po oprave");
                p.setCasZacatiaPrevzatiaOpravenehoAuta(mySim().currentTime());
                sprava.setAddressee(Id.procesPrevzatiaOpravenehoAuta);
                startContinualAssistant(sprava);
            break;
            
            //response AD po oprave a zaparkovani na park2 na request o opravu auta
            case Mc.opravaAjZaparkovanieSkoncene:
                pridelPracu(sprava);
            break;
            
            case Mc.finish:
                switch(sprava.sender().id()) {
                    
                    case Id.procesPlanovaniaDniKancelaria:
                        //aby sa dala ratat statistika treba dequeue nie clear  7.5.2017
                        for(int i=0;i<myAgent().frontaZakaznikovNaZadanieObjednavky().size();i++) {
                            Sprava koniecDna = myAgent().frontaZakaznikovNaZadanieObjednavky().dequeue();
                            myAgent().frontAutNaParkPredServisom().dequeue();
                            Sprava copy = (Sprava) koniecDna.createCopy();
                            myAgent().pridajNeobsluzeneho();
                            myAgent().odoberCakajucehoZadanieObjednavky();
                            copy.auto().setStav("aut odchádza nevybavené");
                            copy.setCode(Mc.zakaznikVybaveny);
                            response(copy);
                        }
                        //myAgent().frontaZakaznikovNaZadanieObjednavky().clear();
                        myAgent().vynulujPocetCakajucichNaZO();
                    break;
                    
                    //koniec cakania zakaznika v rade, zaciatok obsluhy
                    case Id.procesCakaniaNaVolneMiesto:
                        p = sprava.priradenyPracovnik();
                        z = sprava.zakaznik();
                        z.setPrevzalAuto(p);
                        z.setStav("zadáva objednávku pracovníkovi "+p.id());
                        p.setStav("príjma objednávku zákazníka "+z.id());
                        myAgent().pridajObsluzeneho();
                        
                        z.setCasZadaniaObjednavky(mySim().currentTime());
                        myAgent().casCakaniaVRadeNaZadaniaObjednavkyStat().addSample(mySim().currentTime() - z.casPrichodu());
                        sprava.setAddressee(Id.procesZadaniaObjednavky);////////////
                        startContinualAssistant(sprava);
                    break;
                    
                    //koniec zadania objednavky
                    case Id.procesZadaniaObjednavky:
                        z = sprava.zakaznik();
                        z.setCasZacatiaPrevzatiaAuta(mySim().currentTime());
                        p = sprava.priradenyPracovnik();
                        p.setStav("príjma auto od zákazníka "+z.id());
                        z.setStav("odovzdáva auto pracovníkovi "+p.id());
                        a.setStav("zákazník "+z.id()+" odovzdáva auto pracovníkovi "+sprava.priradenyPracovnik().id());
                        sprava.setAddressee(Id.procesPrevzatiaAutaOdZakaznika);
                        startContinualAssistant(sprava);
                        
                    break;
                    
                    //koniec prevzatia auta od zakaznika
                    //posiela request AP na zaparkovanie na parkovisku1 - response= zaparkovanie1Ukoncene
                    case Id.procesPrevzatiaAutaOdZakaznika:
                        myAgent().frontAutNaParkPredServisom().dequeue();
                        z.setStav("čaká na opravu auta");
                        z.setCasUkonceniaPrevzatiaAuta(mySim().currentTime());
                        myAgent().odoberPocetZadavajucichoObjednavku();
                        myAgent().pridajCakajucehoNaOpravu();
                        sprava.setAddressee(Id.agentServisu);
                        sprava.setCode(Mc.zaparkujAutoNaParkovisko1);
                        request(sprava);
                    break;
                    
                    //koniec prevzatia opraveneho auta 
                    //posiela response AS na request o vybaveni zakaznika
                    case Id.procesPrevzatiaOpravenehoAuta:

                        p = sprava.priradenyPracovnik();
                        p.uvolniPracovnika(mySim().currentTime());
                        p.setStav("nepracuje");
                        z = sprava.zakaznik();
                        z.setStav("vybaveny "+z.id());
                        a.setStav("opravené auto zákazníka "+a.majitel().id()+" si prevzal zakazník "+z.id());
                        p.setObsluhovanyZakaznik(null);
                        myAgent().dlzkaOpravyStat().addSample(z.dlzkaOpravy());
                        //myAgent().pridajObsluzeneho();
                        myAgent().casCakaniaNaOpravuStat().addSample(mySim().currentTime() - z.casUkonceniaPrevzatiaAuta());                
                        sprava.setCode(Mc.zakaznikVybaveny);
                        response(sprava);
                        pridelPracu(sprava);
                    break;
                    
                }
            break;
        }
    }
    
    @Override
    public AgentKancelarie myAgent() {
        return (AgentKancelarie) super.myAgent();
    }

    private void pridelPracu(Sprava sprava) {
        Sprava spravaCopy = (Sprava) sprava.createCopy();
        myAgent().dotazVolnyPracovnik1().execute(spravaCopy);
        Pracovnik p = spravaCopy.priradenyPracovnik();
        if(p == null)  {
            return;
        }
        if(myAgent().prioritaZadaniaObjednavky() > myAgent().prioritaPreparkovaniaZParkoviska2()) {
            if(myAgent().frontaZakaznikovNaZadanieObjednavky().size() > 0) {
                
                spravaCopy.setCode(Mc.zistenieParkovaciehoMiesta1);
                spravaCopy.setAddressee(Id.agentServisu);
                request(spravaCopy);
                return;
            } 
            //priorita zadania objednavky najvyssia a nemam zakaznika na zadanie objednavky
            else {
                spravaCopy.setCode(Mc.zistenieParkovaciehoMiesta2);
                spravaCopy.setAddressee(Id.agentServisu);
                request(spravaCopy);
            }
        }
        
        if(myAgent().prioritaPreparkovaniaZParkoviska2() > myAgent().prioritaZadaniaObjednavky()) {
            spravaCopy.setCode(Mc.zistenieParkovaciehoMiesta2);
            spravaCopy.setAddressee(Id.agentServisu);
            request(spravaCopy);
        }
    }
    
}
