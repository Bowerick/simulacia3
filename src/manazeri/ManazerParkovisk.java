/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agenti.AgentParkovisk;
import entity.Auto;
import entity.Pracovnik;
import entity.Zakaznik;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerParkovisk extends Manager {

    public ManazerParkovisk(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        int idx;
        Zakaznik z;
        Pracovnik p;
        Auto a = sprava.auto();
        
        switch(sprava.code()) {
            
            //request od AK na zaparkovanie auta na park 1
            case Mc.zaparkujAutoNaParkovisko1:
                p = sprava.priradenyPracovnik();
                z = sprava.zakaznik();
                p.setZaciatokParkovaniaNa1(mySim().currentTime());
                a.setStav(sprava.priradenyPracovnik().id()+" parkuje auto zákazníka "+z.id()+" na parkovisko 1");
                sprava.setAddressee(Id.procesPreparkovaniAutaNaParkovisko1);
                startContinualAssistant(sprava);
                p.setStav("parkuje auto "+a.id()+" zákazníka "+z.id()+" na parkovisko 1");
            break;
            
            //notice od AD na apakovanie auta na park 2
            case Mc.zaparkujAutoNaParkovisko2:
                p = sprava.priradenyPracovnik();
                z = sprava.zakaznik();
                myAgent().frontaAutNaPreparkovanieZDielne().enqueue(sprava);
                p.setStav("čaká na uvoľnenie parkovacieho miesta na parkovisku 2");
                a.setStav(p.id()+" čaká na uvoľnenie parkovacieho miesta na parkovisku 2 pre auto "+a.id()+" zákazníka "+a.majitel().id());
                idx = myAgent().parkovisko2().dajVolneMiesto(mySim());
                if(idx != -1) {
                    p.setStav("parkuje auto "+a.id()+" zákazníka "+z.id()+" na parkovisko 2");
                    a.setStav(p.id()+" parkuje auto zákazníka "+a.majitel().id()+" na parkovisko 2");
                    sprava = (Sprava) myAgent().frontaAutNaPreparkovanieZDielne().dequeue();
                    sprava.setPriradenyPracovnik(p);
                    sprava.setParkovacieMiesto2(idx);
                    z = sprava.zakaznik();
                    myAgent().parkovisko2().zaparkujAuto(idx, z.auto());
                    myAgent().parkovisko2().listAut().add(z.auto());
                    sprava.setAddressee(Id.procesPreparkovaniAutaNaParkovisko2);
                    startContinualAssistant(sprava);
                }
            break;
            
            //request od AK na odparkovanie auta z park 2 po oprave
            case Mc.odparkujAutaZParkoviska2:
                p = sprava.priradenyPracovnik();
                z = sprava.zakaznik();
                p.setStav("parkuje auto "+a.id()+" zákazníka "+z.id()+" po oprave na parkovisko pred servisom");
                a.setStav(p.id()+" parkuje auto zákazníka "+z.id()+" po oprave na parkovisko pred servisom");
                p.setZaciatokParkovaniaZ2(mySim().currentTime());
                myAgent().parkovisko2().uvolniMiesto(sprava.parkovacieMiesto2(),mySim());
                myAgent().parkovisko2().zaparkujAuto(sprava.parkovacieMiesto2(), null);
                sprava.setAddressee(Id.procesPreparkovaniaAutaZParkoviska2);
                startContinualAssistant(sprava);
                //uvolnilo sa miesto pracovnik 2 moze zaparkovat ak nejaky caka
                if(myAgent().frontaAutNaPreparkovanieZDielne().size() > 0) {
                    idx = myAgent().parkovisko2().dajVolneMiesto(mySim());
                    if(idx != -1) {
                        sprava = myAgent().frontaAutNaPreparkovanieZDielne().dequeue();
                        z = sprava.zakaznik();
                        //p = sprava.zakaznik().opravar();
                        p = sprava.priradenyPracovnik();
                        a = sprava.auto();
                        sprava.setParkovacieMiesto2(idx);//todo 7.5.2017
                        if(p == null) {
                            System.out.println("pracovnik opravar nie je");
                        }
                        
                        p.setStav("parkuje auto "+a.id()+" zákazníka "+z.id()+" na parkovisko 2");
                        a.setStav(p.id()+" parkuje auto zákazníka "+a.majitel().id()+" na parkovisko 2");
                        myAgent().parkovisko2().zaparkujAuto(idx, z.auto());
                        myAgent().parkovisko2().listAut().add(z.auto());
                        sprava.setAddressee(Id.procesPreparkovaniAutaNaParkovisko2);
                        startContinualAssistant(sprava);
                    }
                }
            break;
            
            case Mc.finish:
                switch(sprava.sender().id()) {
                    
                    //koniec parkovania auta na park 1, posiela response AK
                    case Id.procesPreparkovaniAutaNaParkovisko1:
                        myAgent().parkovisko1().zaparkujAuto(sprava.parkovacieMiesto1(), sprava.auto());
                        sprava.setCode(Mc.zaparkovanie1Ukoncene);
                        a.setStav("auto zákazníka "+a.majitel().id()+" čaká na opravu na parkovisku 1");
                        response(sprava);
                    break;
                    
                    //koniec preparkovania auta na park 2 po oprave
                    case Id.procesPreparkovaniAutaNaParkovisko2:
                        p = sprava.priradenyPracovnik();
                        p.setStav("nepracuje");
                        p.uvolniPracovnika(mySim().currentTime());
                        myAgent().frontaAutNaPreparkovanieZParkoviska2().enqueue(sprava);//1
                        myAgent().parkovisko2().zaparkujAuto(sprava.parkovacieMiesto2(), sprava.auto());
                        
                        Sprava copy = (Sprava) sprava.createCopy();
                        copy.setCode(Mc.zaparkovanie2Ukoncene);
                        copy.setAddressee(Id.agentServisu);
                        notice(copy);
                    break;
                    
                    //koniec preparkovania auta z park 2, posiela response AK
                    case Id.procesPreparkovaniaAutaZParkoviska2:
                        myAgent().parkovisko2().zaparkujAuto(sprava.parkovacieMiesto2(), null);
                        sprava.setCode(Mc.odparkovanieAutaZParkoviska2Ukoncene);
                        response(sprava);
                    break;
                }
            break;
            
            //request od AK, posiela request ci mam na park 1 volne miesto, 
            case Mc.zistenieParkovaciehoMiesta1:
                myAgent().dotazVolneParkovacieMiesto1().execute(sprava);
                if(sprava.parkovacieMiesto1() != -1) {
                    myAgent().parkovisko1().rezervuj(sprava.parkovacieMiesto1(), sprava.auto());
                }
                sprava.setCode(Mc.parkovacieMiesto1Zistene);
                response(sprava);
            break;
            
            //request od AK, ci mam nejake auto na parkovisku2, ktore treba preparkovat 
            //dava response so spravou o parkovacom miest (idx)
            case Mc.zistenieParkovaciehoMiesta2:
                p = sprava.priradenyPracovnik();
                idx = -1;
                if(myAgent().frontaAutNaPreparkovanieZParkoviska2().size() > 0) {
                    sprava = myAgent().frontaAutNaPreparkovanieZParkoviska2().dequeue();
                    sprava.setPriradenyPracovnik(p);
                    p.setObsluhovanyZakaznik(sprava.zakaznik());
                    idx = myAgent().parkovisko2().dajObsadeneMiesto();
                    
                }
                sprava.setParkovacieMiesto2(idx);
                sprava.setCode(Mc.parkovacieMiesto2Zistene);
                response(sprava);
            break;
            
            //po zacati opravy posielam notice o uvolneni park miesta 1,
            //aby mohol pripadne vybavovat dalsieho zakaznika
            case Mc.uvolniParkovacieMiesto1:
                myAgent().parkovisko1().uvolniMiesto(sprava.parkovacieMiesto1(),mySim());
                myAgent().parkovisko1().zaparkujAuto(sprava.parkovacieMiesto1(), null);
                Sprava nextMessage = (Sprava) sprava.createCopy();
                nextMessage.setCode(Mc.parkovacieMiesto1Uvolnene);
                nextMessage.setAddressee(Id.agentServisu);
                notice(nextMessage);
            break;
            
        }
    }
    
    @Override
    public AgentParkovisk myAgent() {
        return (AgentParkovisk)super.myAgent();
    }
    
}
