/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import agenti.AgentOkolia;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerOkolia extends Manager {

    public ManazerOkolia(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            //planovanei prichodov zakaznikov
            case Mc.init:
                zacniPlanovatPrichodyZakazniky();
            break;
            
            //prichod zakaznika do systemu, odosiela sa ako notice AM
            case Mc.finish:
                sprava.setCode(Mc.prichodZakaznika);
                sprava.setAddressee(Id.agentModelu);
                myAgent().pridajZakaznika();
                notice(sprava);
            break;
            
            //odchod zakaznika po vybaveni v servise
            case Mc.odchodZakaznika:
                //System.out.println("odchod zakaznika "+sprava.zakaznik().id());
            break;
        }
    }

    private void zacniPlanovatPrichodyZakazniky() {
        Sprava message = new Sprava(mySim());
        message.setAddressee(Id.planovacPrichodovZakaznikov);
        startContinualAssistant(message);
    }
    
    @Override
    public AgentOkolia myAgent() {
        return (AgentOkolia) super.myAgent();
    }
    
}
