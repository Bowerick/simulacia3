/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manazeri;

import OSPABA.Agent;
import OSPABA.Manager;
import OSPABA.MessageForm;
import OSPABA.Simulation;
import simulacia.Id;
import simulacia.Mc;
import simulacia.Sprava;

/**
 *
 * @author Robo
 */
public class ManazerModelu extends Manager {

    public ManazerModelu(int id, Simulation mySim, Agent myAgent) {
        super(id, mySim, myAgent);
    }

    @Override
    public void processMessage(MessageForm message) {
        Sprava sprava = (Sprava) message;
        switch(sprava.code()) {
            //spustenie simulacie 
            case Mc.init:
                //notice - zacni planovanie prichodov
                sprava.setAddressee(Id.agentOkolia);
                notice(sprava);
                
                //notice - zacni planovanie dni - resetovanie radu zakaznikov na konci dna
                Sprava spravaCopy = (Sprava) sprava.createCopy();
                spravaCopy.setCode(Mc.zacniPlanovanieDnov);
                spravaCopy.setAddressee(Id.agentServisu);
                notice(spravaCopy);
                
            break;
            
            //prichod noveho zakaznika
            case Mc.prichodZakaznika:
                //posielam request o opravu AK
                sprava.setAddressee(Id.agentServisu);
                sprava.setCode(Mc.obsluhaZakaznika);
                request(sprava);
            break;
            
            //odchod zakaznika po vybaveni v servise, response od MK
            case Mc.odchodZakaznika:
                //notice pre AO o odchode zakaznika
                sprava.setAddressee(Id.agentOkolia);
                notice(sprava);
            break;
        }
    }
    
}
